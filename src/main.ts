import { ErrorMapper } from "./utilities/ErrorMapper";
import "./prototypes/ConstructionSites";
import "./prototypes/Creep";
import "./prototypes/Misc";
import "./prototypes/Room";
import "./prototypes/RoomObjects";
import "./prototypes/RoomPosition";
import "./prototypes/RoomStructures";
import "./prototypes/RoomVisual";
import "./prototypes/Structures";
import * as Profiler from "./profiler";
import _Collective from "./Collective";
import _ from "lodash";
import Mem from "./Memory";
import { BaseConsole } from "./console/Console";
import { USE_PROFILER } from "./~settings";

// When compiling TS to JS and bundling with rollup, the line numbers and file names in error messages change
// This utility uses source maps to get the line numbers and file names of the original, TS source code
export const loop = ErrorMapper.wrapLoop(() => {
  if (!Mem.shouldRun()) return;
  Mem.clean();

  if (!global.Collective || Collective.shouldRebuild || Game.time >= Collective.expiration) {
    delete global.Collective;
    global.Collective = new _Collective();
    Collective.build();
  } else {
    Collective.refresh()
  }

  Collective.init();
  Collective.run();
  Collective.visuals();
});

function onGlobalReset(): void {
  Mem.format();
  BaseConsole.init();
}

onGlobalReset();

global.Profiler = Profiler.init();

import _ from "lodash";

export const ROOMTYPE_SOURCEKEEPER = 'SK';
export const ROOMTYPE_CORE = 'CORE';
export const ROOMTYPE_CONTROLLER = 'CTRL';
export const ROOMTYPE_ALLEY = 'ALLEY';

export class Cartographer {
    /**
	 * Lists all rooms up to a given distance away, including roomName
	 */
    static findRoomsInRange(roomName: string, depth: number): string[] {
        return _.flatten(_.values(this.recursiveRoomSearch(roomName, depth)));
    }

	/**
	 * Lists all rooms up at a given distance away, including roomName
	 */
    static findRoomsAtRange(roomName: string, depth: number): string[] {
        return this.recursiveRoomSearch(roomName, depth)[depth];
    }

	/**
	 * Recursively enumerate all rooms from a root node using depth first search to a maximum depth
	 */
    static recursiveRoomSearch(roomName: string, maxDepth: number): { [depth: number]: string[] } {
        const visitedRooms = this._recursiveRoomSearch(roomName, 0, maxDepth, {});
        const roomDepths: { [depth: number]: string[] } = {};
        for (const room in visitedRooms) {
            const depth = visitedRooms[room];
            if (!roomDepths[depth]) {
                roomDepths[depth] = [];
            }
            roomDepths[depth].push(room);
        }
        return roomDepths;
    }

	/**
	 * The recursive part of recursiveRoomSearch. Yields inverted results mapping roomName to depth.
	 */
    private static _recursiveRoomSearch(roomName: string, depth: number, maxDepth: number,
        visited: { [roomName: string]: number }): { [roomName: string]: number } {
        if (visited[roomName] == undefined) {
            visited[roomName] = depth;
        } else {
            visited[roomName] = Math.min(depth, visited[roomName]);
        }
        const neighbors = _.values(Game.map.describeExits(roomName)) as string[];
        if (depth < maxDepth) {
            for (const neighbor of neighbors) {
                // Visit the neighbor if not already done or if this would be a more direct route
                if (visited[neighbor] == undefined || depth + 1 < visited[neighbor]) {
                    this._recursiveRoomSearch(neighbor, depth + 1, maxDepth, visited);
                }
            }
        }
        return visited;
    }

    /**
	 * Get the name of a room offset from the anchor room
	 */
    static findRelativeRoomName(roomName: string, xDelta: number, yDelta: number): string {
        const coords = this.getRoomCoordinates(roomName);
        let xDir = coords.xDir;
        if (xDir === 'W') {
            xDelta = -xDelta;
        }
        let yDir = coords.yDir;
        if (yDir === 'N') {
            yDelta = -yDelta;
        }
        let x = coords.x + xDelta;
        let y = coords.y + yDelta;
        if (x < 0) {
            x = Math.abs(x) - 1;
            xDir = this.oppositeDir(xDir);
        }
        if (y < 0) {
            // noinspection JSSuspiciousNameCombination
            y = Math.abs(y) - 1;
            yDir = this.oppositeDir(yDir);
        }

        return xDir + x + yDir + y;
    }

    /**
	 * Get the coordinates from a room name
	 */
    static getRoomCoordinates(roomName: string): RoomCoord {
        const coordinateRegex = /(E|W)(\d+)(N|S)(\d+)/g;
        const match = coordinateRegex.exec(roomName)!;

        const xDir = match[1];
        const x = match[2];
        const yDir = match[3];
        const y = match[4];

        return {
            x: Number(x),
            y: Number(y),
            xDir: xDir,
            yDir: yDir,
        };
    }

    /**
	 * Return the opposite direction, e.g. "W" => "E"
	 */
    static oppositeDir(dir: string): string {
        switch (dir) {
            case 'W':
                return 'E';
            case 'E':
                return 'W';
            case 'N':
                return 'S';
            case 'S':
                return 'N';
            default:
                return 'error';
        }
    }

    /**
	 * Get the type of the room
	 */
    static roomType(roomName: string): 'SK' | 'CORE' | 'CTRL' | 'ALLEY' {
        const coords = this.getRoomCoordinates(roomName);
        if (coords.x % 10 === 0 || coords.y % 10 === 0) {
            return ROOMTYPE_ALLEY;
        } else if (coords.x % 5 === 0 && coords.y % 5 === 0) {
            return ROOMTYPE_CORE;
        } else if (coords.x % 10 <= 6 && coords.x % 10 >= 4 && coords.y % 10 <= 6 && coords.y % 10 >= 4) {
            return ROOMTYPE_SOURCEKEEPER;
        } else {
            return ROOMTYPE_CONTROLLER;
        }
    }
}

import _ from "lodash";

const hexChars = '0123456789abcdef';

export function color(str: string, color: string): string {
    return `<font color='${color}'>${str}</font>`;
}

export function printRoomName(roomName: string): string {
    return '<a href="#!/room/' + Game.shard.name + '/' + roomName + '">' + roomName + '</a>';
}

export function getAllBaseRooms(): Room[] {
    return _.filter(_.values(Game.rooms), room => room.my);
}

/**
 * Generate a randomly-offset cache expiration time
 */
export function getCacheExpiration(timeout: number, offset = 5): number {
    return Game.time + timeout + Math.round((Math.random() * offset * 2) - offset);
}

export function onPublicServer(): boolean {
    return Game.shard.name.includes('shard');
}

export function hasJustSpawned(): boolean {
    return _.keys(Collective.bases).length == 1 && _.keys(Game.creeps).length == 0 && _.keys(Game.spawns).length == 1;
}


/**
 * Generate a random hex string of specified length
 */
export function randomHex(length: number): string {
    let result = '';
    for (let i = 0; i < length; i++) {
        result += hexChars[Math.floor(Math.random() * hexChars.length)];
    }
    return result;
}

/**
 * Compute an exponential moving average
 */
export function exponentialMovingAverage(current: number, avg: number | undefined, window: number): number {
    return (current + (avg || 0) * (window - 1)) / window;
}

/**
 * Compute an exponential moving average for unevenly spaced samples
 */
export function irregularExponentialMovingAverage(current: number, avg: number, dt: number, window: number): number {
    return (current * dt + avg * (window - dt)) / window;
}

export function derefProtoPos(pos: ProtoPos): RoomPosition {
    return new RoomPosition(pos.x, pos.y, pos.roomName);
}

export function derefCoords(coordName: string, roomName: string): RoomPosition {
    const [x, y] = coordName.split(':');
    return new RoomPosition(parseInt(x, 10), parseInt(y, 10), roomName);
}

export function equalXYR(a: ProtoPos, b: ProtoPos): boolean {
    return a.x == b.x && a.y == b.y && a.roomName == b.roomName;
}

/**
 * Correct generalization of the modulo operator to negative numbers
 */
export function mod(n: number, m: number): number {
    return ((n % m) + m) % m;
}

export function deref(ref: string): RoomObject | null { // dereference any object from identifier
    return Game.getObjectById(ref) || Game.flags[ref] || Game.creeps[ref] || Game.spawns[ref] || null;
};

/**
 * Obtain the username of the player
 */
export function getUsername(): string {
    for (const i in Game.rooms) {
        const room = Game.rooms[i];
        if (room.controller && room.controller.my) {
            return room.controller.owner.username;
        }
    }
    for (const i in Game.creeps) {
        const creep = Game.creeps[i];
        if (creep.owner) {
            return creep.owner.username;
        }
    }
    console.log('ERROR: Could not determine username. You can set this manually in src/settings/settings_user');
    return 'ERROR: Could not determine username.';
}

/**
 * Merges a list of store-like objects, summing overlapping keys. Useful for calculating assets from multiple sources
 */
export function mergeSum(objects: { [key: string]: number | undefined }[]): { [key: string]: number } {
    const ret: { [key: string]: number } = {};
    for (const object of objects) {
        for (const key in object) {
            const amount = object[key] || 0;
            if (!ret[key]) {
                ret[key] = 0;
            }
            ret[key] += amount;
        }
    }
    return ret;
}


export function hasMinerals(store: { [resourceType: string]: number }): boolean {
    for (const resourceType in store) {
        if (resourceType != RESOURCE_ENERGY && (store[<ResourceConstant>resourceType] || 0) > 0) {
            return true;
        }
    }
    return false;
}

export function coordName(coord: Coord): string {
    return coord.x + ':' + coord.y;
}

/**
 * Equivalent to lodash.minBy() method
 */
export function minBy<T>(objects: T[], iteratee: ((obj: T) => number | false)): T | undefined {
    let minObj: T | undefined;
    let minVal = Infinity;
    let val: number | false;
    for (const i in objects) {
        val = iteratee(objects[i]);
        if (val !== false && val < minVal) {
            minVal = val;
            minObj = objects[i];
        }
    }
    return minObj;
}

/**
 * Equivalent to lodash.maxBy() method
 */
export function maxBy<T>(objects: T[], iteratee: ((obj: T) => number | false)): T | undefined {
    let maxObj: T | undefined;
    let maxVal = -Infinity;
    let val: number | false;
    for (const i in objects) {
        val = iteratee(objects[i]);
        if (val !== false && val > maxVal) {
            maxVal = val;
            maxObj = objects[i];
        }
    }
    return maxObj;
}

interface ToColumnOpts {
    padChar: string;
    justify: boolean;
}

/**
 * Create column-aligned text array from object with string key/values
 */
export function toColumns(obj: { [key: string]: string }, opts = {} as ToColumnOpts): string[] {
    _.defaults(opts, {
        padChar: ' ',	// Character to pad with, e.g. "." would be key........val
        justify: false 	// Right align values column?
    });

    const ret = [];
    const keyPadding = _.maxBy(_.map(_.keys(obj), str => str.length))! + 1;
    const valPadding = _.max(_.map(_.keys(obj), str => str.length))!;

    for (const key in obj) {
        if (opts.justify) {
            ret.push(key.padRight(keyPadding, opts.padChar) + obj[key].padLeft(valPadding, opts.padChar));
        } else {
            ret.push(key.padRight(keyPadding, opts.padChar) + obj[key]);
        }
    }

    return ret;
}


export function getPosFromString(str: string | undefined | null): RoomPosition | undefined {
    if (!str) return;
    const posName = _.first(str.match(/(E|W)\d+(N|S)\d+:\d+:\d+/g) || []);
    if (posName) {
        const [roomName, x, y] = posName.split(':');
        return new RoomPosition(parseInt(x, 10), parseInt(y, 10), roomName);
    }
    return undefined;
}

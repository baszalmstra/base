import { MY_USERNAME } from "../~settings";
import _ from "lodash";

Object.defineProperty(Structure.prototype, 'isWalkable', {
    get() {
        return this.structureType == STRUCTURE_ROAD ||
            this.structureType == STRUCTURE_CONTAINER ||
            (this.structureType == STRUCTURE_RAMPART && (<StructureRampart>this.my ||
                <StructureRampart>this.isPublic));
    },
    configurable: true,
});


// Container prototypes ================================================================================================

Object.defineProperty(StructureContainer.prototype, 'energy', {
    get() {
        return this.store[RESOURCE_ENERGY];
    },
    configurable: true,
});

Object.defineProperty(StructureContainer.prototype, 'isFull', { // if this container-like object is full
    get() {
        return _.sum(_.values(this.store)) >= this.storeCapacity;
    },
    configurable: true,
});
Object.defineProperty(StructureContainer.prototype, 'isEmpty', { // if this container-like object is empty
    get() {
        return _.sum(_.values(this.store)) == 0;
    },
    configurable: true,
});

// Storage prototypes ==================================================================================================

Object.defineProperty(StructureStorage.prototype, 'energy', {
    get() {
        return this.store[RESOURCE_ENERGY];
    },
    configurable: true,
});

Object.defineProperty(StructureStorage.prototype, 'isFull', { // if this container-like object is full
    get() {
        return _.sum(this.store) >= this.storeCapacity;
    },
    configurable: true,
});

Object.defineProperty(StructureStorage.prototype, 'isEmpty', { // if this container-like object is empty
    get() {
        return _.sum(this.store) == 0;
    },
    configurable: true,
});

// Terminal prototypes =================================================================================================

Object.defineProperty(StructureTerminal.prototype, 'energy', {
    get() {
        return this.store[RESOURCE_ENERGY];
    },
    configurable: true,
});

Object.defineProperty(StructureTerminal.prototype, 'isFull', { // if this container-like object is full
    get() {
        return _.sum(this.store) >= this.storeCapacity;
    },
    configurable: true,
});

Object.defineProperty(StructureTerminal.prototype, 'isEmpty', { // if this container-like object is empty
    get() {
        return _.sum(this.store) == 0;
    },
    configurable: true,
});

// Tombstone prototypes ================================================================================================

Object.defineProperty(Tombstone.prototype, 'energy', {
    get() {
        return this.store[RESOURCE_ENERGY];
    },
    configurable: true,
});

// Controller prototypes ===============================================================================================

Object.defineProperty(StructureController.prototype, 'reservedByMe', {
    get: function () {
        return this.reservation && this.reservation.username == MY_USERNAME;
    },
    configurable: true,
});

Object.defineProperty(StructureController.prototype, 'signedByMe', {
    get: function () {
        return this.sign && this.sign.text == Memory.settings.signature && Game.time - this.sign.time < 250000;
    },
    configurable: true,
});

Object.defineProperty(StructureController.prototype, 'signedByScreeps', {
    get: function () {
        return this.sign && this.sign.username == 'Screeps';
    },
    configurable: true,
});


StructureController.prototype.needsReserving = function (reserveBuffer: number): boolean {
    return !this.reservation || (this.reservedByMe && this.reservation.ticksToEnd < reserveBuffer);
};

Object.defineProperty(RoomObject.prototype, 'ref', { // reference object; see globals.deref (which includes Creep)
    get: function () {
        return this.id || this.name || '';
    },
    configurable: true,
});

Object.defineProperty(RoomObject.prototype, 'targetedBy', { // List of creep names with tasks targeting this object
    get: function () {
        return Collective.cache.targets[this.ref] || [];
    },
    configurable: true,
});

import { Bot } from "../bots/Bot";
import _ from "lodash";
import { maxBy } from "../utilities/util";
import CombatIntel from "../intel/CombatIntel";
import { profile } from "../profiler";
import { HostileStructurePriorities, AttackStructurePriorities, AttackStructureScores } from "../priorities/structures";
import { Pathing } from "../movement/Pathing";

@profile
export default class CombatTargeting {

    /**
	 * Finds the best target within a given range that a bot can currently attack
	 */
    static findBestCreepTargetInRange(bot: Bot, range: number, targets = bot.room.hostiles): Creep | undefined {
        const nearbyHostiles = _.filter(targets, c => bot.pos.inRangeToXY(c.pos.x, c.pos.y, range));
        return maxBy(nearbyHostiles, function (hostile) {
            if (hostile.hitsPredicted == undefined) hostile.hitsPredicted = hostile.hits;
            if (hostile.pos.lookForStructure(STRUCTURE_RAMPART)) return false;
            return hostile.hitsMax - hostile.hitsPredicted + CombatIntel.getHealPotential(hostile); // compute score
        });
    }

    /**
	 * Finds the best target within a given range that a zerg can currently attack
	 */
    static findBestStructureTargetInRange(bot: Bot, range: number, allowUnowned = true): Structure | undefined {
        let nearbyStructures = _.filter(bot.room.hostileStructures,
            s => bot.pos.inRangeToXY(s.pos.x, s.pos.y, range));
        // If no owned structures to attack and not in colony room or outpost, target unowned structures
        if (allowUnowned && nearbyStructures.length == 0 && !Collective.basesByRoom[bot.room.name]) {
            nearbyStructures = _.filter(bot.room.structures,
                s => bot.pos.inRangeToXY(s.pos.x, s.pos.y, range));
        }
        return maxBy(nearbyStructures, function (structure) {
            let score = 10 * AttackStructureScores[structure.structureType];
            if (structure.pos.lookForStructure(STRUCTURE_RAMPART)) score *= .1;
            return score;
        });
    }

    /**
	 * Finds the best (friendly) target in range that a zerg can currently heal
	 */
    static findBestHealingTargetInRange(healer: Bot, range = 3, friendlies = healer.room.creeps): Creep | undefined {
        return maxBy(_.filter(friendlies, f => healer.pos.getRangeTo(f) <= range), friend => {
            if (friend.hitsPredicted == undefined) friend.hitsPredicted = friend.hits;
            const attackProbability = 0.5;
            for (const hostile of friend.pos.findInRange(friend.room.hostiles, 3)) {
                if (hostile.pos.isNearTo(friend)) {
                    friend.hitsPredicted -= attackProbability * CombatIntel.getAttackDamage(hostile);
                } else {
                    friend.hitsPredicted -= attackProbability * (CombatIntel.getAttackDamage(hostile)
                        + CombatIntel.getRangedAttackDamage(hostile));
                }
            }
            const healScore = friend.hitsMax - friend.hitsPredicted;
            if (healer.pos.getRangeTo(friend) > 1) {
                return healScore + CombatIntel.getRangedHealAmount(healer.creep);
            } else {
                return healScore + CombatIntel.getHealAmount(healer.creep);
            }
        });
    }

    static findClosestHostileStructure(bot: Bot, checkReachable = false): Structure | undefined {
        for (const structureType of HostileStructurePriorities) {
            const structures = _.filter(bot.room.hostileStructures, s => s.structureType == structureType);
            if (structures.length == 0) continue;
            if (checkReachable) {
                const closestReachable = this.findClosestReachable(bot.pos, structures) as Structure | undefined;
                if (closestReachable) return closestReachable;
            } else {
                return bot.pos.findClosestByRange(structures) as Structure | undefined;
            }
        }
        return undefined
    }

    // This method is expensive
    static findClosestReachable(pos: RoomPosition, targets: (Creep | Structure)[]): Creep | Structure | undefined {
        const targetsByRange = _.sortBy(targets, target => pos.getRangeTo(target));
        return _.find(targetsByRange, target => Pathing.isReachable(pos, target.pos, target.room.barriers));
    }

    static findClosestHurtFriendly(healer: Bot): Creep | null {
        return healer.pos.findClosestByRange(_.filter(healer.room.creeps, creep => creep.hits < creep.hitsMax));
    }

    static findClosestHostile(bot: Bot, checkReachable = false, ignoreCreepsAtEdge = true): Creep | undefined {
        if (bot.room.hostiles.length > 0) {
            let targets: Creep[];
            if (ignoreCreepsAtEdge) {
                targets = _.filter(bot.room.hostiles, hostile => hostile.pos.rangeToEdge > 0);
            } else {
                targets = bot.room.hostiles;
            }
            if (checkReachable) {
                const targetsByRange = _.sortBy(targets, target => bot.pos.getRangeTo(target));
                return _.find(targetsByRange, target => Pathing.isReachable(bot.pos, target.pos, bot.room.barriers));
            } else {
                return bot.pos.findClosestByRange(targets) as Creep | undefined;
            }
        }
        return undefined
    }

    /**
	 * Finds the best target within a given range that a bot can currently attack
	 */
    static findBestCreepTargetForTowers(room: Room, targets = room.hostiles): Creep | undefined {
        return maxBy(targets, hostile => {
            if (hostile.hitsPredicted == undefined) hostile.hitsPredicted = hostile.hits;
            if (hostile.pos.lookForStructure(STRUCTURE_RAMPART)) return false;
            return hostile.hitsMax - hostile.hitsPredicted
                + CombatIntel.getHealPotential(hostile) + (CombatIntel.towerDamageAtPos(hostile.pos) || 0);
        });
    }

    static findClosestPrioritizedStructure(bot: Bot, checkReachable = false): Structure | undefined {
        for (const structureType of AttackStructurePriorities) {
            const structures = _.filter(bot.room.hostileStructures, s => s.structureType == structureType);
            if (structures.length == 0) continue;
            if (checkReachable) {
                const closestReachable = this.findClosestReachable(bot.pos, structures) as Structure | undefined;
                if (closestReachable) return closestReachable;
            } else {
                return bot.pos.findClosestByRange(structures) as Structure | undefined;
            }
        }
        return undefined
    }
}

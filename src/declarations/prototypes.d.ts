type rechargeObjectType = StructureStorage
    | StructureTerminal
    | StructureContainer
    | StructureLink
    | Tombstone
    | Resource;

type StorageUnit = StructureContainer | StructureTerminal | StructureStorage;

interface Room {
    room: any;
    print: string;
    my: boolean;
    owner: string | undefined;
    signedByMe: boolean;
    reservedByMe: boolean;
    isOutpost: boolean;
    spawns: StructureSpawn[];
    extensions: StructureExtension[];
    roads: StructureRoad[];
    walls: StructureWall[];
    ramparts: StructureRampart[];
    keeperLairs: StructureKeeperLair[];
    portals: StructurePortal[];
    links: StructureLink[];
    towers: StructureTower[];
    labs: StructureLab[];
    containers: StructureContainer[];
    powerBanks: StructurePowerBank[];
    observer: StructureObserver;
    powerSpawn: StructurePowerSpawn;
    extractor: StructureExtractor;
    nuker: StructureNuker;
    constructedWalls: StructureWall[];
    walkableRamparts: StructureRampart[];
    barriers: (StructureWall | StructureRampart)[];
    invaderCores: StructureInvaderCore[];

    creeps: Creep[];
    hostiles: Creep[];
    dangerousHostiles: Creep[];
    playerHostiles: Creep[];
    invaders: Creep[];
    dangerousPlayerHostiles: Creep[];
    fleeDefaults: HasPos[];
    hostileStructures: Structure[];
    structures: Structure[];

    flags: Flag[];
    tombstones: Tombstone[];
    drops: { [resourceType: string]: Resource[] };
    droppedEnergy: Resource[];
    droppedPower: Resource[];
    sources: Source[];
    rechargeables: rechargeObjectType[];
    storageUnits: StorageUnit[];
    mineral: Mineral | undefined;
    constructionSites: ConstructionSite[];
    repairables: Structure[];

    _refreshStructureCache(): void;

    _creepMatrix: CostMatrix;
}

interface ConstructionSite {
    isWalkable: boolean;
}

interface RoomPosition {
    print: string,
    printPlain: string,
    room: Room,
    name: string,
    coordName: string,
    isVisible: boolean;
    rangeToEdge: number;
    isEdge: boolean;
    roomCoords: Coord
    neighbors: RoomPosition[];

    inRangeToXY(x: number, y: number, range: number): boolean;

    getRangeToXY(x: number, y: number): number;

    getPositionsAtRange(range: number, includeWalls?: boolean, includeEdges?: boolean): RoomPosition[];

    getPositionsInRange(range: number, includeWalls?: boolean, includeEdges?: boolean): RoomPosition[];

    inRangeToPos(pos: RoomPosition, range: number): boolean;

    getMultiRoomRangeTo(pos: RoomPosition): number;

    lookForStructure(structureType: StructureConstant): Structure | undefined;

    isWalkable(ignoreCreeps?: boolean): boolean;

    availableNeighbors(ignoreCreeps?: boolean): RoomPosition[];

    getPositionAtDirection(direction: DirectionConstant, range?: number): RoomPosition;

    getOffsetPos(dx: number, dy: number): RoomPosition

    findClosestByLimitedRange<T extends _HasRoomPosition | RoomPosition>(objects: T[], rangeLimit: number,
        opts?: { filter: any | string; }): T | undefined;

    findClosestByMultiRoomRange<T extends _HasRoomPosition>(objects: T[]): T | undefined;

    findClosestByRangeThenPath<T extends _HasRoomPosition>(objects: T[]): T | undefined;
}

interface StructureContainer {
    energy: number;
    isFull: boolean;
    isEmpty: boolean;
}

interface StructureController {
    reservedByMe: boolean;
    signedByMe: boolean;
    signedByScreeps: boolean;

    needsReserving(reserveBuffer: number): boolean;
}

interface StructureStorage {
    energy: number;
    isFull: boolean;
    isEmpty: boolean;
}

interface StructureTerminal {
    energy: any;
    isFull: boolean;
    isEmpty: boolean;
}

interface Tombstone {
    energy: number;
}

interface Structure {
    isWalkable: boolean;
}

interface RoomObject {
    ref: string;
    targetedBy: string[];
}

interface RoomVisual {
    box(x: number, y: number, w: number, h: number, style?: LineStyle): RoomVisual;

    infoBox(info: string[], x: number, y: number, opts?: { [option: string]: any }): RoomVisual;

    multitext(textLines: string[], x: number, y: number, opts?: { [option: string]: any }): RoomVisual;

    structure(x: number, y: number, type: string, opts?: { [option: string]: any }): RoomVisual;

    connectRoads(opts?: { [option: string]: any }): RoomVisual | void;

    speech(text: string, x: number, y: number, opts?: { [option: string]: any }): RoomVisual;

    animatedPosition(x: number, y: number, opts?: { [option: string]: any }): RoomVisual;

    resource(type: ResourceConstant, x: number, y: number, size?: number, opacity?: number): number;

    _fluid(type: string, x: number, y: number, size?: number, opacity?: number): void;

    _mineral(type: string, x: number, y: number, size?: number, opacity?: number): void;

    _compound(type: string, x: number, y: number, size?: number, opacity?: number): void;

    test(): RoomVisual;

    roads: [number, number][] | undefined
}


interface Creep {
    hitsPredicted?: number;
    intel?: { [property: string]: number };
    boosts: _ResourceConstantSansEnergy[];
    boostCounts: { [boostType: string]: number };
    inRampart: boolean;
}

interface String {
    padRight(length: number, char?: string): string;

    padLeft(length: number, char?: string): string;
}

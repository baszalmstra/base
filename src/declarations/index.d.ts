declare var global: any;
declare var Collective: ICollective;
declare var _cache: IGlobalCache;

declare namespace NodeJS {
    interface Global {
        [name: string]: any,

        Collective: ICollective

        _cache: IGlobalCache;
    }
}

declare module 'screeps-profiler';

interface IGlobalCache {
    accessed: { [key: string]: number };
    expiration: { [key: string]: number };
    structures: { [key: string]: Structure[] };
    numbers: { [key: string]: number };
    //lists: { [key: string]: any[] };
    costMatrices: { [key: string]: CostMatrix };
    roomPositions: { [key: string]: RoomPosition | undefined };
    things: { [key: string]: undefined | HasID | HasID[] };
    // objects: { [key: string]: Object };
}

interface ICollective {
    shouldRebuild: boolean;                     // If true force the collective to be rebuild the next tick
    expiration: number;                         // The time at which the collective is forcefully rebuilt
    cache: ICache;                              // Caches commong things at the start of a tick
    director: IDirector;                        // The director that runs the collective
    bases: { [roomName: string]: any }          // A map of all bases by name
    basesByRoom: { [roomName: string]: any }    // A map of all bases by the rooms they own
    directives: { [name: string]: any }         // A map of all directives by name
    foremen: { [name: string]: any }            // A map of foremen by name
    bots: { [creepName: string]: any };         // is actually { [creepName: string]: Bot }
    spawnGroups: { [ref: string]: any };		// is actually { [ref: string]: SpawnGroup }
    terminalNetwork: ITerminalNetwork

    build(): void;
    refresh(): void;

    /**
     * Phase 1: spawning and energy requests
     */
    init(): void;

    /**
     * Phase 2: execute state-changing actions
     */
    run(): void;

    visuals(): void;
}

interface IDirector {
    registerDirective(directive: any): void;
    removeDirective(directive: any): void;

    registerForeman(foreman: any): void;
    removeForeman(foreman: any): void;

    init(): void;
    run(): void;

    getCreepReport(colony: any): string[][];

    visuals(): void;
}

interface TerminalState {
    name: string;
    type: 'in' | 'out' | 'in/out';
    amounts: { [resourceType: string]: number };
    tolerance: number;
}

interface ITerminalNetwork {
    allTerminals: StructureTerminal[];
    readyTerminals: StructureTerminal[];
    // terminals: StructureTerminal[];
    memory: any;

    refresh(): void;

    requestResource(terminal: StructureTerminal, resourceType: ResourceConstant, amount: number): void;

    registerTerminalState(terminal: StructureTerminal, state: TerminalState): void;

    init(): void;

    run(): void;
}

interface ICache {
    foremen: { [foremen: string]: { [roleName: string]: string[] } };
    creepsByBase: { [baseName: string]: Creep[] };
    targets: { [ref: string]: string[] };
    outpostFlags: Flag[];

    build(): void;
    refresh(): void;
}

interface ProtoPos {
    x: number;
    y: number;
    roomName: string;
}

interface ProtoCreep {
    body: BodyPartConstant[];
    name: string;
    memory: any;
}

interface Coord {
    x: number;
    y: number;
}

interface RoomCoord {
    x: number;
    y: number;
    xDir: string;
    yDir: string;
}

interface HasPos {
    pos: RoomPosition;
}

interface HasRef {
    ref: string;
}

interface HasID {
    id: string;
}

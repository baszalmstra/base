type operationMode = 'manual' | 'semiautomatic' | 'automatic';

interface Memory {
    Collective: {},
    colonies: { [name: string]: any };
    creeps: { [name: string]: CreepMemory; };
    flags: { [name: string]: FlagMemory; };
    rooms: { [name: string]: RoomMemory; };
    spawns: { [name: string]: SpawnMemory; };
    pathing: PathingMemory,
    settings: {
        signature: string;
        operationMode: operationMode;
        log: LoggerMemory;
        enableVisuals: boolean;
    };
    constructionSites: { [id: string]: number };
    resetBucket?: boolean;
    haltTick?: number;
}

interface FlagMemory {
    base?: string
    persistent?: boolean
    setPosition?: ProtoPos
    tick?: number
    maxLinearRange?: number
    maxPathLength?: number
    amount?: number;
}

interface LoggerMemory {
    level: number;
    showSource: boolean;
    showTick: boolean;
}

interface CreepMemory {
    foreman: string | null;
    base: string;
    role: string;
    task: ProtoTask | null;
    data: {
        origin: string
    }
    debug?: boolean;
    _go?: MoveData;
    talkative?: boolean;
}

interface ExpansionData {
    score: number;
    bunkerAnchor: string;
    outposts: { [roomName: string]: number };
}

interface RoomMemory {
    expiration?: number;
    tick?: number;
    avoid?: boolean;
    sources?: SavedSource[];
    controller?: SavedController | undefined;
    portals?: SavedPortal[] | undefined;
    mineral?: SavedMineral | undefined;
    lairs?: SavedRoomObject[];
    structures?: {
        towers: string[];
        spawns: string[];
        storage: string | undefined;
        terminal: string | undefined;
        walls: string[];
        ramparts: string[]
    } | undefined;
    exp?: ExpansionData | false;
    invasion?: {
        harvested: number;
        lastSeen: number;
    }
    prevPositions?: { [creepID: string]: ProtoPos };
    creepsInRoom?: { [tick: number]: string[] };
    safety?: SafetyData
}

interface SavedRoomObject {
    c: string; 	// coordinate name
}

interface SavedSource extends SavedRoomObject {
    contnr: string | undefined;
}

interface SavedPortal extends SavedRoomObject {
    dest: string | { shard: string, room: string }; // destination name
    expiration: number; // when portal will decay
}

interface SavedController extends SavedRoomObject {
    level: number;
    owner: string | undefined;
    reservation: {
        username: string,
        ticksToEnd: number,
    } | undefined;
    safeMode: number | undefined;
    safeModeAvailable: number;
    safeModeCooldown: number | undefined;
    progress: number | undefined;
    progressTotal: number | undefined;
}

interface SavedMineral extends SavedRoomObject {
    type: MineralConstant;
    density: number;
}

interface SafetyData {
    safeFor: number;
    unsafeFor: number;
    safety1k: number;
    safety10k: number;
    tick: number;
}

interface MoveData {
    state: any[];
    path: string;
    roomVisibility: { [roomName: string]: boolean };
    delay?: number;
    fleeWait?: number;
    destination?: ProtoPos;
    priority?: number;
    waypoints?: string[];
    waypointsVisited?: string[];
    portaling?: boolean;
}

interface CachedPath {
    path: RoomPosition[];
    length: number;
    tick: number;
}

interface PathingMemory {
    paths: { [originName: string]: { [destinationName: string]: CachedPath; } };
    distances: { [pos1Name: string]: { [pos2Name: string]: number; } };
    weightedDistances: { [pos1Name: string]: { [pos2Name: string]: number; } };
}

import { log } from "../console/log";
import Foreman from "../foremen/Foreman";
import _ from "lodash";
import { MoveOptions, Movement } from "../movement/Movement";
import Task from "../tasks/Task";
import { initializeTask } from "../tasks/initializer";
import Base from "../Base";
import { isBot, isCreep } from "../declarations/typeGuards";
import CombatIntel from "../intel/CombatIntel";

const RANGES = {
    BUILD: 3,
    REPAIR: 3,
    TRANSFER: 1,
    WITHDRAW: 1,
    HARVEST: 1,
    DROP: 0,
};

interface FleeOptions {
    dropEnergy?: boolean;
    invalidateTask?: boolean;
}

// Last pipeline is more complex because it depends on the energy a creep has; sidelining this for now
const actionPipelines: string[][] = [
    ['harvest', 'attack', 'build', 'repair', 'dismantle', 'attackController', 'rangedHeal', 'heal'],
    ['rangedAttack', 'rangedMassAttack', 'build', 'repair', 'rangedHeal'],
    // ['upgradeController', 'build', 'repair', 'withdraw', 'transfer', 'drop'],
];

export function getForeman(creep: Bot | Creep): Foreman | null {
    if (creep.memory.foreman) {
        return Collective.foremen[creep.memory.foreman!] || null;
    } else {
        return null;
    }
}

export function setForeman(creep: Bot | Creep, newForeman: Foreman | null) {
    // Remove cache references to old assignments
    const roleName = creep.memory.role;
    const ref = creep.memory.foreman;
    const oldForeman: Foreman | null = ref ? Collective.foremen[ref] : null;
    if (ref && Collective.cache.foremen[ref] && Collective.cache.foremen[ref][roleName]) {
        _.remove(Collective.cache.foremen[ref][roleName], name => name == creep.name);
    }
    if (newForeman) {
        // Change to the new overlord's colony
        creep.memory.base = newForeman.base.name;
        // Change assignments in memory
        creep.memory.foreman = newForeman.ref;
        // Update the cache references
        if (!Collective.cache.foremen[newForeman.ref]) {
            Collective.cache.foremen[newForeman.ref] = {};
        }
        if (!Collective.cache.foremen[newForeman.ref][roleName]) {
            Collective.cache.foremen[newForeman.ref][roleName] = [];
        }
        Collective.cache.foremen[newForeman.ref][roleName].push(creep.name);
    } else {
        creep.memory.foreman = null;
    }
    if (oldForeman) oldForeman.recalculateCreeps();
    if (newForeman) newForeman.recalculateCreeps();
}

export function normalizeBot(creep: Bot | Creep): Bot | Creep {
    return Collective.bots[creep.name] || creep;
}

export function toCreep(creep: Bot | Creep): Creep {
    return isBot(creep) ? creep.creep : creep;
}

export class Bot {

    id: string;
    creep: Creep;
    body: BodyPartDefinition[];
    carry: StoreDefinition;
    carryCapacity: number;
    name: string;
    memory: CreepMemory
    pos: RoomPosition;
    room: Room;
    nextPos: RoomPosition;                // | The next position the creep will be in after registering a move intent
    spawning: boolean;
    roleName: string;
    ticksToLive: number | undefined
    fatigue: number;
    blockMovement: boolean;               // Whether the Bot is allowed to move or not
    hits: number;
    hitsMax: number;
    actionLog: { [actionName: string]: boolean }; // Tracks the actions that a creep has completed this tick

    private _task: Task | null; 		// Cached Task object that is instantiated once per tick and on change

    constructor(creep: Creep, notifyWhenAttacked = true) {
        this.id = creep.id;
        this.name = creep.name;
        this.creep = creep;
        this.body = creep.body;
        this.memory = creep.memory;
        this.pos = creep.pos;
        this.nextPos = creep.pos;
        this.room = creep.room;
        this.spawning = creep.spawning;
        this.roleName = creep.memory.role;
        this.ticksToLive = creep.ticksToLive;
        this.fatigue = creep.fatigue;
        this.carry = creep.carry;
        this.carryCapacity = creep.carryCapacity;
        this.hits = creep.hits;
        this.hitsMax = creep.hitsMax;
        this.blockMovement = false;
        this._task = null;
        this.actionLog = {};

        // Register global references
        Collective.bots[this.name] = this;
        global[this.name] = this;
    }

    refresh(): void {
        const creep = Game.creeps[this.name];
        if (creep) {
            this.id = creep.id;
            this.creep = creep;
            this.body = creep.body;
            this.memory = creep.memory;
            this.pos = creep.pos;
            this.nextPos = creep.pos;
            this.room = creep.room;
            this.spawning = creep.spawning;
            this.roleName = creep.memory.role;
            this.ticksToLive = creep.ticksToLive;
            this.fatigue = creep.fatigue;
            this.carry = creep.carry;
            this.carryCapacity = creep.carryCapacity;
            this.hits = creep.hits;
            this.hitsMax = creep.hitsMax;
            this.blockMovement = false;
            this.actionLog = {};
        } else {
            log.debug(`Deleting from global`);
            delete Collective.bots[this.name];
            delete global[this.name];
        }
    }

    get print(): string {
        return `<a href="#!/room/${Game.shard.name}/${this.pos.roomName}">[${this.name}]</a>`;
    }

    get base(): Base {
        return Collective.bases[this.memory.base];
    }

    set base(newBase: Base) {
        this.memory.base = newBase.name;
    }

    debug(...args: any[]) {
        if (this.memory.debug) {
            log.debug(this.print, args);
        }
    }

	/**
	 * If the creep is in a colony room or outpost
	 */
    get inColonyRoom(): boolean {
        return Collective.basesByRoom[this.room.name] == this.memory.base;
    }

    get ticksUntilSpawned(): number | undefined {
        if (this.spawning) {
            const spawner = this.pos.lookForStructure(STRUCTURE_SPAWN) as StructureSpawn;
            if (spawner && spawner.spawning) {
                return spawner.spawning.remainingTime;
            } else {
                // Shouldn't ever get here
                log.error(`Error determining ticks to spawn for ${this.name} @ ${this.pos.print}!`);
            }
        }
        return undefined
    }

    get foreman(): Foreman | null {
        return getForeman(this);
    }

    set foreman(newForeman: Foreman | null) {
        setForeman(this, newForeman);
    }

    get isMoving(): boolean {
        const moveData = this.memory._go as MoveData | undefined;
        return !!moveData && !!moveData.path && moveData.path.length > 1;
    }

    // Carry methods

    get hasMineralsInCarry(): boolean {
        for (const resourceType in this.carry) {
            if (resourceType != RESOURCE_ENERGY && (this.carry[<ResourceConstant>resourceType] || 0) > 0) {
                return true;
            }
        }
        return false;
    }

    get task(): Task | null {
        if (!this._task) {
            this._task = this.memory.task ? initializeTask(this.memory.task) : null;
        }
        return this._task;
    }

    set task(task: Task | null) {
        // Unregister target from old task if applicable
        const oldProtoTask = this.memory.task;
        if (oldProtoTask) {
            const oldName = oldProtoTask._target.ref;
            if (Collective.cache.targets[oldName]) {
                _.remove(Collective.cache.targets[oldName], name => name == this.name);
            }
        }
        // Set the new task
        this.memory.task = task ? task.proto : null;
        if (task) {
            if (task.target) {
                // Register task target in cache if it is actively targeting something (excludes goTo and similar)
                if (!Collective.cache.targets[task.target.ref]) {
                    Collective.cache.targets[task.target.ref] = [];
                }
                Collective.cache.targets[task.target.ref].push(this.name);
            }
            // Register references to creep
            task.creep = this;
        }
        // Clear cache
        this._task = null;
    }

    reassign(newForeman: Foreman | null, newRole: string, invalidateTask = true) {
        this.foreman = newForeman;
        this.roleName = newRole;
        this.memory.role = newRole;
        // if (invalidateTask) {
        //     this.task = null
        // }
    }

    getActiveBodyparts(type: BodyPartConstant): number {
        return this.creep.getActiveBodyparts(type);
    }

    /* The same as creep.getActiveBodyparts, but just counts bodyparts regardless of condition. */
    getBodyparts(partType: BodyPartConstant): number {
        return _.filter(this.body, (part: BodyPartDefinition) => part.type == partType).length;
    }

    goTo(destination: RoomPosition | HasPos, options: MoveOptions = {}) {
        return Movement.goTo(this, destination, options);
    }

    goToRoom(roomName: string, options: MoveOptions = {}) {
        return Movement.goToRoom(this, roomName, options);
    }

    inSameRoomAs(target: HasPos): boolean {
        return this.pos.roomName == target.pos.roomName;
    }

    safelyInRoom(roomName: string): boolean {
        return this.room.name == roomName && !this.pos.isEdge;
    }

    // get inRampart(): boolean {
    // 	return this.creep.inRampart;
    // }

    /**
	 * Flee from hostiles in the room, while not repathing every tick
	 */
    flee(avoidGoals: (RoomPosition | HasPos)[] = this.room.fleeDefaults,
        fleeOptions: FleeOptions = {},
        moveOptions: MoveOptions = {}): boolean {
        if (avoidGoals.length == 0) {
            return false;
        } else if (this.room.controller && this.room.controller.my && this.room.controller.safeMode) {
            return false;
        } else {
            const fleeing = Movement.flee(this, avoidGoals, fleeOptions.dropEnergy, moveOptions) != undefined;
            if (fleeing) {
                // Drop energy if needed
                if (fleeOptions.dropEnergy && this.carry.energy > 0) {
                    const nearbyContainers = this.pos.findInRange(this.room.storageUnits, 1);
                    if (nearbyContainers.length > 0) {
                        this.transfer(_.first(nearbyContainers)!, RESOURCE_ENERGY);
                    } else {
                        this.drop(RESOURCE_ENERGY);
                    }
                }
                // Invalidate task
                if (fleeOptions.invalidateTask) {
                    this.task = null;
                }
            }
            return fleeing;
        }
    }

    park(pos: RoomPosition = this.pos, maintainDistance = false): number {
        return Movement.park(this, pos, maintainDistance);
    }

    /**
	 * Moves a creep off of the current tile to the first available neighbor
	 */
    moveOffCurrentPos(): number | undefined {
        return Movement.moveOffCurrentPos(this);
    }

	/**
	 * Moves onto an exit tile
	 */
    moveOnExit(): ScreepsReturnCode | undefined {
        return Movement.moveOnExit(this);
    }

	/**
	 * Moves off of an exit tile
	 */
    moveOffExit(avoidSwamp = true): ScreepsReturnCode {
        return Movement.moveOffExit(this, avoidSwamp);
    }

    moveOffExitToward(pos: RoomPosition, detour = true): number | undefined {
        return Movement.moveOffExitToward(this, pos, detour);
    }

    harvest(source: Source | Mineral) {
        const result = this.creep.harvest(source);
        if (!this.actionLog.harvest) this.actionLog.harvest = (result == OK);
        return result;
    }

    goHarvest(source: Source | Mineral) {
        if (this.pos.inRangeToPos(source.pos, RANGES.HARVEST)) {
            return this.harvest(source);
        } else {
            return this.goTo(source);
        }
    }

    drop(resourceType: ResourceConstant, amount?: number) {
        const result = this.creep.drop(resourceType, amount);
        if (!this.actionLog.drop) this.actionLog.drop = (result == OK);
        return result;
    }

    goDrop(pos: RoomPosition, resourceType: ResourceConstant, amount?: number) {
        if (this.pos.inRangeToPos(pos, RANGES.DROP)) {
            return this.drop(resourceType, amount);
        } else {
            return this.goTo(pos);
        }
    }

    dismantle(target: Structure): CreepActionReturnCode {
        const result = this.creep.dismantle(target);
        if (!this.actionLog.dismantle) this.actionLog.dismantle = (result == OK);
        return result;
    }

    attack(target: Creep | Structure) {
        const result = this.creep.attack(target);
        if (result == OK) {
            //this.actionLog.attack = true;
            if (isCreep(target)) {
                if (target.hitsPredicted == undefined) target.hitsPredicted = target.hits;
                target.hitsPredicted -= CombatIntel.predictedDamageAmount(this.creep, target, 'attack');
                // account for hitback effects
                if (this.creep.hitsPredicted == undefined) this.creep.hitsPredicted = this.creep.hits;
                this.creep.hitsPredicted -= CombatIntel.predictedDamageAmount(target, this.creep, 'attack');
            }
            if (this.memory.talkative) this.say(`💥`);
        }
        return result;
    }

    attackController(controller: StructureController) {
        const result = this.creep.attackController(controller);
        if (!this.actionLog.attackController) this.actionLog.attackController = (result == OK);
        return result;
    }

    build(target: ConstructionSite) {
        const result = this.creep.build(target);
        if (!this.actionLog.build) this.actionLog.build = (result == OK);
        return result;
    }

    goBuild(target: ConstructionSite) {
        if (this.pos.inRangeToPos(target.pos, RANGES.BUILD)) {
            return this.build(target);
        } else {
            return this.goTo(target);
        }
    }

    heal(target: Creep | Bot, rangedHealInstead = false) {
        if (rangedHealInstead && !this.pos.isNearTo(target)) {
            return this.rangedHeal(target);
        }
        const creep = toCreep(target);
        const result = this.creep.heal(creep);
        if (result == OK) {
            //this.actionLog.heal = true;
            if (creep.hitsPredicted == undefined) creep.hitsPredicted = creep.hits;
            creep.hitsPredicted += CombatIntel.getHealAmount(this);
            if (this.memory.talkative) this.say('🚑');
        }
        return result;
    }

    rangedHeal(target: Creep | Bot) {
        const creep = toCreep(target);
        const result = this.creep.rangedHeal(creep);
        if (result == OK) {
            //this.actionLog.rangedHeal = true;
            if (creep.hitsPredicted == undefined) creep.hitsPredicted = creep.hits;
            creep.hitsPredicted += CombatIntel.getRangedHealAmount(this);
            if (this.memory.talkative) this.say(`💉`);
        }
        return result;
    }

    transfer(target: Creep | Bot | Structure, resourceType: ResourceConstant = RESOURCE_ENERGY, amount?: number) {
        let result: ScreepsReturnCode;
        if (target instanceof Bot) {
            result = this.creep.transfer(target.creep, resourceType, amount);
        } else {
            result = this.creep.transfer(target, resourceType, amount);
        }
        if (!this.actionLog.transfer) this.actionLog.transfer = (result == OK);
        return result;
    }

    goTransfer(target: Creep | Bot | Structure, resourceType: ResourceConstant = RESOURCE_ENERGY, amount?: number) {
        if (this.pos.inRangeToPos(target.pos, RANGES.TRANSFER)) {
            return this.transfer(target, resourceType, amount);
        } else {
            return this.goTo(target);
        }
    }

    withdraw(target: Structure | Tombstone, resourceType: ResourceConstant = RESOURCE_ENERGY, amount?: number) {
        const result = this.creep.withdraw(target, resourceType, amount);
        if (!this.actionLog.withdraw) this.actionLog.withdraw = (result == OK);
        return result;
    }

    goWithdraw(target: Structure | Tombstone, resourceType: ResourceConstant = RESOURCE_ENERGY, amount?: number) {
        if (this.pos.inRangeToPos(target.pos, RANGES.WITHDRAW)) {
            return this.withdraw(target, resourceType, amount);
        } else {
            return this.goTo(target);
        }
    }

    pickup(resource: Resource) {
        const result = this.creep.pickup(resource);
        if (!this.actionLog.pickup) this.actionLog.pickup = (result == OK);
        return result;
    }

    repair(target: Structure) {
        const result = this.creep.repair(target);
        if (!this.actionLog.repair) this.actionLog.repair = (result == OK);
        return result;
    }

    goRepair(target: Structure) {
        if (this.pos.inRangeToPos(target.pos, RANGES.REPAIR)) {
            return this.repair(target);
        } else {
            return this.goTo(target);
        }
    }

    reserveController(controller: StructureController) {
        const result = this.creep.reserveController(controller);
        if (!this.actionLog.reserveController) this.actionLog.reserveController = (result == OK);
        return result;
    }

    claimController(controller: StructureController) {
        const result = this.creep.claimController(controller);
        if (!this.actionLog.claimController) this.actionLog.claimController = (result == OK);
        return result;
    }

    /* Say a message; maximum message length is 10 characters */
    say(message: string, pub?: boolean) {
        return this.creep.say(message, pub);
    }

    signController(target: StructureController, text: string) {
        const result = this.creep.signController(target, text);
        if (!this.actionLog.signController) this.actionLog.signController = (result == OK);
        return result;
    }

    suicide() {
        return this.creep.suicide();
    }

    upgradeController(controller: StructureController) {
        const result = this.creep.upgradeController(controller);
        if (!this.actionLog.upgradeController) this.actionLog.upgradeController = (result == OK);
        // Determine amount of upgrade power
        // let weightedUpgraderParts = _.map(this.boostCounts, )
        // let upgradeAmount = this.getActiveBodyparts(WORK) * UPGRADE_CONTROLLER_POWER;
        // let upgrade

        // Stats.accumulate(`colonies.${this.colony.name}.rcl.progressTotal`, upgradeAmount);
        return result;
    }

    /**
	 * Determine whether the given action will conflict with an action the creep has already taken.
	 * See http://docs.screeps.com/simultaneous-actions.html for more details.
	 */
    canExecute(actionName: string): boolean {
        // Only one action can be executed from within a single pipeline
        let conflictingActions: string[] = [actionName];
        for (const pipeline of actionPipelines) {
            if (pipeline.includes(actionName)) conflictingActions = conflictingActions.concat(pipeline);
        }
        for (const action of conflictingActions) {
            if (this.actionLog[action]) {
                return false;
            }
        }
        return true;
    }

    /**
	 * Does the creep have a valid task at the moment?
	 */
    get hasValidTask(): boolean {
        return !!this.task && this.task.isValid();
    }

	/**
	 * Creeps are idle if they don't have a task.
	 */
    get isIdle(): boolean {
        return !this.task || !this.task.isValid();
    }

    /**
	 * Execute the task you currently have.
	 */
    run(): number | undefined {
        if (this.task) {
            return this.task.run();
        }
        return undefined
    }

    move(direction: DirectionConstant, force = false) {
        if (!this.blockMovement && !force) {
            const result = this.creep.move(direction);
            if (result == OK) {
                if (!this.actionLog.move) this.actionLog.move = true;
                this.nextPos = this.pos.getPositionAtDirection(direction);
            }
            return result;
        } else {
            return ERR_BUSY;
        }
    }

    // Boosting logic --------------------------------------------------------------------------------------------------

    get boosts(): _ResourceConstantSansEnergy[] {
        return this.creep.boosts;
    }

    get boostCounts(): { [boostType: string]: number } {
        return this.creep.boostCounts;
    }

    get needsBoosts(): boolean {
        // if (this.foreman) {
        // 	return this.foreman.shouldBoost(this);
        // }
        return false;
    }
}

import { Bot } from "./Bot";
import _ from "lodash";
import CombatTargeting from "../targeting/CombatTargeting";
import { profile } from "../profiler";
import CombatIntel from "../intel/CombatIntel";

export const DEFAULT_PARTNER_TICK_DIFFERENCE = 650;
export const DEFAULT_SWARM_TICK_DIFFERENCE = 500;

interface CombatZergMemory extends CreepMemory {
    recovering: boolean;
    lastInDanger: number;
    partner?: string;
    swarm?: string;
}

@profile
export default class BattleBot extends Bot {
    memory!: CombatZergMemory;
    isBattleBot: boolean;

    constructor(creep: Creep, notifyWhenAttacked = true) {
        super(creep, notifyWhenAttacked);
        this.isBattleBot = true;
        _.defaults(this.memory, {
            recovering: false,
            lastInDanger: 0,
            targets: {}
        });
    }

    findPartner(partners: BattleBot[], tickDifference = DEFAULT_PARTNER_TICK_DIFFERENCE): BattleBot | undefined {
        if (this.memory.partner) {
            const partner = _.find(partners, partner => partner.name == this.memory.partner);
            if (partner) {
                return partner;
            } else {
                delete this.memory.partner;
                this.findPartner(partners, tickDifference);
            }
        } else {
            let partner = _.find(partners, partner => partner.memory.partner == this.name);
            if (!partner) {
                partner = _(partners)
                    .filter(partner => !partner.memory.partner &&
                        Math.abs((this.ticksToLive || CREEP_LIFE_TIME)
                            - (partner.ticksToLive || CREEP_LIFE_TIME)) <= tickDifference)
                    .minBy(partner => Math.abs((this.ticksToLive || CREEP_LIFE_TIME)
                        - (partner.ticksToLive || CREEP_LIFE_TIME)));
            }
            if (_.isObject(partner)) {
                this.memory.partner = partner.name;
                partner.memory.partner = this.name;
                return partner;
            }
        }
        return undefined
    }

    doMedicActions(roomName: string): void {
        // Travel to the target room
        if (!this.safelyInRoom(roomName)) {
            this.goToRoom(roomName, { ensurePath: true });
            return;
        }
        // Heal friendlies
        const target = CombatTargeting.findClosestHurtFriendly(this);
        if (target) {
            // Approach the target
            const range = this.pos.getRangeTo(target);
            if (range > 1) {
                this.goTo(target, { movingTarget: true });
            }

            // Heal or ranged-heal the target
            if (range <= 1) {
                this.heal(target);
            } else if (range <= 3) {
                this.rangedHeal(target);
            }
        } else {
            this.park();
        }
    }

    healSelfIfPossible(): CreepActionReturnCode | undefined {
        // Heal yourself if it won't interfere with attacking
        if (this.canExecute('heal')
            && (this.hits < this.hitsMax || this.pos.findInRange(this.room.hostiles, 3).length > 0)) {
            return this.heal(this);
        }
        return undefined
    }

    /**
	 * Attack and chase the specified target
	 */
    attackAndChase(target: Creep | Structure): CreepActionReturnCode {
        let ret: CreepActionReturnCode;
        // Attack the target if you can, else move to get in range
        if (this.pos.isNearTo(target)) {
            ret = this.attack(target);
            // Move in the direction of the creep to prevent it from running away
            this.move(this.pos.getDirectionTo(target));
            return ret;
        } else {
            if (this.pos.getRangeTo(target.pos) > 10 && target instanceof Creep) {
                this.goTo(target, { movingTarget: true });
            } else {
                this.goTo(target);
            }
            return ERR_NOT_IN_RANGE;
        }
    }

    /**
	 * Automatically melee-attack the best creep in range
	 */
    autoMelee(possibleTargets = this.room.hostiles) {
        const target = CombatTargeting.findBestCreepTargetInRange(this, 1, possibleTargets)
            || CombatTargeting.findBestStructureTargetInRange(this, 1);
        this.debug(`Melee target: ${target}`);
        if (target) {
            return this.attack(target);
        }
        return undefined;
    }

	/**
	 * Automatically heal the best creep in range
	 */
    autoHeal(allowRangedHeal = true, friendlies = this.room.creeps) {
        const target = CombatTargeting.findBestHealingTargetInRange(this, allowRangedHeal ? 3 : 1, friendlies);
        this.debug(`Heal taget: ${target}`);
        if (target) {
            if (this.pos.getRangeTo(target) <= 1) {
                return this.heal(target);
            } else if (allowRangedHeal && this.pos.getRangeTo(target) <= 3) {
                return this.rangedHeal(target);
            }
        }
        return undefined;
    }

    needsToRecover(recoverThreshold = CombatIntel.minimumDamageTakenMultiplier(this.creep) < 1 ? 0.85 : 0.75,
        reengageThreshold = 1.0): boolean {
        let recovering: boolean;
        if (this.memory.recovering) {
            recovering = this.hits < this.hitsMax * reengageThreshold;
        } else {
            recovering = this.hits < this.hitsMax * recoverThreshold;
        }
        this.memory.recovering = recovering;
        return recovering;
    }
}

import Base from "../Base";
import Foreman from "../foremen/Foreman";

/**
 * Represents a cluster of one or more buildings that together perform a specific task.
 */
export default abstract class StructureGroup {
    base: Base;                         // Base the group belongs to
    room: Room;                         // Room of the group (not necessarily base.room)
    pos: RoomPosition;                  // Location of the group
    ref: string;
    memory: any;
    foreman: Foreman | undefined;

    constructor(base: Base, instantiationObject: RoomObject, name: string, withPos: false = false) {
        this.base = base;
        this.room = instantiationObject.room!;
        this.pos = instantiationObject.pos;
        this.ref = withPos ? `${name}@${instantiationObject.pos.name}` : `${name}@${this.base.name}`;
        this.base.structureGroups.push(this)
    }

    get print(): string {
        return `<a href="#!/room/${Game.shard.name}/${this.pos.roomName}">[${this.ref}]</a>`
    }

    abstract spawnForemen(): void;

    // Logic to refresh the state of the group between ticks
    abstract refresh(): void;

    // Pre-run logic, such as registering energy requests
    abstract init(): void;

    // Runtime logic; controlling creap actions
    abstract run(): void;
}

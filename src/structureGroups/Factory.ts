import StructureGroup from "./StructureGroup";
import Base, { BaseStage } from "../Base";
import { CreepSetup, bodyCost } from "../creepSetups/CreepSetup";
import Foreman from "../foremen/Foreman";
import _ from "lodash";
import { log } from "../console/log";
import FactoryForeman from "../foremen/Factory";
import TransportRequestGroup from "../logistics/TransportRequestGroup";
import { Priority } from "../priorities/priorities";
import $ from "../GlobalCache";
import { hasMinerals } from "../utilities/util";
import { insideBunkerBounds, allBunkerCoords, getPosFromBunkerCoord } from "../roomPlanner/layouts/bunker";
import { names } from "../utilities/names";
import Visualizer from "../visuals/Visualizer";
import { profile } from "../profiler";

const ERR_ROOM_ENERGY_CAPACITY_NOT_ENOUGH = -20;
const ERR_SPECIFIED_SPAWN_BUSY = -21;

export interface SpawnRequest {
    setup: CreepSetup;                  // creep body generator to use
    foreman: Foreman;                   // foreman requesting the creep
    priority: number;                   // priority of the request // TODO: WIP
    partners?: CreepSetup[];            // partners to spawn along with the creep
    options?: SpawnRequestOptions;      // options
}

export interface SpawnRequestOptions {
    spawn?: StructureSpawn;             // allows you to specify which spawn to use; only use for high priority
    directions?: DirectionConstant[];   // StructureSpawn.spawning.directions
}

interface SpawnOrder {
    protoCreep: ProtoCreep;
    options: SpawnOptions | undefined;
}

@profile
export default class Factory extends StructureGroup {
    spawns: StructureSpawn[];           // List of spawns in the hatchery
    availableSpawns: StructureSpawn[];  // Spawns that are available to make stuff right now
    extensions: StructureExtension[];
    energyStructures!: (StructureSpawn | StructureExtension)[]; 	// All spawns and extensions
    battery: StructureContainer | undefined;				// The container to provide an energy buffer
    towers: StructureTower[]; 								// All towers that aren't in the command center
    transportRequests: TransportRequestGroup;
    foreman!: FactoryForeman;

    private _nextAvailability: number | undefined;
    // private _queuedSpawnTime: number | undefined;
    private productionPriorities: number[];
    private productionQueue: {								// Prioritized spawning queue
        [priority: number]: SpawnOrder[]
    };
    private isOverloaded: boolean;

    settings!: {												// Settings for hatchery operation
        refillTowersBelow: number,  							// What value to refill towers at?
        linksRequestEnergyBelow: number, 						// What value will links store more energy at?
        suppressSpawning: boolean,             					// Prevents the hatchery from spawning this tick
    };

    constructor(base: Base, headSpawn: StructureSpawn) {
        super(base, headSpawn, "botfactory");
        this.spawns = base.spawns;
        this.availableSpawns = _.filter(this.spawns, spawn => !spawn.spawning);
        this.extensions = base.extensions;
        this.towers = base.towers;

        if (this.base.roomPlanner.memory.bunkerData && this.base.roomPlanner.memory.bunkerData.anchor) {
            this.battery = _.first(_.filter(this.room.containers, cont => insideBunkerBounds(cont.pos, this.base))) || undefined;
            $.set(this, 'energyStructures', () => this.computeEnergyStructures());
        } else {
            this.battery = this.pos.findClosestByLimitedRange(this.room.containers, 2);
            this.energyStructures = (<(StructureSpawn | StructureExtension)[]>[]).concat(this.spawns, this.extensions);
        }
        this.productionPriorities = [];
        this.productionQueue = {};
        this.isOverloaded = false;

        this.settings = {
            refillTowersBelow: 750,
            linksRequestEnergyBelow: 0,
            suppressSpawning: false,
        };

        this.transportRequests = base.transportRequests;
    }

    refresh(): void {
        $.refreshRoom(this);
        $.refresh(this, "spawns", "extensions", "energyStructures", "battery", "towers")
        this.availableSpawns = _.filter(this.spawns, spawn => !spawn.spawning);

        this.isOverloaded = false;
        this.productionPriorities = [];
        this.productionQueue = {};
    }

    // Idle position for queen
    get idlePos(): RoomPosition {
        if (this.battery) {
            return this.battery.pos;
        } else {
            return this.spawns[0].pos.availableNeighbors(true)[0];
        }
    }

    computeEnergyStructures(): (StructureSpawn | StructureExtension)[] {
        const positions = _.map(allBunkerCoords[this.base.level], coord => getPosFromBunkerCoord(coord, this.base));
        let spawnsAndExtensions: (StructureSpawn | StructureExtension)[] = [];
        spawnsAndExtensions = spawnsAndExtensions.concat(this.spawns, this.extensions);
        const energyStructures: (StructureSpawn | StructureExtension)[] = [];
        for (const pos of positions) {
            const structure = _.find(pos.lookFor(LOOK_STRUCTURES), s =>
                s.structureType == STRUCTURE_SPAWN
                || s.structureType == STRUCTURE_EXTENSION) as StructureSpawn | StructureExtension;
            if (structure) {
                energyStructures.push(_.remove(spawnsAndExtensions, s => s.id == structure.id)[0]);
            }
        }
        return _.compact(energyStructures.concat(spawnsAndExtensions));
    }

    /* Generate (but not spawn) the largest creep possible, returns the protoCreep as an object */
    private generateProtoCreep(setup: CreepSetup, foreman: Foreman): ProtoCreep {
        // Generate the creep body
        let creepBody: BodyPartConstant[];
        // if (foreman.colony.incubator) { // if you're being incubated, build as big a creep as you want
        //     creepBody = setup.generateBody(foreman.colony.incubator.room.energyCapacityAvailable);
        // } else { // otherwise limit yourself to actual energy constraints
        creepBody = setup.generateBody(this.room.energyCapacityAvailable);
        // }
        // Generate the creep memory
        const creepMemory: CreepMemory = {
            base: foreman.base.name,                 // name of the colony the creep is assigned to
            foreman: foreman.ref,                    // name of the Overlord running this creep
            role: setup.role,                        // role of the creep
            task: null,                              // task the creep is performing
            data: {                                  // rarely-changed data about the creep
                origin: '',                          // where it was spawned, filled in at spawn time
            },
        };
        // Create the protocreep and return it
        const protoCreep: ProtoCreep = {                        // object to add to spawner queue
            body: creepBody,                                  // body array
            name: setup.role,                                 // name of the creep - gets modified by hatchery
            memory: creepMemory,                                // memory to initialize with
        };
        return protoCreep;
    }

    private generateCreepName(roleName: string): string {
        // Generate a creep name based on the role and add a suffix to make it unique
        let name = "";
        do {
            name = `${roleName}_${_.sample(names)}`
        } while (Game.creeps[name])
        return name;
    }

    private spawnCreep(protoCreep: ProtoCreep, options: SpawnRequestOptions = {}): number {
        // get a spawn to use
        let spawnToUse: StructureSpawn | undefined;
        if (options.spawn) {
            spawnToUse = options.spawn;
            if (spawnToUse.spawning) {
                return ERR_SPECIFIED_SPAWN_BUSY;
            } else {
                _.remove(this.availableSpawns, spawn => spawn.id == spawnToUse!.id); // mark as used
            }
        } else {
            spawnToUse = this.availableSpawns.shift();
        }
        if (spawnToUse) { // if there is a spawn, create the creep
            if (this.base.bunker && this.base.bunker.coreSpawn
                && spawnToUse.id == this.base.bunker.coreSpawn.id && !options.directions
                && this.base.level == 8) {
                options.directions = [BOTTOM, RIGHT]; // don't spawn into the manager spot
            }

            protoCreep.name = this.generateCreepName(protoCreep.name); // modify the creep name to make it unique
            if (bodyCost(protoCreep.body) > _.sumBy(this.energyStructures, struct => struct.energy)) {
                return ERR_ROOM_ENERGY_CAPACITY_NOT_ENOUGH;
            }
            protoCreep.memory.data.origin = spawnToUse.pos.roomName;
            const result = spawnToUse.spawnCreep(protoCreep.body, protoCreep.name, {
                memory: protoCreep.memory,
                energyStructures: this.energyStructures,
                directions: options.directions
            });
            log.debug(`${this.print}: Spawning ${protoCreep.name}: ${result}`)
            if (result == OK) {
                return result;
            } else {
                this.availableSpawns.unshift(spawnToUse); // return the spawn to the available spawns list
                return result;
            }
        } else { // otherwise, return busy
            return ERR_BUSY;
        }
    }

    canSpawn(body: BodyPartConstant[]): boolean {
        return bodyCost(body) <= this.room.energyCapacityAvailable;
    }

    /* Returns the approximate aggregated time at which the hatchery will next be available to spawn something */
    get nextAvailability(): number {
        if (!this._nextAvailability) {
            const allQueued = _.flatten(_.values(this.productionQueue)) as SpawnOrder[];
            const queuedSpawnTime = _.sumBy(allQueued, order => order.protoCreep.body.length) * CREEP_SPAWN_TIME;
            const activeSpawnTime = _.sumBy(this.spawns, spawn => spawn.spawning ? spawn.spawning.remainingTime : 0);
            this._nextAvailability = (activeSpawnTime + queuedSpawnTime) / this.spawns.length;
        }
        return this._nextAvailability;
    }

    /* Enqueues a request to the hatchery */
    enqueue(request: SpawnRequest): void {
        const protoCreep = this.generateProtoCreep(request.setup, request.foreman);
        const priority = request.priority;
        if (this.canSpawn(protoCreep.body) && protoCreep.body.length > 0) {
            // Spawn the creep yourself if you can
            this._nextAvailability = undefined; // invalidate cache
            // this._queuedSpawnTime = undefined;
            if (!this.productionQueue[priority]) {
                this.productionQueue[priority] = [];
                this.productionPriorities.push(priority); // this is necessary because keys interpret number as string
            }
            this.productionQueue[priority].push({ protoCreep: protoCreep, options: request.options });
        } else {
            log.debug(`${this.room.print}: cannot spawn creep ${protoCreep.name} with body ` +
                `${JSON.stringify(protoCreep.body)}!`);
        }
    }

    private spawnHighestPriorityCreep(): number | undefined {
        const sortedKeys = _.sortBy(this.productionPriorities);
        for (const priority of sortedKeys) {

            // if (this.colony.defcon >= DEFCON.playerInvasion
            // 	&& !this.colony.controller.safeMode
            // 	&& priority > OverlordPriority.warSpawnCutoff) {
            // 	continue; // don't spawn non-critical creeps during wartime
            // }

            const nextOrder = this.productionQueue[priority].shift();
            if (nextOrder) {
                const { protoCreep, options } = nextOrder;
                const result = this.spawnCreep(protoCreep, options);
                if (result == OK) {
                    return result;
                } else if (result == ERR_SPECIFIED_SPAWN_BUSY) {
                    return result; // continue to spawn other things while waiting on specified spawn
                } else {
                    // If there's not enough energyCapacity to spawn, ignore it and move on, otherwise block and wait
                    if (result != ERR_ROOM_ENERGY_CAPACITY_NOT_ENOUGH) {
                        this.productionQueue[priority].unshift(nextOrder);
                        return result;
                    }
                }
            }
        }
        return undefined
    }

    private handleSpawns(): void {
        // Spawn all queued creeps that you can
        while (this.availableSpawns.length > 0) {
            const result = this.spawnHighestPriorityCreep();
            if (result == ERR_NOT_ENOUGH_ENERGY) { // if you can't spawn something you want to
                this.isOverloaded = true;
            }
            if (result != OK && result != ERR_BUSY) {
                // Can't spawn creep right now
                break;
            }
        }
    }

    private registerEnergyRequests() {
        if (this.battery) {
            const threshold = this.base.stage == BaseStage.Small ? 0.75 : 0.5;
            if (this.battery.energy < threshold * this.battery.storeCapacity) {
                this.base.logisticsNetwork.requestInput(this.battery, { multiplier: 1.5 });
            }
            // get rid of any minerals in the container if present
            if (hasMinerals(this.battery.store)) {
                this.base.logisticsNetwork.requestOutputMinerals(this.battery);
            }
        }

        _.forEach(this.energyStructures, struct => this.transportRequests.requestInput(struct, Priority.NormalLow));

        // let refillSpawns = _.filter(this.spawns, spawn => spawn.energy < spawn.energyCapacity);
        // let refillExtensions = _.filter(this.extensions, extension => extension.energy < extension.energyCapacity);
        const refillTowers = _.filter(this.towers, tower => tower.energy < this.settings.refillTowersBelow);
        // _.forEach(refillSpawns, spawn => this.transportRequests.requestInput(spawn, Priority.NormalLow));
        // _.forEach(refillExtensions, extension => this.transportRequests.requestInput(extension, Priority.NormalLow));
        _.forEach(refillTowers, tower => this.transportRequests.requestInput(tower, Priority.NormalLow));
    }

    spawnForemen(): void {
        this.foreman = new FactoryForeman(this);
    }
    init(): void {
        this.registerEnergyRequests()
    }
    run(): void {
        this.handleSpawns();
    }

    visuals(coord: Coord): Coord {
        let { x, y } = coord;
        const spawning: string[] = [];
        const spawnProgress: [number, number][] = [];
        _.forEach(this.spawns, function (spawn) {
            if (spawn.spawning) {
                spawning.push(spawn.spawning.name.split('_')[0]);
                const timeElapsed = spawn.spawning.needTime - spawn.spawning.remainingTime;
                spawnProgress.push([timeElapsed, spawn.spawning.needTime]);
            }
        });
        const boxCoords = Visualizer.section(`${this.base.name} Hatchery`, { x, y, roomName: this.room.name },
            9.5, 1 + spawning.length + .1);
        const boxX = boxCoords.x;
        y = boxCoords.y + 0.25;

        // Log energy
        Visualizer.text('Energy', { x: boxX, y: y, roomName: this.room.name });
        Visualizer.barGraph([this.room.energyAvailable, this.room.energyCapacityAvailable],
            { x: boxX + 4, y: y, roomName: this.room.name }, 5);
        y += 1;

        // // Log uptime
        // const uptime = this.memory.stats.uptime;
        // Visualizer.text('Uptime', {x: boxX, y: y, roomName: this.room.name});
        // Visualizer.barGraph(uptime, {x: boxX + 4, y: y, roomName: this.room.name}, 5);
        // y += 1;

        // // Log overload status
        // const overload = this.memory.stats.overload;
        // Visualizer.text('Overload', {x: boxX, y: y, roomName: this.room.name});
        // Visualizer.barGraph(overload, {x: boxX + 4, y: y, roomName: this.room.name}, 5);
        // y += 1;

        for (const i in spawning) {
            Visualizer.text(spawning[i], { x: boxX, y: y, roomName: this.room.name });
            Visualizer.barGraph(spawnProgress[i], { x: boxX + 4, y: y, roomName: this.room.name }, 5);
            y += 1;
        }
        return { x: x, y: y + .25 };
    }
}

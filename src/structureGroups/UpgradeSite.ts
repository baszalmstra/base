import StructureGroup from "./StructureGroup";
import Base, { BaseStage } from "../Base";
import Mem from "../Memory";
import $ from "../GlobalCache";
import _ from "lodash";
import { log } from "../console/log";
import UpgradeForeman from "../foremen/Upgrade";
import { hasMinerals } from "../utilities/util";
import { profile } from "../profiler";

interface UpgradeSiteMemory {
    stats: { downtime: number };
}

@profile
export default class UpgradeSite extends StructureGroup {
    memory: UpgradeSiteMemory;
    controller: StructureController;
    upgradePowerNeeded: number;
    link: StructureLink | undefined;
    battery: StructureContainer | undefined;
    batteryPos: RoomPosition | undefined;

    foreman!: UpgradeForeman

    static settings = {
        energyBuffer: 100000,       // Number of upgrader parts scales with energy - this value
        energyPerBodyUnit: 10000,   // Scaling factor; this much excess energy adds one extra body repetition
        minLinkDistance: 10,        // Required distance to build link
        linksRequestBelow: 200,     // Links request energy when less than this amount
    }

    constructor(base: Base, controller: StructureController) {
        super(base, controller, "upgradeSite");
        this.controller = controller;
        this.memory = Mem.wrap(this.base.memory, 'upgradeSite');
        this.upgradePowerNeeded = this.getUpgradePowerNeeded();
        $.set(this, "battery", () => {
            const allowableContainers = _.filter(this.room.containers, container =>
                container.pos.findInRange(FIND_SOURCES, 1).length == 0);
            return this.pos.findClosestByLimitedRange(allowableContainers, 3);
        });
        this.batteryPos = $.pos(this, "batteryPos", () => {
            if (this.battery) {
                return this.battery.pos;
            }
            const inputSite = this.findInputConstructionSite();
            if (inputSite) {
                return inputSite.pos;
            }
            return this.calculateBatteryPos() || log.alert(`Upgrade site at ${this.pos.print}: no batteryPos`)
        })
        if (this.batteryPos) this.base.destinations.push({ pos: this.batteryPos, order: 0 });
        $.set(this, "link", () => this.pos.findClosestByLimitedRange(base.availableLinks, 3));
    }

    refresh() {
        this.memory = Mem.wrap(this.base.memory, "upgradeSite");
        $.refreshRoom(this);
        $.refresh(this, "controller", "battery"/*, "link" */);
    }

    private getUpgradePowerNeeded(): number {
        return $.number(this, 'upgradePowerNeeded', () => {
            if (this.room.storage) { // Workers perform upgrading until storage is set up
                const amountOver = Math.max(this.base.assets.energy - UpgradeSite.settings.energyBuffer, 0);
                let upgradePower = 1 + Math.floor(amountOver / UpgradeSite.settings.energyPerBodyUnit);
                if (amountOver > 800000) {
                    upgradePower *= 4; // double upgrade power if we have lots of surplus energy
                } else if (amountOver > 500000) {
                    upgradePower *= 2;
                }
                if (this.controller.level == 8) {
                    upgradePower = Math.min(upgradePower, 15); // don't go above 15 work parts at RCL 8
                }
                return upgradePower;
            } else {
                return 0;
            }
        });
    }

    findInputConstructionSite(): ConstructionSite | undefined {
        const nearbyInputSites = this.pos.findInRange(this.room.constructionSites, 4, {
            filter: (s: ConstructionSite) => s.structureType == STRUCTURE_CONTAINER ||
                s.structureType == STRUCTURE_LINK,
        });
        return _.first(nearbyInputSites);
    }

    /* Calculate where the input will be built for this site */
    private calculateBatteryPos(): RoomPosition | undefined {
        let originPos: RoomPosition | undefined;
        if (this.base.storage) {
            originPos = this.base.storage.pos;
        } else if (this.base.roomPlanner.storagePos) {
            originPos = this.base.roomPlanner.storagePos;
        } else {
            return;
        }
        // Find all positions at range 2 from controller
        let inputLocations: RoomPosition[] = [];
        for (const pos of this.pos.getPositionsAtRange(2)) {
            if (pos.isWalkable(true)) {
                inputLocations.push(pos);
            }
        }
        // Try to find locations where there is maximal standing room
        const maxNeighbors = _.max(_.map(inputLocations, pos => pos.availableNeighbors(true).length)) as number;
        inputLocations = _.filter(inputLocations,
            pos => pos.availableNeighbors(true).length >= maxNeighbors);
        // Return location closest to storage by path
        const inputPos = originPos.findClosestByPath(inputLocations);
        if (inputPos) {
            return inputPos;
        }

        return undefined;
    }

    /* Build a container output at the optimal location */
    private buildBatteryIfMissing(): void {
        if (!this.battery && !this.findInputConstructionSite()) {
            const buildHere = this.batteryPos;
            if (buildHere) {
                const result = buildHere.createConstructionSite(STRUCTURE_CONTAINER);
                if (result == OK) {
                    return;
                } else {
                    log.warning(`Upgrade site at ${this.pos.print}: cannot build battery! Result: ${result}`);
                }
            }
        }
    }

    run(): void {
        if (Game.time % 25 == 7 && this.base.level >= 2) {
            this.buildBatteryIfMissing();
        }
    }

    spawnForemen(): void {
        this.foreman = new UpgradeForeman(this)
    }
    init(): void {
        // Register energy requests
        if (this.link && this.link.energy < UpgradeSite.settings.linksRequestBelow) {
            this.base.linkNetwork.requestReceive(this.link);
        }
        const inThreshold = this.base.stage > BaseStage.Small ? 0.5 : 0.75;
        if (this.battery) {
            if (this.battery.energy < inThreshold * this.battery.storeCapacity) {
                const energyPerTick = UPGRADE_CONTROLLER_POWER * this.upgradePowerNeeded;
                this.base.logisticsNetwork.requestInput(this.battery, { dAmountdt: energyPerTick });
            }
            if (hasMinerals(this.battery.store)) { // get rid of any minerals in the container if present
                this.base.logisticsNetwork.requestOutputMinerals(this.battery);
            }
        }
    }
}

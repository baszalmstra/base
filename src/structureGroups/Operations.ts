import StructureGroup from "./StructureGroup";
import TransportRequestGroup from "../logistics/TransportRequestGroup";
import Base from "../Base";
import Mem from "../Memory";
import { derefProtoPos } from "../utilities/util";
import _ from "lodash";
import $ from "../GlobalCache";
import { Priority } from "../priorities/priorities";
import Visualizer from "../visuals/Visualizer";
import { profile } from "../profiler";
import ManagerForeman from "../foremen/Manager";

interface OperationsMemory {
    idlePos?: ProtoPos
}

@profile
export default class Operations extends StructureGroup {
    memory: OperationsMemory;
    storage: StructureStorage;								// The base storage, also the instantiation object
    terminal: StructureTerminal | undefined;
    link: StructureLink | undefined;						// Link closest to storage
    towers: StructureTower[];								// Towers within range 3 of storage are part of cmdCenter
    transportRequests: TransportRequestGroup;				// Box for energy requests

    // private _idlePos: RoomPosition;							// Cached idle position

    static settings = {
        enableIdleObservation: true,
        linksTransmitAt: LINK_CAPACITY - 100,
        refillTowersBelow: 750,
    };

    constructor(base: Base, storage: StructureStorage) {
        super(base, storage, 'commandCenter');
        this.memory = Mem.wrap(this.base.memory, 'commandCenter');
        // Register physical components
        this.storage = storage;
        this.terminal = base.terminal;
        if (this.base.bunker) {
            this.link = this.base.bunker.anchor.findClosestByLimitedRange(base.availableLinks, 1);
            this.base.linkNetwork.claimLink(this.link);
            this.towers = this.base.bunker.anchor.findInRange(base.towers, 1);
        } else {
            this.link = this.pos.findClosestByLimitedRange(base.availableLinks, 2);
            this.base.linkNetwork.claimLink(this.link);
            this.towers = this.pos.findInRange(base.towers, 3);
        }
        this.transportRequests = new TransportRequestGroup(); // commandCenter always gets its own request group
    }

    refresh() {
        this.memory = Mem.wrap(this.base.memory, 'commandCenter');
        $.refreshRoom(this);
        $.refresh(this, 'storage', 'link', 'towers', 'terminal');
        this.transportRequests.refresh();
    }

    spawnForemen(): void {
        if (this.link || this.terminal) {
            this.foreman = new ManagerForeman(this);
        }
    }

    // Idle position
    get idlePos(): RoomPosition {
        if (this.base.bunker) {
            return this.base.bunker.anchor;
        }
        if (!this.memory.idlePos || Game.time % 25 == 0) {
            this.memory.idlePos = this.findIdlePos();
        }
        return derefProtoPos(this.memory.idlePos);
    }

    /* Find the best idle position */
    private findIdlePos(): RoomPosition {
        // Try to match as many other structures as possible
        const proximateStructures: Structure[] = _.compact([this.link!,
        this.terminal!,
        // this.powerSpawn!,
        // this.nuker!,
        ...this.towers]);
        const numNearbyStructures = (pos: RoomPosition) =>
            _.filter(proximateStructures, s => s.pos.isNearTo(pos) && !s.pos.isEqualTo(pos)).length;
        return _.last(_.sortBy(this.storage.pos.neighbors, pos => numNearbyStructures(pos)))!;
    }

    /* Register a link transfer store if the link is sufficiently full */
    private registerLinkTransferRequests(): void {
        if (this.link) {
            if (this.link.energy > Operations.settings.linksTransmitAt) {
                this.base.linkNetwork.requestTransmit(this.link);
            }
        }
    }

    private registerRequests(): void {

        // Supply requests:

        // If the link is empty and can send energy and something needs energy, fill it up
        if (this.link && this.link.energy < 0.9 * this.link.energyCapacity && this.link.cooldown <= 1) {
            if (this.base.linkNetwork.receive.length > 0) { 	// If something wants energy
                this.transportRequests.requestInput(this.link, Priority.Critical);
            }
        }
        // Refill towers as needed with variable priority
        const refillTowers = _.filter(this.towers, tower => tower.energy < Operations.settings.refillTowersBelow);
        _.forEach(refillTowers, tower => this.transportRequests.requestInput(tower, Priority.High));

        // Refill core spawn (only applicable to bunker layouts)
        if (this.base.bunker && this.base.bunker.coreSpawn) {
            if (this.base.bunker.coreSpawn.energy < this.base.bunker.coreSpawn.energyCapacity) {
                this.transportRequests.requestInput(this.base.bunker.coreSpawn, Priority.Normal);
            }
        }

        // Withdraw requests:

        // If the link has energy and nothing needs it, empty it
        if (this.link && this.link.energy > 0) {
            if (this.base.linkNetwork.receive.length == 0 || this.link.cooldown > 3) {
                this.transportRequests.requestOutput(this.link, Priority.High);
            }
        }
    }

    init(): void {
        this.registerLinkTransferRequests();
        this.registerRequests();
    }

    run(): void {
    }

    visuals(coord: Coord): Coord {
        let { x, y } = coord;
        const height = this.storage && this.terminal ? 2 : 1;
        const titleCoords = Visualizer.section(`${this.base.name} Operations`,
            { x, y, roomName: this.room.name }, 9.5, height + .1);
        const boxX = titleCoords.x;
        y = titleCoords.y + 0.25;
        if (this.storage) {
            Visualizer.text('Storage', { x: boxX, y: y, roomName: this.room.name });
            Visualizer.barGraph(_.sum(_.values(this.storage.store)) / this.storage.storeCapacity,
                { x: boxX + 4, y: y, roomName: this.room.name }, 5);
            y += 1;
        }
        if (this.terminal) {
            Visualizer.text('Terminal', { x: boxX, y: y, roomName: this.room.name });
            Visualizer.barGraph(this.terminal.store.getUsedCapacity() / this.terminal.storeCapacity,
                { x: boxX + 4, y: y, roomName: this.room.name }, 5);
            y += 1;
        }
        return { x: x, y: y + .25 };
    }
}

const LabStatus = {
    Idle: 0,
    AcquiringMinerals: 1,
    LoadingLabs: 2,
    Synthesizing: 3,
    UnloadingLabs: 4,
};

const LabStageTimeouts = {
    Idle: Infinity,
    AcquiringMinerals: 100,
    LoadingLabs: 50,
    Synthesizing: 10000,
    UnloadingLabs: 1000
};

const LAB_USAGE_WINDOW = 100;

// interface EvolutionChamberMemory {
//     status: number;
//     statusTick: number;
//     activeReaction: Reaction | undefined;
//     reactionQueue: Reaction[];
//     labMineralTypes: {
//         [labID: string]: _ResourceConstantSansEnergy;
//     };
//     stats: {
//         totalProduction: { [resourceType: string]: number }
//         avgUsage: number;
//     };
// }

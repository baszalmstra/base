import { EnergyStructure, StoreStructure, isStoreStructure, isEnergyStructure } from "../declarations/typeGuards";
import { blankPriorityQueue, Priority } from "../priorities/priorities";
import _ from "lodash";
import { log } from "../console/log";
import { profile } from "../profiler";

export type TransportRequestTarget =
    EnergyStructure
    | StoreStructure
    | StructureLab
    | StructureNuker
    | StructurePowerSpawn;

export interface TransportRequest {
    target: TransportRequestTarget;
    amount: number;
    resourceType: ResourceConstant;
}

interface TransportRequestOptions {
    amount?: number;
    resourceType?: ResourceConstant;
}

/**
 * Transport request groups handle close-range prioritized resoruce requests, in contrast to the
 * logistics network, which handles longer-ranged requests.
  */
@profile
export default class TransportRequestGroup {
    supply!: { [priority: number]: TransportRequest[] };
    withdraw!: { [priority: number]: TransportRequest[] };
    supplyById!: { [id: string]: TransportRequest[] };
    withdrawById!: { [id: string]: TransportRequest[] };

    constructor() {
        this.refresh();
    }

    refresh(): void {
        this.supply = blankPriorityQueue();
        this.withdraw = blankPriorityQueue();
        this.supplyById = {};
        this.withdrawById = {};
    }

    get needsSupplying(): boolean {
        for (const priority in this.supply) {
            if (this.supply[priority].length > 0) {
                return true;
            }
        }
        return false;
    }

    get needsWithdrawing(): boolean {
        for (const priority in this.withdraw) {
            if (this.withdraw[priority].length > 0) {
                return true;
            }
        }
        return false;
    }

    getPrioritizedClosestRequest(pos: RoomPosition, type: 'supply' | 'withdraw',
        filter?: ((requst: TransportRequest) => boolean)): TransportRequest | undefined {
        const requests = type == 'withdraw' ? this.withdraw : this.supply;
        for (const priority in requests) {
            const targets = _.map(requests[priority], request => request.target);
            const target = pos.findClosestByRangeThenPath(targets);
            if (target) {
                let searchRequests;
                if (filter) {
                    searchRequests = _.filter(requests[priority], req => filter(req));
                } else {
                    searchRequests = requests[priority];
                }
                return _.find(searchRequests, request => request.target.ref == target!.ref);
            }
        }
        return undefined;
    }

    /**
	 * Request for resources to be deposited into this target
	 */
    requestInput(target: TransportRequestTarget, priority = Priority.Normal, opts = {} as TransportRequestOptions): void {
        _.defaults(opts, {
            resourceType: RESOURCE_ENERGY,
        });
        if (opts.amount == undefined) {
            opts.amount = this.getInputAmount(target, opts.resourceType!);
        }
        // Register the request
        const req: TransportRequest = {
            target: target,
            resourceType: opts.resourceType!,
            amount: opts.amount!,
        };
        if (opts.amount > 0) {
            this.supply[priority].push(req);
            if (!this.supplyById[target.id]) this.supplyById[target.id] = [];
            this.supplyById[target.id].push(req);
        }
    }

    /**
	 * Request for resources to be withdrawn from this target
	 */
    requestOutput(target: TransportRequestTarget, priority = Priority.Normal, opts = {} as TransportRequestOptions): void {
        _.defaults(opts, {
            resourceType: RESOURCE_ENERGY,
        });
        if (opts.amount == undefined) {
            opts.amount = this.getOutputAmount(target, opts.resourceType!);
        }
        // Register the request
        const req: TransportRequest = {
            target: target,
            resourceType: opts.resourceType!,
            amount: opts.amount!,
        };
        if (opts.amount > 0) {
            this.withdraw[priority].push(req);
            if (!this.withdrawById[target.id]) this.withdrawById[target.id] = [];
            this.withdrawById[target.id].push(req);
        }
    }

    private getInputAmount(target: TransportRequestTarget, resourceType: ResourceConstant): number {
        if (isStoreStructure(target)) {
            return target.storeCapacity - _.sum(_.values(target.store));
        } else if (isEnergyStructure(target) && resourceType == RESOURCE_ENERGY) {
            return target.energyCapacity - target.energy;
        } else {
            if (target instanceof StructureLab) {
                if (resourceType == target.mineralType) {
                    return target.mineralCapacity - target.mineralAmount;
                } else if (resourceType == RESOURCE_ENERGY) {
                    return target.energyCapacity - target.energy;
                }
            } else if (target instanceof StructureNuker) {
                if (resourceType == RESOURCE_GHODIUM) {
                    return target.ghodiumCapacity - target.ghodium;
                } else if (resourceType == RESOURCE_ENERGY) {
                    return target.energyCapacity - target.energy;
                }
            } else if (target instanceof StructurePowerSpawn) {
                if (resourceType == RESOURCE_POWER) {
                    return target.powerCapacity - target.power;
                } else if (resourceType == RESOURCE_ENERGY) {
                    return target.energyCapacity - target.energy;
                }
            }
        }
        log.warning('Could not determine requestor amount!');
        return 0;
    }

    private getOutputAmount(target: TransportRequestTarget, resourceType: ResourceConstant): number {
        if (isStoreStructure(target)) {
            return target.store[resourceType]!;
        } else if (isEnergyStructure(target) && resourceType == RESOURCE_ENERGY) {
            return target.energy;
        } else {
            if (target instanceof StructureLab) {
                if (resourceType == target.mineralType) {
                    return target.mineralAmount;
                } else if (resourceType == RESOURCE_ENERGY) {
                    return target.energy;
                }
            } else if (target instanceof StructureNuker) {
                if (resourceType == RESOURCE_GHODIUM) {
                    return target.ghodium;
                } else if (resourceType == RESOURCE_ENERGY) {
                    return target.energy;
                }
            } else if (target instanceof StructurePowerSpawn) {
                if (resourceType == RESOURCE_POWER) {
                    return target.power;
                } else if (resourceType == RESOURCE_ENERGY) {
                    return target.energy;
                }
            }
        }
        log.warning('Could not determine provider amount!');
        return 0;
    }
}

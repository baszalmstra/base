import Base, { getAllBases } from "../Base";
import _ from "lodash";
import $ from "../GlobalCache";
import Mem from "../Memory";
import { log } from "../console/log";
import { AssertionError } from "assert";
import { Energetics } from "./Energetics";
import { rightArrow } from "../utilities/stringConstants";

interface TerminalNetworkMemory {
    equalizeIndex: number;
}

const TerminalNetworkMemoryDefaults: TerminalNetworkMemory = {
    equalizeIndex: 0
};

function baseOf(terminal: StructureTerminal): Base {
    return Collective.bases[terminal.room.name];
}

export class TerminalNetwork implements ITerminalNetwork {
    allTerminals: StructureTerminal[];				// All terminals
    terminals: StructureTerminal[];					// Terminals in standard state
    readyTerminals: StructureTerminal[];
    memory: TerminalNetworkMemory;
    exceptionTerminals: { [terminalID: string]: TerminalState }; 		// Terminals in a special operational state
    assets: { [resourceType: string]: number };		// All assets
    private averageFullness: number;
    private alreadyReceived: StructureTerminal[];
    private alreadySent: StructureTerminal[];

    static settings = {
        equalize: {
            frequency: 2 * (TERMINAL_COOLDOWN + 1),
            maxEnergySendSize: 25000,
            maxMineralSendSize: 5000,
            tolerance: {
                [RESOURCE_ENERGY]: 100000,
                [RESOURCE_POWER]: 2000,
                default: 5000
            } as { [resourceType: string]: number },
            resources: [
                RESOURCE_ENERGY,
                RESOURCE_POWER,
                RESOURCE_CATALYST,
                RESOURCE_ZYNTHIUM,
                RESOURCE_LEMERGIUM,
                RESOURCE_KEANIUM,
                RESOURCE_UTRIUM,
                RESOURCE_OXYGEN,
                RESOURCE_HYDROGEN,
            ],
        },
        buyEnergyThreshold: 200000, // buy energy off market if average amount is less than this
    };

    constructor(terminals: StructureTerminal[]) {
        this.allTerminals = terminals;
        this.terminals = _.clone(terminals);
        this.readyTerminals = _.filter(terminals, t => t.cooldown == 0);
        this.memory = Mem.wrap(Memory.Collective, 'terminalNetwork', TerminalNetworkMemoryDefaults);
        this.alreadyReceived = [];
        this.alreadySent = [];
        this.exceptionTerminals = {}; 		// populated in init()
        this.assets = {}; 					// populated in init()
        this.averageFullness = _.sumBy(this.terminals, t => _.sum(_.values(t.store)) / t.storeCapacity) / this.terminals.length;
    }

    refresh(): void {
        $.refresh(this, 'allTerminals');
        this.terminals = _.clone(this.allTerminals);
        this.readyTerminals = _.filter(this.terminals, t => t.cooldown == 0);
        this.memory = Mem.wrap(Memory.Collective, 'terminalNetwork', TerminalNetworkMemoryDefaults);
        this.alreadyReceived = [];
        this.alreadySent = [];
        this.exceptionTerminals = {}; 		// populated in init()
        this.assets = {}; 					// populated in init()
        this.averageFullness = _.sumBy(this.terminals, t => _.sum(_.values(t.store)) / t.storeCapacity) / this.terminals.length;
    }

    requestResource(terminal: StructureTerminal, resourceType: ResourceConstant, amount: number): void {
    }
    registerTerminalState(terminal: StructureTerminal, state: TerminalState): void {
    }
    init(): void {
    }

    /**
	 * Transfer resources from one terminal to another, logging the results
	 */
    private transfer(sender: StructureTerminal, receiver: StructureTerminal, resourceType: ResourceConstant,
        amount: number, description: string): number {
        const cost = Game.market.calcTransactionCost(amount, sender.room.name, receiver.room.name);
        const response = sender.send(resourceType, amount, receiver.room.name);
        if (response == OK) {
            let msg = `${sender.room.print} ${rightArrow} ${amount} ${resourceType} ${rightArrow} ` +
                `${receiver.room.print} `;
            if (description) {
                msg += `(for ${description})`;
            }
            //this.notify(msg);
            log.info(`Sent ${amount} ${resourceType} from ${sender.room.print} to ` +
                `${receiver.room.print}. Fee: ${cost}.`);
            //this.logTransfer(resourceType, amount, sender.room.name, receiver.room.name);
            this.alreadySent.push(sender);
            this.alreadyReceived.push(receiver);
            _.remove(this.readyTerminals, terminal => terminal.id == sender.id);
        } else {
            log.warning(`Could not send ${amount} ${resourceType} from ${sender.room.print} to ` +
                `${receiver.room.print}! Response: ${response}`);
        }
        return response;
    }

    /**
	 * Equalize resource amounts of each type through all non-exceptional terminals in the network
	 */
    private equalize(resourceType: ResourceConstant, terminals = this.terminals, verbose = false): void {

        log.debug(`Equalizing ${resourceType} within terminal network`);
        const maxSendSize = resourceType == RESOURCE_ENERGY ? TerminalNetwork.settings.equalize.maxEnergySendSize
            : TerminalNetwork.settings.equalize.maxMineralSendSize;
        const averageAmount = _.sum(_.map(terminals,
            terminal => (baseOf(terminal).assets[resourceType] || 0))) / terminals.length;

        const terminalsByResource = _.sortBy(terminals, terminal => (baseOf(terminal).assets[resourceType] || 0));
        if (verbose) log.debug(_.map(terminalsByResource, t => `${t.room.name}: ${baseOf(t).assets[resourceType]}`));
        // Min-max match terminals
        const receivers = _.take(terminalsByResource, Math.floor(terminalsByResource.length / 2));
        terminalsByResource.reverse();

        const senders = _.take(terminalsByResource, Math.floor(terminalsByResource.length / 2));
        if (verbose) log.debug(`Receivers: ${_.map(receivers, t => t.room.print)}`);
        if (verbose) log.debug(`Senders:   ${_.map(senders, t => t.room.print)}`);

        for (const [sender, receiver] of _.zip(senders, receivers)) {
            if (sender === undefined || receiver === undefined) {
                break;
            }
            if (verbose) log.debug(` > ${sender.room.print} to ${receiver.room.print}...`);
            const senderAmount = baseOf(sender).assets[resourceType] || 0;
            const receiverAmount = baseOf(receiver).assets[resourceType] || 0;
            const tolerance = TerminalNetwork.settings.equalize.tolerance[resourceType]
                || TerminalNetwork.settings.equalize.tolerance.default;
            if (verbose) {
                log.debug(`    sender amt: ${senderAmount}  receiver amt: ${receiverAmount}  tolerance: ${tolerance}`);
            }
            if (senderAmount - receiverAmount < tolerance
                && receiverAmount > Energetics.settings.terminal.energy.inThreshold) {
                if (verbose) log.debug(`   Low tolerance`);
                continue; // skip if colonies are close to equilibrium
            }

            const senderSurplus = senderAmount - averageAmount;
            const receiverDeficit = averageAmount - receiverAmount;

            let sendAmount = Math.min(senderSurplus, receiverDeficit, maxSendSize);
            sendAmount = Math.floor(Math.max(sendAmount, 0));
            const sendCost = Game.market.calcTransactionCost(sendAmount, sender.room.name, receiver.room.name);
            sendAmount = Math.min(sendAmount, (sender.store[resourceType] || 0) - sendCost - 10,
                (receiver.storeCapacity - _.sum(_.values(receiver.store))));

            if (sendAmount < TERMINAL_MIN_SEND) {
                if (verbose) log.debug(`    Size too small: ${sendAmount}`);
                continue;
            }
            const ret = this.transfer(sender, receiver, resourceType, sendAmount, 'equalize');
            if (verbose) log.debug(`Response: ${ret}`);
        }
    }

    private equalizeCycle(): void {
        const equalizeResources = TerminalNetwork.settings.equalize.resources;
        if (this.memory.equalizeIndex >= equalizeResources.length) {
            this.memory.equalizeIndex = 0;
        }

        // Equalize current resource type
        const resource = equalizeResources[this.memory.equalizeIndex] as ResourceConstant | undefined;
        const terminals = resource == RESOURCE_POWER ? _.filter(this.terminals, t => baseOf(t).powerSpawn != undefined)
            : this.terminals;
        if (resource) { // resource is undefined if there is nothing to equalize
            this.equalize(resource, terminals, true);
        }

        // Determine next resource type to equalize; most recent resourceType gets cycled to end
        const resourceEqualizeOrder = equalizeResources.slice(this.memory.equalizeIndex + 1)
            .concat(equalizeResources.slice(0, this.memory.equalizeIndex + 1));
        const allColonies = getAllBases();
        const nextResourceType = _.find(resourceEqualizeOrder, resource => {
            if (resource == RESOURCE_ENERGY) {
                return true;
            }
            const amounts = _.map(this.terminals, terminal => baseOf(terminal).assets[resource] || 0);
            const tolerance = TerminalNetwork.settings.equalize.tolerance[resource]
                || TerminalNetwork.settings.equalize.tolerance.default;
            return _.max(amounts)! - _.min(amounts)! > tolerance;
        });

        // Set next equalize resource index
        this.memory.equalizeIndex = _.findIndex(equalizeResources, resource => resource == nextResourceType);
    }

    run(): void {
        // Equalize resources
        if (Game.time % TerminalNetwork.settings.equalize.frequency == 0) {
            this.equalizeCycle();
        }
    }
}

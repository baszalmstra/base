import { onPublicServer, getCacheExpiration, minBy, getAllBaseRooms } from "../utilities/util";
import { profile } from "../profiler/Profiler";
import Factory, { SpawnRequest } from "../structureGroups/Factory";
import Mem from "../Memory";
import _ from "lodash";
import { log } from "../console/log";
import { Pathing } from "../movement/Pathing";
import { bodyCost } from "../creepSetups/CreepSetup";
import Base from "../Base";

interface SpawnGroupMemory {
    bases: string[];
    distances: { [baseName: string]: number };
    routes: { [baseName: string]: { [roomName: string]: boolean } };
    // paths: { [baseName: string]: { startPos: RoomPosition, path: string[] } }
    // tick: number;
    expiration: number;
}

const SpawnGroupMemoryDefaults: SpawnGroupMemory = {
    bases: [],
    distances: {},
    routes: {},
    // paths    : {},
    expiration: 0,
};


const MAX_LINEAR_DISTANCE = 10; // maximum linear distance to search for ANY spawn group
const MAX_PATH_DISTANCE = 600;	// maximum path distance to consider for ANY spawn group
const DEFAULT_RECACHE_TIME = onPublicServer() ? 2000 : 1000;

const defaultSettings: SpawnGroupSettings = {
    maxPathDistance: 400,		// override default path distance
    requiredRCL: 7,
    flexibleEnergy: true,
};

export interface SpawnGroupSettings {
    maxPathDistance: number;	// maximum path distance bases can spawn creeps to
    requiredRCL: number;		// required RCL of bases to contribute
    flexibleEnergy: boolean;	// whether to enforce that only the largest possible creeps are spawned
}

export interface SpawnGroupInitializer {
    ref: string;
    room: Room | undefined;
    pos: RoomPosition;
}


/**
 * SpawnGroup provides a decentralized method of spawning creeps from multiple nearby bases. Use cases include
 * incubation, spawning large combat groups, etc.
 */
@profile
export class SpawnGroup {

    memory: SpawnGroupMemory;
    requests: SpawnRequest[];
    roomName: string;
    baseNames: string[];
    energyCapacityAvailable: number;
    ref: string;
    settings: SpawnGroupSettings;
    stats: {
        avgDistance: number;
    };

    constructor(initializer: SpawnGroupInitializer, settings: Partial<SpawnGroupSettings> = {}) {
        this.roomName = initializer.pos.roomName;
        // this.room = initializer.room;
        if (!Memory.rooms[this.roomName]) {
            Memory.rooms[this.roomName] = {};
        }
        this.memory = Mem.wrap(Memory.rooms[this.roomName], 'spawnGroup', SpawnGroupMemoryDefaults);
        this.ref = initializer.ref + ':SG';
        this.stats = {
            avgDistance: (_.sum(_.values(this.memory.distances)) / _.keys(this.memory.distances).length) || 100,
        };
        this.requests = [];
        this.settings = _.defaults(settings, defaultSettings) as SpawnGroupSettings;
        if (Game.time >= this.memory.expiration) {
            this.recalculateColonies();
        }
        // Compute stats
        this.baseNames = _.filter(this.memory.bases,
            roomName => this.memory.distances[roomName] <= this.settings.maxPathDistance &&
                Game.rooms[roomName] && Game.rooms[roomName].my &&
                Game.rooms[roomName].controller!.level >= this.settings.requiredRCL);
        if (this.baseNames.length == 0) {
            log.warning(`No bases meet the requirements for SwarmGroup: ${this.ref}`);
        }
        this.energyCapacityAvailable = _.max(_.map(this.baseNames,
            roomName => Game.rooms[roomName].energyCapacityAvailable))!;
        Collective.spawnGroups[this.ref] = this;
    }

	/**
	 * Refresh the state of the spawnGroup; called by the Collective object.
	 */
    refresh() {
        this.memory = Mem.wrap(Memory.rooms[this.roomName], 'spawnGroup', SpawnGroupMemoryDefaults);
        this.requests = [];
    }

    private recalculateColonies() { // don't use settings when recalculating bases as spawnGroups share memory
        const baseRoomsInRange = _.filter(getAllBaseRooms(), room =>
            Game.map.getRoomLinearDistance(room.name, this.roomName) <= MAX_LINEAR_DISTANCE);
        const bases = [] as string[];
        const routes = {} as { [baseName: string]: { [roomName: string]: boolean } };
        // let paths = {} as { [baseName: string]: { startPos: RoomPosition, path: string[] } };
        const distances = {} as { [baseName: string]: number };
        for (const baseRoom of baseRoomsInRange) {
            const spawn = baseRoom.spawns[0];
            if (spawn) {
                const route = Pathing.findRoute(baseRoom.name, this.roomName);
                const path = Pathing.findPathToRoom(spawn.pos, this.roomName, { route: route });
                if (route && !path.incomplete && path.path.length <= MAX_PATH_DISTANCE) {
                    bases.push(baseRoom.name);
                    routes[baseRoom.name] = route;
                    // paths[room.name] = path.path;
                    distances[baseRoom.name] = path.path.length;
                }
            }
        }
        this.memory.bases = bases;
        this.memory.routes = routes;
        // this.memory.paths = TODO
        this.memory.distances = distances;
        this.memory.expiration = getCacheExpiration(DEFAULT_RECACHE_TIME, 25);
    }

    enqueue(request: SpawnRequest): void {
        this.requests.push(request);
    }

	/**
	 * SpawnGroup.init() must be called AFTER all hatcheries have been initialized
	 */
    init(): void {
        // Most initialization needs to be done at init phase because bases are still being constructed earlier
        const bases = _.compact(_.map(this.baseNames, name => Collective.bases[name])) as Base[];
        const hatcheries = _.compact(_.map(bases, base => base.factory)) as Factory[];
        const distanceTo = (factory: Factory) => this.memory.distances[factory.pos.roomName] + 25;
        // Enqueue all requests to the factory with least expected wait time that can spawn full-size creep
        for (const request of this.requests) {
            const maxCost = bodyCost(request.setup.generateBody(this.energyCapacityAvailable));
            const okHatcheries = _.filter(hatcheries,
                factory => factory.room.energyCapacityAvailable >= maxCost);
            // || this.settings.flexibleEnergy);
            const bestFactory = minBy(okHatcheries, factory => factory.nextAvailability + distanceTo(factory));
            if (bestFactory) {
                bestFactory.enqueue(request);
            } else {
                log.warning(`Could not enqueue creep ${request.setup.role} in ${this.roomName}, ` +
                    `no factory with ${maxCost} energy capacity`);
            }
        }
    }

    run(): void {
        // Nothing goes here
    }

}

import Base from "./Base";
import Directive from "./directives/Directive";
import { DirectiveWrapper } from "./directives/initializer";
import Director from "./Director";
import Foreman from "./foremen/Foreman";
import GameCache from "./GameCache";
import { Bot } from "./bots/Bot";
import { setFlagsFromString } from "v8";
import { Log, log } from "./console/log";
import RoomIntel from "./intel/RoomIntel";
import DirectiveOutpost from "./directives/Outpost";
import _ from "lodash";
import { profile } from "./profiler";
import { ExpansionPlanner } from "./roomPlanner/ExpansionPlanner";
import { SpawnGroup } from "./logistics/SpawnGroup";
import { TerminalNetwork } from "./logistics/TerminalNetwork";

const COLLECTIVE_EXPIRATION_TIME = 32;

@profile
export default class _Collective implements ICollective {
    bots: { [creepName: string]: Bot; };
    shouldRebuild: boolean = false;
    expiration: number = Game.time + COLLECTIVE_EXPIRATION_TIME;
    bases: { [roomName: string]: Base; } = {};
    basesByRoom: { [roomName: string]: Base; } = {};
    directives: { [name: string]: Directive } = {};
    spawnGroups: { [ref: string]: SpawnGroup } = {};
    director: Director;
    foremen: { [name: string]: Foreman; } = {};
    cache: GameCache;
    expansionPlanner: ExpansionPlanner;
    terminalNetwork!: TerminalNetwork;

    constructor() {
        this.cache = new GameCache();
        this.director = new Director();
        this.expansionPlanner = new ExpansionPlanner();
        this.bots = {}
    }

    build(): void {
        this.cache.build();

        const terminals = [];
        for (let name in Game.rooms) {
            const room = Game.rooms[name]
            if (room.my && room.terminal && room.terminal.my) {
                terminals.push(room.terminal)
            }
        }
        this.terminalNetwork = new TerminalNetwork(terminals);

        this.registerBases()
        this.registerDirectives();
        for (const name in this.bases) {
            this.bases[name].spawnForemen();
        }
        for (const name in this.directives) {
            this.directives[name].spawnForemen();
        }
    }
    refresh(): void {
        this.cache.refresh();
        this.terminalNetwork.refresh();
        for (let name in this.bases) {
            this.bases[name].refresh()
        }
        for (let name in this.directives) {
            this.directives[name].refresh()
        }
        for (let name in this.foremen) {
            this.foremen[name].refresh()
        }
        this.director.refresh()
        this.expansionPlanner.refresh();
    }
    init(): void {
        for (let name in this.bases) {
            this.bases[name].init()
        }
        this.director.init()    // Director handles the foremen and directives
        this.expansionPlanner.init();
    }
    run(): void {
        RoomIntel.run();

        this.director.run()     // Director handles the foremen and directives

        for (let name in this.bases) {
            this.bases[name].run()
        }

        this.terminalNetwork.run();

        this.expansionPlanner.run();
    }
    visuals(): void {
        if (Memory.settings.enableVisuals) {
            this.director.visuals();

            for (let name in this.bases) {
                this.bases[name].visuals()
            }
        }
    }

    /* Iterate over all rooms to detect all the bases that belong to us. */
    private registerBases(): void {
        let colonyOutposts: { [roomName: string]: string[] } = {}; // key: lead room, values: outposts[]

        // Register colony capitols
        for (let name in Game.rooms) {
            if (Game.rooms[name].my) { 			// Will add a new colony for each owned room
                colonyOutposts[name] = [];		// Make a blank list of outposts
            }
        }

        // Register colony outposts
        let outpostFlags = _.filter(Game.flags, flag => DirectiveOutpost.filter(flag));
        for (let flag of outpostFlags) {
            let baseName = flag.memory.base as string | undefined;
            if (baseName && colonyOutposts[baseName]) {
                let outpostName = flag.pos.roomName;
                colonyOutposts[baseName].push(outpostName);
            }
        }

        let count = Object.keys(this.bases).length;
        for (let roomName in colonyOutposts) {
            let room = Game.rooms[roomName]
            const base = new Base(count++, roomName, colonyOutposts[roomName]);
            this.bases[roomName] = base;
            this.basesByRoom[roomName] = base;
            for (const outposts of colonyOutposts[roomName]) {
                this.basesByRoom[outposts] = base
            }
        }
    }

    /* Wrap each flag in a color coded wrapper */
    private registerDirectives(): void {
        this.directives = {}
        // Create a directive for each flag (registration takes place on construction)
        for (let name in Game.flags) {
            const flag = Game.flags[name];
            let directive = DirectiveWrapper(flag);
            if (directive && directive.base != undefined) {
                this.directives[name] = directive;
                directive.base.flags.push(directive.flag);
            } else if (flag.memory.base) {
                const base = this.bases[flag.memory.base];
                if (base) {
                    base.flags.push(flag);
                }
            } else {
                log.error(`Unhandled flag '${name}' at ${flag.pos.print} color: ${flag.color}, secondaryColor: ${flag.secondaryColor}`)
            }
        }
    }
}

import _ from "lodash";
import { toCreep, Bot } from "../bots/Bot";
import { isOwnedStructure, isStructure, isBot } from "../declarations/typeGuards";
import { log } from "../console/log";
import RoomIntel from "./RoomIntel";
import { profile } from "../profiler";
import { Cartographer } from "../utilities/Cartographer";

@profile
export default class CombatIntel {

    /**
	 * Cache the result of a computation for a tick
	 */
    static cache(creep: Creep, key: string, callback: () => number): number {
        if (!creep.intel) creep.intel = {};
        if (creep.intel[key] == undefined) {
            creep.intel[key] = callback();
        }
        return creep.intel[key];
    }

    /**
	 * Heal potential of a single creep in units of effective number of parts
	 */
    static getHealPotential(creep: Creep): number {
        return this.cache(creep, 'healPotential', () =>
            _.sumBy(creep.body, function (part) {
                if (part.hits == 0) {
                    return 0;
                }
                if (part.type == HEAL) {
                    if (!part.boost) {
                        return 1;
                    }
                    // else if (part.boost == boostResources.heal[1]) {
                    // 	return BOOSTS.heal.LO.heal;
                    // } else if (part.boost == boostResources.heal[2]) {
                    // 	return BOOSTS.heal.LHO2.heal;
                    // } else if (part.boost == boostResources.heal[3]) {
                    // 	return BOOSTS.heal.XLHO2.heal;
                    // }
                }
                return 0;
            })
        );
    }

    static getHealAmount(creep: Creep | Bot): number {
        return HEAL_POWER * this.getHealPotential(toCreep(creep));
    }

    static getRangedHealAmount(creep: Creep | Bot): number {
        return RANGED_HEAL_POWER * this.getHealPotential(toCreep(creep));
    }

    /**
	 * Determine the predicted damage amount of a certain type of attack. Can specify if you should use predicted or
	 * current hits amount and whether to include predicted healing. Does not update predicted hits.
	 */
    static predictedDamageAmount(attacker: Creep | Bot, target: Creep, attackType: 'attack' | 'rangedAttack',
        useHitsPredicted = true): number {
        // Compute initial (gross) damage amount
        let grossDamage: number;
        if (attackType == 'attack') {
            grossDamage = this.getAttackDamage(attacker);
        } else if (attackType == 'rangedAttack') {
            grossDamage = this.getRangedAttackDamage(attacker);
        } else { // rangedMassAttack; not currently used
            grossDamage = this.getMassAttackDamageTo(attacker, target);
        }
        // Adjust for remaining tough parts
        let toughHits: number;
        if (useHitsPredicted) {
            if (target.hitsPredicted == undefined) target.hitsPredicted = target.hits;
            const nonToughHits = _.sumBy(target.body, part => part.type == TOUGH ? 0 : part.hits);
            toughHits = Math.min(target.hitsPredicted - nonToughHits, 0); // predicted amount of TOUGH
        } else {
            toughHits = 100 * target.getActiveBodyparts(TOUGH);
        }
        const damageMultiplier = this.minimumDamageTakenMultiplier(target); // assumes only 1 tier of boosts
        if (grossDamage * damageMultiplier < toughHits) { // if you can't eat through armor
            return grossDamage * damageMultiplier;
        } else { // if you break tough shield
            grossDamage -= toughHits / damageMultiplier;
            return toughHits + grossDamage;
        }
    }

    /**
	 * Minimum damage multiplier a creep has
	 */
    static minimumDamageTakenMultiplier(creep: Creep): number {
        return this.cache(creep, 'minDamageMultiplier', () =>
            _.min(_.map(creep.body, part => {
                // if (part.type == TOUGH && part.hits > 0) {
                // 	if (part.boost == boostResources.tough[1]) {
                // 		return BOOSTS.tough.GO.damage;
                // 	} else if (part.boost == boostResources.tough[2]) {
                // 		return BOOSTS.tough.GHO2.damage;
                // 	} else if (part.boost == boostResources.tough[3]) {
                // 		return BOOSTS.tough.XGHO2.damage;
                // 	}
                // }
                return 1;
            }))!
        );
    }

    /**
	 * Attack potential of a single creep in units of effective number of parts
	 */
    static getAttackPotential(creep: Creep): number {
        return this.cache(creep, 'attackPotential', () => _.sumBy(creep.body, function (part) {
            if (part.hits == 0) {
                return 0;
            }
            if (part.type == ATTACK) {
                if (!part.boost) {
                    return 1;
                }
                // else if (part.boost == boostResources.attack[1]) {
                // 	return BOOSTS.attack.UH.attack;
                // } else if (part.boost == boostResources.attack[2]) {
                // 	return BOOSTS.attack.UH2O.attack;
                // } else if (part.boost == boostResources.attack[3]) {
                // 	return BOOSTS.attack.XUH2O.attack;
                // }
            }
            return 0;
        }));
    }

    static getAttackDamage(creep: Creep | Bot): number {
        return ATTACK_POWER * this.getAttackPotential(toCreep(creep));
    }

	/**
	 * Ranged attack potential of a single creep in units of effective number of parts
	 */
    static getRangedAttackPotential(creep: Creep): number {
        return this.cache(creep, 'rangedAttackPotential', () =>
            _.sumBy(creep.body, function (part) {
                if (part.hits == 0) {
                    return 0;
                }
                if (part.type == RANGED_ATTACK) {
                    if (!part.boost) {
                        return 1;
                    }
                    // else if (part.boost == boostResources.ranged_attack[1]) {
                    // 	return BOOSTS.ranged_attack.KO.rangedAttack;
                    // } else if (part.boost == boostResources.ranged_attack[2]) {
                    // 	return BOOSTS.ranged_attack.KHO2.rangedAttack;
                    // } else if (part.boost == boostResources.ranged_attack[3]) {
                    // 	return BOOSTS.ranged_attack.XKHO2.rangedAttack;
                    // }
                }
                return 0;
            })
        );
    }

    static getRangedAttackDamage(creep: Creep | Bot): number {
        return RANGED_ATTACK_POWER * this.getRangedAttackPotential(toCreep(creep));
    }

    static getMassAttackDamageTo(attacker: Creep | Bot, target: Creep | Structure): number {
        if (isStructure(target) && (!isOwnedStructure(target) || target.my)) {
            return 0;
        }
        const range = attacker.pos.getRangeTo(target.pos);
        let rangedMassAttackPower = 0;
        if (range <= 1) {
            rangedMassAttackPower = 10;
        } else if (range == 2) {
            rangedMassAttackPower = 4;
        } else if (range == 3) {
            rangedMassAttackPower = 1;
        }
        return rangedMassAttackPower * this.getRangedAttackPotential(isBot(attacker) ? attacker.creep : attacker);
    }

    /**
	 * Get the tower damage at a given range
	 */
    static singleTowerDamage(range: number): number {
        if (range <= TOWER_OPTIMAL_RANGE) {
            return TOWER_POWER_ATTACK;
        }
        range = Math.min(range, TOWER_FALLOFF_RANGE);
        const falloff = (range - TOWER_OPTIMAL_RANGE) / (TOWER_FALLOFF_RANGE - TOWER_OPTIMAL_RANGE);
        return TOWER_POWER_ATTACK * (1 - TOWER_FALLOFF * falloff);
    }

	/**
	 * Total tower tamage from all towers in room at a given position
	 */
    static towerDamageAtPos(pos: RoomPosition, ignoreEnergy = false): number {
        if (pos.room) {
            let expectedDamage = 0;
            for (const tower of pos.room.towers) {
                if (tower.energy > 0 || ignoreEnergy) {
                    expectedDamage += this.singleTowerDamage(pos.getRangeTo(tower));
                }
            }
            return expectedDamage;
        } else {
            log.warning(`CombatIntel.towerDamageAtPos: room visibility at ${pos.print}!`);
            return 0;
        }
    }

    /**
	 * Heal potential of self and possible healer neighbors
	 */
    static maxHostileHealingTo(creep: Creep): number {
        return this.cache(creep, 'maxHostileHealing', () => {
            const selfHealing = this.getHealAmount(creep);
            const neighbors = _.filter(creep.room.hostiles, hostile => hostile.pos.isNearTo(creep));
            const neighborHealing = _.sumBy(neighbors, neighbor => this.getHealAmount(neighbor));
            const rangedHealers = _.filter(creep.room.hostiles, hostile => hostile.pos.getRangeTo(creep) <= 3 &&
                !neighbors.includes(hostile));
            const rangedHealing = _.sumBy(rangedHealers, healer => this.getRangedHealAmount(healer));
            return selfHealing + neighborHealing + rangedHealing;
        });
    }

	/**
	 * Heal potential of self and possible healer neighbors
	 */
    static avgHostileHealingTo(creeps: Creep[]): number {
        return _.max(_.map(creeps, creep => CombatIntel.maxHostileHealingTo(creep)))! / creeps.length;
    }

	/**
	 * Heal potential of self and possible healer neighbors
	 */
    static maxFriendlyHealingTo(friendly: Creep | Bot): number {
        const creep = toCreep(friendly);
        return this.cache(creep, 'maxFriendlyHealing', () => {
            const selfHealing = this.getHealAmount(creep);
            const neighbors = _.filter(creep.room.creeps, hostile => hostile.pos.isNearTo(creep));
            const neighborHealing = _.sumBy(neighbors, neighbor => this.getHealAmount(neighbor));
            const rangedHealers = _.filter(creep.room.creeps, hostile => hostile.pos.getRangeTo(creep) <= 3 &&
                !neighbors.includes(hostile));
            const rangedHealing = _.sumBy(rangedHealers, healer => this.getHealAmount(healer));
            return selfHealing + neighborHealing + rangedHealing;
        });
    }

    static isApproaching(approacher: Creep, toPos: RoomPosition): boolean {
        const previousPos = RoomIntel.getPreviousPos(approacher);
        const previousRange = toPos.getRangeTo(previousPos);
        const currentRange = toPos.getRangeTo(approacher.pos);
        return currentRange < previousRange;
    }

    static isRetreating(retreater: Creep, fromPos: RoomPosition): boolean {
        const previousPos = RoomIntel.getPreviousPos(retreater);
        const previousRange = fromPos.getRangeTo(previousPos);
        const currentRange = fromPos.getRangeTo(retreater.pos);
        return currentRange > previousRange;
    }

	/**
	 * This method is probably expensive; use sparingly
	 */
    static isEdgeDancing(creep: Creep, reentryThreshold = 3): boolean {
        if (!creep.room.my) {
            log.warning(`isEdgeDancing should only be called in owned rooms!`);
        }
        const creepOccupancies = creep.room.memory.creepsInRoom
        if (creepOccupancies) {
            // Look to see if the creep has exited and re-entered the room a given number of times
            const creepInRoomTicks = [];
            for (const tick in creepOccupancies) {
                if (creepOccupancies[tick].includes(creep.name)) {
                    creepInRoomTicks.push(parseInt(tick, 10));
                }
            }
            let reentries = 1;
            if (creepInRoomTicks.length > 0) {
                for (const i of _.range(creepInRoomTicks.length - 1)) {
                    if (creepInRoomTicks[i + 1] != creepInRoomTicks[i] + 1) {
                        // There was a gap between the creep's presence in the room so it must have reentered
                        reentries++;
                    }
                }
            }
            return reentries >= reentryThreshold;
        } else {
            return false;
        }
    }

    static getPositionsNearEnemies(hostiles: Creep[], range = 0): RoomPosition[] {
        return _.uniq(_.flatten(_.map(hostiles, hostile =>
            hostile.pos.getPositionsInRange(range, false, true))));
    }

    /**
	 * Fallback is a location on the other side of the nearest exit the directive is placed at
	 */
    static getFallbackFrom(pos: RoomPosition, fallbackDistance = 2): RoomPosition {
        let { x, y, roomName } = pos;
        const rangesToExit = [[x, 'left'], [49 - x, 'right'], [y, 'top'], [49 - y, 'bottom']];
        const [range, direction] = _.first(_.sortBy(rangesToExit, pair => pair[0]))!;
        switch (direction) {
            case 'left':
                x = 49 - fallbackDistance;
                roomName = Cartographer.findRelativeRoomName(roomName, -1, 0);
                break;
            case 'right':
                x = fallbackDistance;
                roomName = Cartographer.findRelativeRoomName(roomName, 1, 0);
                break;
            case 'top':
                y = 49 - fallbackDistance;
                roomName = Cartographer.findRelativeRoomName(roomName, 0, -1);
                break;
            case 'bottom':
                y = fallbackDistance;
                roomName = Cartographer.findRelativeRoomName(roomName, 0, 1);
                break;
            default:
                log.error('Error getting fallback position!');
                break;
        }
        return new RoomPosition(x, y, roomName);
    }
}

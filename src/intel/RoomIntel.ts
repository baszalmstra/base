import _ from "lodash";
import { derefCoords, derefProtoPos, irregularExponentialMovingAverage, getCacheExpiration } from "../utilities/util";
import { Bot } from "../bots/Bot";
import { profile } from "../profiler";
import { ExpansionEvaluator } from "../strategy/ExpansionEvaluator";

const RECACHE_TIME = 2500;
const OWNED_RECACHE_TIME = 1000;
const SCORE_RECALC_PROB = 0.05;
const FALSE_SCORE_RECALC_PROB = 0.01;

@profile
export default class RoomIntel {
    /**
	 * Mark a room as being visible this tick
	 */
    private static markVisible(room: Room): void {
        room.memory.tick = Game.time;
    }

    /**
	 * Returns the last tick at which the room was visible, or -100
	 */
    static lastVisible(roomName: string): number {
        if (Memory.rooms[roomName]) {
            return Memory.rooms[roomName].tick || -100;
        } else {
            return -100;
        }
    }

    /**
	 * Records all info for permanent room objects, e.g. sources, controllers, etc.
	 */
    private static recordPermanentObjects(room: Room): void {
        const savedSources: SavedSource[] = [];
        for (const source of room.sources) {
            const container = source.pos.findClosestByLimitedRange(room.containers, 2);
            savedSources.push({
                c: source.pos.coordName,
                contnr: container ? container.pos.coordName : undefined
            })
        }
        room.memory.sources = savedSources;
        room.memory.controller = room.controller ? {
            c: room.controller.pos.coordName,
            level: room.controller.level,
            owner: room.controller.owner ? room.controller.owner.username : undefined,
            reservation: room.controller.reservation ? {
                username: room.controller.reservation.username,
                ticksToEnd: room.controller.reservation.ticksToEnd
            } : undefined,
            safeMode: room.controller.safeMode,
            safeModeAvailable: room.controller.safeModeAvailable,
            safeModeCooldown: room.controller.safeModeCooldown,
            progress: room.controller.progress,
            progressTotal: room.controller.progressTotal
        } : undefined;
        room.memory.mineral = room.mineral ? {
            c: room.mineral.pos.coordName,
            density: room.mineral.density,
            type: room.mineral.mineralType
        } : undefined;
        room.memory.lairs = _.map(room.keeperLairs, lair => {
            return { c: lair.pos.coordName };
        });
        room.memory.portals = _.map(room.portals, portal => {
            const dest = portal.destination instanceof RoomPosition ? { shard: Game.shard.name, room: portal.destination.roomName } : portal.destination;
            const exp = portal.ticksToDecay != undefined ? Game.time + portal.ticksToDecay : Game.time + 1e6;
            return { c: portal.pos.coordName, dest: dest, expiration: exp };
        });
        if (room.controller && room.controller.owner) {
            room.memory.structures = {
                towers: _.map(room.towers, t => t.pos.coordName),
                spawns: _.map(room.spawns, s => s.pos.coordName),
                walls: _.map(room.walls, w => w.pos.coordName),
                ramparts: _.map(room.ramparts, r => r.pos.coordName),
                storage: room.storage ? room.storage.pos.coordName : undefined,
                terminal: room.terminal ? room.terminal.pos.coordName : undefined,
            }
        }
        room.memory.tick = Game.time
    }

    private static recomputeScoreIfNecessary(room: Room): boolean {
        if (room.memory.exp == false) { // room is uninhabitable or owned
            if (Math.random() < FALSE_SCORE_RECALC_PROB) {
                // false scores get evaluated very occasionally
                return ExpansionEvaluator.computeExpansionData(room);
            }
        } else { // if the room is not uninhabitable
            if (!room.memory.exp || Math.random() < SCORE_RECALC_PROB) {
                // recompute some of the time
                return ExpansionEvaluator.computeExpansionData(room);
            }
        }
        return false;
    }

    private static updateInvasionData(room: Room): void {
        if (!room.memory.invasion) {
            room.memory.invasion = {
                harvested: 0,
                lastSeen: 0,
            };
        }
        const sources = room.sources;
        const invasionData = room.memory.invasion!;
        for (const source of sources) {
            if (source.ticksToRegeneration == 1) {
                invasionData.harvested += source.energyCapacity - source.energy;
            }
        }
        if (room.invaders.length > 0) {
            invasionData.harvested = 0;
            invasionData.lastSeen = Game.time;
        }
    }

    static roomOwnedBy(roomName: string): string | undefined {
        if (Memory.rooms[roomName] && Memory.rooms[roomName].controller &&
            Memory.rooms[roomName].controller!.owner) {
            if (Game.time - (Memory.rooms[roomName].tick || 0) < 25000) { // ownership expires after 25k ticks
                return Memory.rooms[roomName].controller!.owner;
            }
        }
        return undefined
    }

    static roomReservedBy(roomName: string): string | undefined {
        if (Memory.rooms[roomName] && Memory.rooms[roomName].controller &&
            Memory.rooms[roomName].controller!.reservation) {
            if (Game.time - (Memory.rooms[roomName].tick || 0) < 10000) { // reservation expires after 10k ticks
                return Memory.rooms[roomName].controller!.reservation!.username;
            }
        }
        return undefined;
    }

    static roomReservationRemaining(roomName: string): number {
        if (Memory.rooms[roomName] && Memory.rooms[roomName].controller &&
            Memory.rooms[roomName].controller!.reservation) {
            const ticksToEnd = Memory.rooms[roomName].controller!.reservation!.ticksToEnd;
            const timeSinceLastSeen = Game.time - (Memory.rooms[roomName].tick || 0);
            return ticksToEnd - timeSinceLastSeen;
        }
        return 0;
    }

    static isInvasionLikely(room: Room): boolean {
        const data = room.memory.invasion;
        if (!data) return false;
        if (data.lastSeen > 20000) { // maybe room is surrounded by owned/reserved rooms and invasions aren't possible
            return false;
        }
        switch (room.sources.length) {
            case 1:
                return data.harvested > 90000;
            case 2:
                return data.harvested > 75000;
            case 3:
                return data.harvested > 65000;
            default: // shouldn't ever get here
                return false;
        }
    }

    /**
	 * Get the pos a creep was in on the previous tick
	 */
    static getPreviousPos(creep: Creep | Bot): RoomPosition {
        if (creep.room.memory.prevPositions && creep.room.memory.prevPositions![creep.id]) {
            return derefProtoPos(creep.room.memory.prevPositions![creep.id]);
        } else {
            return creep.pos; // no data
        }
    }

    private static recordCreepPositions(room: Room): void {
        room.memory.prevPositions = {};
        for (const creep of room.find(FIND_CREEPS)) {
            room.memory.prevPositions![creep.id] = creep.pos;
        }
    }

    private static recordSafety(room: Room): void {
        if (!room.memory.safety) {
            room.memory.safety = {
                safeFor: 0,
                unsafeFor: 0,
                safety1k: 1,
                safety10k: 1,
                tick: Game.time
            };
        }
        let safety: number;
        const safetyData = room.memory.safety as SafetyData;
        if (room.dangerousHostiles.length > 0) {
            safetyData.safeFor = 0;
            safetyData.unsafeFor += 1;
            safety = 0;
        } else {
            safetyData.safeFor += 1;
            safetyData.unsafeFor = 0;
            safety = 1;
        }
        // Compute rolling averages
        const dTime = Game.time - safetyData.tick;
        safetyData.safety1k = +(irregularExponentialMovingAverage(
            safety, safetyData.safety1k, dTime, 1000)).toFixed(5);
        safetyData.safety10k = +(irregularExponentialMovingAverage(
            safety, safetyData.safety10k, dTime, 10000)).toFixed(5);
        safetyData.tick = Game.time;
    }

    static getSafetyData(roomName: string): SafetyData {
        if (!Memory.rooms[roomName]) {
            Memory.rooms[roomName] = {};
        }
        if (!Memory.rooms[roomName].safety) {
            Memory.rooms[roomName].safety = {
                safeFor: 0,
                unsafeFor: 0,
                safety1k: 1,
                safety10k: 1,
                tick: Game.time
            };
        }
        return Memory.rooms[roomName].safety!;
    }

    static inSafeMode(roomName: string): boolean {
        if (!!Memory.rooms[roomName] && !!Memory.rooms[roomName].controller) {
            const safemode = Memory.rooms[roomName].controller!.safeMode;
            const tick = Memory.rooms[roomName].expiration;
            if (safemode && tick) {
                return Game.time < tick + safemode;
            }
        }
        return false;
    }


    static run(): void {
        let alreadyComputedScore = false;
        for (const name in Game.rooms) {

            const room: Room = Game.rooms[name];

            this.markVisible(room);
            this.recordSafety(room);

            // Track invasion data, harvesting, and casualties for all colony rooms and outposts
            if (Collective.basesByRoom[room.name]) { // if it is an owned or outpost room
                this.updateInvasionData(room);
                //this.updateHarvestData(room);
                //this.updateCasualtyData(room);
            }

            // Record previous creep positions if needed (RoomIntel.run() is executed at end of each tick)
            if (room.hostiles.length > 0) {
                this.recordCreepPositions(room);
                // if (room.my) {
                //     this.recordCreepOccupancies(room);
                // }
            }

            // Record location of permanent objects in room and recompute score as needed
            if (Game.time >= (room.memory.expiration || 0)) {
                this.recordPermanentObjects(room);
                if (!alreadyComputedScore) {
                    alreadyComputedScore = this.recomputeScoreIfNecessary(room);
                }
                // // Refresh cache
                const recacheTime = room.owner ? OWNED_RECACHE_TIME : RECACHE_TIME;
                room.memory.expiration = getCacheExpiration(recacheTime, 250);
            }

            // if (room.controller && Game.time % 5 == 0) {
            // 	this.recordControllerInfo(room.controller);
            // }

        }
    }
}

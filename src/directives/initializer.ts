import Directive from "./Directive";
import DirectiveHarvest from "./Harvest";
import DirectiveBootstrap from "./Bootstrap";
import DirectiveOutpost from "./Outpost";
import DirectiveGuard from "./Guard";
import { DirectiveColonize } from "./Colonize";
import { DirectiveOutpostDefense } from "./OutpostDefense";
import { DirectiveClearRoom } from "./ClearRoom";
import { DirectiveDismantle } from "./Dismantle";
import { DirectivePairDestroy } from "./PairDestroy";
import { DirectiveExtract } from "./Extract";

export function DirectiveWrapper(flag: Flag): Directive | undefined {
    if (flag.color == DirectiveHarvest.color && flag.secondaryColor == DirectiveHarvest.secondaryColor) {
        return new DirectiveHarvest(flag);
    } else if (flag.color == DirectiveBootstrap.color && flag.secondaryColor == DirectiveBootstrap.secondaryColor) {
        return new DirectiveBootstrap(flag);
    } else if (flag.color == DirectiveOutpost.color && flag.secondaryColor == DirectiveOutpost.secondaryColor) {
        return new DirectiveOutpost(flag);
    } else if (flag.color == DirectiveGuard.color && flag.secondaryColor == DirectiveGuard.secondaryColor) {
        return new DirectiveGuard(flag);
    } else if (flag.color == DirectiveColonize.color && flag.secondaryColor == DirectiveColonize.secondaryColor) {
        return new DirectiveColonize(flag);
    } else if (flag.color == DirectiveOutpostDefense.color && flag.secondaryColor == DirectiveOutpostDefense.secondaryColor) {
        return new DirectiveOutpostDefense(flag);
    } else if (flag.color == DirectiveClearRoom.color && flag.secondaryColor == DirectiveClearRoom.secondaryColor) {
        return new DirectiveClearRoom(flag);
    } else if (flag.color == DirectiveDismantle.color && flag.secondaryColor == DirectiveDismantle.secondaryColor) {
        return new DirectiveDismantle(flag)
    } else if (flag.color == DirectivePairDestroy.color && flag.secondaryColor == DirectivePairDestroy.secondaryColor) {
        return new DirectivePairDestroy(flag);
    } else if (flag.color == DirectiveExtract.color && flag.secondaryColor == DirectiveExtract.secondaryColor) {
        return new DirectiveExtract(flag);
    }
    return undefined
}

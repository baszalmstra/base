import Directive from "./Directive";
import { Cartographer, ROOMTYPE_CONTROLLER } from "../utilities/Cartographer";
import { log } from "../console/log";
import { MY_USERNAME } from "../~settings";
import Base from "../Base";
import { printRoomName } from "../utilities/util";
import { profile } from "../profiler";
import { ClaimingForeman } from "../foremen/Claim";
import { PioneerForeman } from "../foremen/Pioneer";
import { Roles } from "../creepSetups/setups";
import _ from "lodash";

/**
 * Claims a new room and builds a spawn but does not incubate. Removes when spawn is constructed.
 */
@profile
export class DirectiveColonize extends Directive {

    static directiveName = 'colonize';
    static color = COLOR_PURPLE;
    static secondaryColor = COLOR_GREY;

    static requiredRCL = 3;

    toColonize: Base | undefined;
    foremen!: {
        claim: ClaimingForeman;
        pioneer: PioneerForeman;
    };

    constructor(flag: Flag) {
        super(flag, base => base.level >= DirectiveColonize.requiredRCL
            && base.name != Directive.getPos(flag).roomName && base.spawns.length > 0);
        // Register incubation status
        this.toColonize = this.room ? Collective.basesByRoom[this.room.name] : undefined;
        // Remove if misplaced
        if (Cartographer.roomType(this.pos.roomName) != ROOMTYPE_CONTROLLER) {
            log.warning(`${this.print}: ${printRoomName(this.pos.roomName)} is not a controller room; ` +
                `removing directive!`);
            this.remove(true);
        }
    }

    spawnForemen() {
        this.foremen.claim = new ClaimingForeman(this);
        this.foremen.pioneer = new PioneerForeman(this);
    }

    init() {
        //this.alert(`Colonization in progress`);
    }

    run(verbose = false) {
        if (this.toColonize && this.toColonize.spawns.length > 0) {
            // Reassign all pioneers to be miners and workers
            const miningOverlords = _.map(this.toColonize.miningSites, site => site.foremen.mine);
            for (const pioneer of this.foremen.pioneer.pioneers) {
                const miningOverlord = miningOverlords.shift();
                if (miningOverlord) {
                    if (verbose) {
                        log.debug(`Reassigning: ${pioneer.print} to mine: ${miningOverlord.print}`);
                    }
                    pioneer.reassign(miningOverlord, Roles.drone);
                } else {
                    if (verbose) {
                        log.debug(`Reassigning: ${pioneer.print} to work: ${this.toColonize.foremen.work.print}`);
                    }
                    pioneer.reassign(this.toColonize.foremen.work, Roles.worker);
                }
            }
            // Remove the directive
            this.remove();
        }
        if (Game.time % 10 == 2 && this.room && !!this.room.owner && this.room.owner != MY_USERNAME) {
            log.notify(`Removing Colonize directive in ${this.pos.roomName}: room already owned by another player.`);
            this.remove();
        }
    }
}

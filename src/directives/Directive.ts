import Base, { getAllBases } from "../Base";
import { randomHex, derefProtoPos, equalXYR } from "../utilities/util";
import { log } from "../console/log";
import _ from "lodash";
import Foreman from "../foremen/Foreman";
import { Pathing } from "../movement/Pathing";

interface DirectiveCreationOptions {
    name?: string;
    quiet?: boolean;
    memory?: FlagMemory;
}

const DEFAULT_MAX_PATH_LENGTH = 600;
const DEFAULT_MAX_LINEAR_RANGE = 10;

export default abstract class Directive {

    static directiveName: string;               // Name of the type of directive, e.g. "incubate"
    static color: ColorConstant;                // Flag color
    static secondaryColor: ColorConstant;       // Flag secondaryColor

    name: string;                               // The name of the flag
    ref: string;                                // The name of the flag
    base!: Base;                                // The colony of the directive (directive is removed if undefined)
    baseFilter?: (base: Base) => boolean

    pos!: RoomPosition;                         // Flag position
    room: Room | undefined;                     // Flag room

    memory: FlagMemory;

    foremen: { [name: string]: Foreman };       // Overlords

    constructor(flag: Flag, baseFilter?: (base: Base) => boolean) {
        this.name = flag.name;
        this.ref = flag.ref;
        this.memory = flag.memory
        this.baseFilter = baseFilter

        this.pos = flag.pos;
        this.room = flag.room;

        this.foremen = {}

        // Relocate flag if needed; this must be called before the colony calculations
        const needsRelocating = this.handleRelocation();
        if (!needsRelocating) {
            this.pos = flag.pos;
            this.room = flag.room;
        }

        const base = this.getBase(this.baseFilter);
        if (!base) {
            log.warning(`Could not get base for directive ${this.print}; removing flag!`);
            flag.remove();
            return;
        }
        this.base = base;
        this.base.flags.push(flag);

        global[this.name] = this;
        Collective.director.registerDirective(this);
        Collective.directives[this.name] = this;
    }

    private handleRelocation(): boolean {
        if (this.memory.setPosition) {
            const pos = derefProtoPos(this.memory.setPosition);
            if (!this.flag.pos.isEqualTo(pos)) {
                const result = this.flag.setPosition(pos);
                if (result == OK) {
                    log.debug(`Moving ${this.name} from ${this.flag.pos.print} to ${pos.print}.`);
                } else {
                    log.warning(`Could not set room position to ${JSON.stringify(this.memory.setPosition)}!`);
                }
            } else {
                delete this.memory.setPosition;
            }
            this.pos = pos;
            this.room = Game.rooms[pos.roomName];
            return true;
        }
        return false;
    }

    // Flag must be a getter to avoid caching issues
    get flag(): Flag {
        return Game.flags[this.name];
    }

    /**
	 * Gets an effective room position for a directive; allows you to reference this.pos in constructor super() without
	 * throwing an error
	 */
    static getPos(flag: Flag): RoomPosition {
        if (flag.memory && flag.memory.setPosition) {
            const pos = derefProtoPos(flag.memory.setPosition);
            return pos;
        }
        return flag.pos;
    }

    setPosition(pos: RoomPosition): number {
        // Ignore the (x,y) setPosition option since I never use it
        return this.flag.setPosition(pos);
    }

    refresh() {
        const flag = this.flag;
        if (!flag) {
            log.warning(`Missing flag for directive ${this.print}! Removing directive.`);
            this.remove();
            return;
        }
        this.memory = flag.memory;
        this.pos = flag.pos;
        this.room = flag.room;
    }

    get print(): string {
        return '<a href="#!/room/' + Game.shard.name + '/' + this.pos.roomName + '">[' + this.name + ']</a>';
    }

    private getBase(baseFilter?: (colony: Base) => boolean, ): Base | undefined {
        // If the flag remembered a Base, use that
        if (this.memory.base) {
            return Collective.bases[this.memory.base]
        } else {
            // Try to find a base by the room the flag is in
            const base = Collective.basesByRoom[this.pos.roomName] as Base | undefined;
            if (base) {
                this.memory.base = base.name
                return base
            }

            // Otherwise assign to closest colony
            const nearestColony = this.findNearestColony(baseFilter);
            if (nearestColony) {
                log.info(`Colony ${nearestColony.room.print} assigned to ${this.name}.`);
                this.memory.base = nearestColony.room.name;
                return nearestColony;
            } else {
                log.error(`Could not find colony match for ${this.name} in ${this.pos.roomName}! ` +
                    `Try setting memory.maxPathLength and memory.maxLinearRange.`);
            }
        }
        return undefined
    }

    private findNearestColony(colonyFilter?: (colony: Base) => boolean, verbose = false): Base | undefined {
        const maxPathLength = this.memory.maxPathLength || DEFAULT_MAX_PATH_LENGTH;
        const maxLinearRange = this.memory.maxLinearRange || DEFAULT_MAX_LINEAR_RANGE;
        if (verbose) log.info(`Recalculating colony association for ${this.name} in ${this.pos.roomName}`);
        let nearestColony: Base | undefined;
        let minDistance = Infinity;
        const colonyRooms = _.filter(Game.rooms, room => room.my);
        for (const colony of getAllBases()) {
            if (Game.map.getRoomLinearDistance(this.pos.roomName, colony.name) > maxLinearRange) {
                continue;
            }
            if (!colonyFilter || colonyFilter(colony)) {
                const ret = Pathing.findPath((colony.factory || colony).pos, this.pos);
                if (!ret.incomplete) {
                    if (ret.path.length < maxPathLength && ret.path.length < minDistance) {
                        nearestColony = colony;
                        minDistance = ret.path.length;
                    }
                    if (verbose) log.info(`Path length to ${colony.room.print}: ${ret.path.length}`);
                } else {
                    if (verbose) log.info(`Incomplete path from ${colony.room.print}`);
                }
            }
        }
        if (nearestColony) {
            return nearestColony;
        }
        return undefined
    }

    remove(force = false): number | undefined {
        if (!this.memory.persistent || force) {
            delete Collective.directives[this.name];
            Collective.director.removeDirective(this);
            delete global[this.name];
            if (this.base) {
                _.remove(this.base.flags, flag => flag.name == this.name);
            }
            if (this.flag) { // check in case flag was removed manually in last build cycle
                return this.flag.remove();
            }
        }
        return undefined
    }

    /* Create an appropriate flag to instantiate this directive in the next tick */
    static create(pos: RoomPosition, opts: DirectiveCreationOptions = {}): number | string {
        let flagName = opts.name || undefined;
        if (!flagName) {
            do {
                flagName = this.directiveName + ':' + randomHex(6);
            } while (Game.flags[flagName]);
        }
        if (!opts.quiet) {
            log.alert(`Creating ${this.directiveName} directive at ${pos.print}!`);
        }
        const result = pos.createFlag(flagName, this.color, this.secondaryColor) as string | number;
        if (result == flagName && opts.memory) {
            Memory.flags[flagName] = opts.memory;
        }
        //log.info(`Result: ${result}, memory: ${JSON.stringify(Memory.flags[result])}`);
        return result;
    }

    /* Whether a directive of the same type is already present (in room | at position) */
    static isPresent(pos: RoomPosition, scope: 'room' | 'pos'): boolean {
        const room = Game.rooms[pos.roomName] as Room | undefined;
        switch (scope) {
            case 'room':
                if (room) {
                    return _.filter(room.flags,
                        flag => this.filter(flag) &&
                            !(flag.memory.setPosition
                                && flag.memory.setPosition.roomName != pos.roomName)).length > 0;
                } else {
                    const flagsInRoom = _.filter(Game.flags, function (flag) {
                        if (flag.memory.setPosition) { // does it need to be relocated?
                            return flag.memory.setPosition.roomName == pos.roomName;
                        } else { // properly located
                            return flag.pos.roomName == pos.roomName;
                        }
                    });
                    return _.filter(flagsInRoom, flag => this.filter(flag)).length > 0;
                }
            case 'pos':
                if (room) {
                    return _.filter(pos.lookFor(LOOK_FLAGS),
                        flag => this.filter(flag) &&
                            !(flag.memory.setPosition
                                && !equalXYR(pos, flag.memory.setPosition))).length > 0;
                } else {
                    const flagsAtPos = _.filter(Game.flags, function (flag) {
                        if (flag.memory.setPosition) { // does it need to be relocated?
                            return equalXYR(flag.memory.setPosition, pos);
                        } else { // properly located
                            return equalXYR(flag.pos, pos);
                        }
                    });
                    return _.filter(flagsAtPos, flag => this.filter(flag)).length > 0;
                }
        }
    }

    /* Create a directive if one of the same type is not already present (in room | at position).
	 * Calling this method on positions in invisible rooms can be expensive and should be used sparingly. */
    static createIfNotPresent(pos: RoomPosition, scope: 'room' | 'pos',
        opts: DirectiveCreationOptions = {}): number | string | undefined {
        if (this.isPresent(pos, scope)) {
            return; // do nothing if flag is already here
        }
        const room = Game.rooms[pos.roomName]
        if (!room) {
            if (!opts.memory) {
                opts.memory = {};
            }
            opts.memory.setPosition = pos;
        }
        switch (scope) {
            case 'room':
                if (room) {
                    return this.create(pos, opts);
                } else {
                    log.info(`Creating directive at ${pos.print}... ` +
                        `No visibility in room; directive will be relocated on next tick.`);
                    let createAtPos: RoomPosition;
                    if (opts.memory && opts.memory.base) {
                        createAtPos = Pathing.findPathablePosition(opts.memory.base!);
                    } else {
                        createAtPos = Pathing.findPathablePosition(_.first(getAllBases())!.room.name);
                    }
                    return this.create(createAtPos, opts);
                }
            case 'pos':
                if (room) {
                    return this.create(pos, opts);
                } else {
                    log.info(`Creating directive at ${pos.print}... ` +
                        `No visibility in room; directive will be relocated on next tick.`);
                    let createAtPos: RoomPosition;
                    if (opts.memory && opts.memory.base) {
                        createAtPos = Pathing.findPathablePosition(opts.memory.base!);
                    } else {
                        createAtPos = Pathing.findPathablePosition(_.first(getAllBases())!.room.name);
                    }
                    return this.create(createAtPos, opts);
                }
        }
    }

    /* Filter for _.filter() that checks if a flag is of the matching type */
    static filter(flag: Flag): boolean {
        return flag.color == this.color && flag.secondaryColor == this.secondaryColor;
    }

    /* Map a list of flags to directives, accepting a filter */
    static find(flags: Flag[]): Directive[] {
        flags = _.filter(flags, flag => this.filter(flag));
        return _.compact(_.map(flags, flag => Collective.directives[flag.name]));
    }

    abstract spawnForemen(): void;

    /* Initialization logic goes here, called in overseer.init() */
    abstract init(): void;

    /* Runtime logic goes here, called in overseer.run() */
    abstract run(): void;

    // Overwrite this in child classes to display relevant information
    visuals(): void {

    }
}

import { profile } from "../profiler/Profiler";
import Directive from "./Directive";
import { PairDestroyerForeman } from "../foremen/PairDestroyer";
import { log } from "../console/log";
import Visualizer from "../visuals/Visualizer";
import CombatIntel from "../intel/CombatIntel";

/**
 * Spawns a pair of attacker/healer creeps to siege a room
 */
@profile
export class DirectivePairDestroy extends Directive {

    static directiveName = 'destroy';
    static color = COLOR_RED;
    static secondaryColor = COLOR_CYAN;

    foremen!: {
        destroy: PairDestroyerForeman;
    };

    constructor(flag: Flag) {
        super(flag);
    }

    spawnForemen() {
        this.foremen.destroy = new PairDestroyerForeman(this);
    }

    init(): void {
        //log.alert(`${this.print} Pair detroyer directive active`);
    }

    run(): void {
        // If there are no hostiles left in the room then remove the flag and associated healpoint
        if (this.room && this.room.hostiles.length == 0 && this.room.hostileStructures.length == 0) {
            log.notify(`Pair destroyer mission at ${this.pos.roomName} completed successfully.`);
            this.remove();
        }
    }

    visuals(): void {
        Visualizer.marker(this.pos, { color: 'red' });
        const fallback = CombatIntel.getFallbackFrom(this.pos);
        Visualizer.marker(fallback, { color: 'green' });
    }
}

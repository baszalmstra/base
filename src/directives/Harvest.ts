import Directive from "./Directive";
import MiningForeman from "../foremen/Mining";
import { ForemanPriority } from "../priorities/foremen";
import { Pathing } from "../movement/Pathing";
import { getCacheExpiration } from "../utilities/util";
import { _MEM } from "../Memory";
import { Cartographer, ROOMTYPE_SOURCEKEEPER } from "../utilities/Cartographer";
import { profile } from "../profiler";

// Because harvest directives are the most common, they have special shortened memory keys to minimize memory impact
export const _HARVEST_MEM_PATHING = 'P';
export const _HARVEST_MEM_USAGE = 'u';
export const _HARVEST_MEM_DOWNTIME = 'd';

interface DirectiveHarvestMemory extends FlagMemory {
    [_HARVEST_MEM_PATHING]?: {
        [_MEM.DISTANCE]: number,
        [_MEM.EXPIRATION]: number
    };
    [_HARVEST_MEM_USAGE]: number;
    [_HARVEST_MEM_DOWNTIME]: number;
}

const defaultDirectiveHarvestMemory: DirectiveHarvestMemory = {
    [_HARVEST_MEM_USAGE]: 1,
    [_HARVEST_MEM_DOWNTIME]: 0,
};

@profile
export default class DirectiveHarvest extends Directive {
    static directiveName = 'harvest';
    static color = COLOR_YELLOW;
    static secondaryColor = COLOR_YELLOW;

    memory!: DirectiveHarvestMemory;
    foremen!: {
        mine: MiningForeman;
    };

    constructor(flag: Flag) {
        super(flag);

        if (this.base) {
            this.base.miningSites[this.name] = this;
            this.base.destinations.push({ pos: this.pos, order: this.memory.tick || Game.time });
        }
    }

    // Hauling distance
    get distance(): number {
        if (!this.memory[_HARVEST_MEM_PATHING] || Game.time >= this.memory[_HARVEST_MEM_PATHING]![_MEM.EXPIRATION]) {
            const distance = Pathing.distance(this.base.pos, this.pos);
            const expiration = getCacheExpiration(this.base.storage ? 5000 : 1000);
            this.memory[_HARVEST_MEM_PATHING] = {
                [_MEM.DISTANCE]: distance,
                [_MEM.EXPIRATION]: expiration
            };
        }
        return this.memory[_HARVEST_MEM_PATHING]![_MEM.DISTANCE];
    }

    init(): void {
    }

    run(): void {
    }

    spawnForemen(): void {
        let priority = ForemanPriority.ownedRoom.mine;
        if (!(this.room && this.room.my)) {
            priority = Cartographer.roomType(this.pos.roomName) == ROOMTYPE_SOURCEKEEPER ?
                ForemanPriority.remoteSKRoom.mine : ForemanPriority.remoteRoom.mine;
        }
        this.foremen.mine = new MiningForeman(this, priority);
    }
}

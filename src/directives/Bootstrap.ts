import Directive from "./Directive";
import Base from "../Base";
import { Roles } from "../creepSetups/setups";
import { log } from "../console/log";
import BootstrapForeman from "../foremen/Bootstrap";
import { profile } from "../profiler";

@profile
export default class DirectiveBootstrap extends Directive {
    static directiveName = "bootstrap";
    static color = COLOR_ORANGE;
    static secondaryColor = COLOR_ORANGE;

    base!: Base;
    room!: Room;
    private needsMiner!: boolean;
    private needsSupplier!: boolean;

    foremen!: {
        bootstrap: BootstrapForeman,
    }

    constructor(flag: Flag) {
        super(flag);
        this.refresh();
    }

    refresh() {
        super.refresh();
        this.base.bootstrapping = true;
        this.needsMiner = this.base.getCreepsByRole(Roles.drone).length == 0;
        this.needsSupplier = this.base.getCreepsByRole(Roles.supplier).length == 0;
    }

    spawnForemen() {
        this.foremen.bootstrap = new BootstrapForeman(this);
    }

    init(): void {
        if (Game.time % 100 == 0) {
            log.alert(`Colony ${this.room.print} is in emergency recovery mode.`);
        }
    }

    run(): void {
        if (!this.needsSupplier && !this.needsMiner) {
            log.alert(`Colony ${this.room.print} has recovered from crash; removing bootstrap directive.`);
            // Suicide any fillers so they don't get in the way
            const overlord = this.foremen.bootstrap
            for (const filler of overlord.fillers) {
                filler.suicide();
            }
            this.remove();
        }
    }

}

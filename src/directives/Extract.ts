import Directive from "./Directive";
import { profile } from "../profiler/Profiler";
import { ForemanPriority } from "../priorities/foremen";
import { log } from "../console/log";
import { ExtractorForeman } from "../foremen/Extract";

/**
 * Mineral extraction directive. Spawns extraction creeps to operate extractors in owned or source keeper rooms
 */
@profile
export class DirectiveExtract extends Directive {

    static directiveName = 'extract';
    static color = COLOR_YELLOW;
    static secondaryColor = COLOR_CYAN;

    foremen!: {
        extract: ExtractorForeman;
    };

    constructor(flag: Flag) {
        super(flag);
        if (this.base) {
            this.base.destinations.push({ pos: this.pos, order: this.memory.tick || Game.time });
        }
    }

    spawnForemen() {
        let priority: number;
        if (this.room && this.room.my) {
            if (this.base.level == 8) {
                priority = ForemanPriority.ownedRoom.mineralRCL8;
            } else {
                priority = ForemanPriority.ownedRoom.mineral;
            }
        } else {
            priority = ForemanPriority.remoteSKRoom.mineral;
        }
        this.foremen.extract = new ExtractorForeman(this, priority);
    }

    init() {

    }

    run() {
        if (this.base.level < 6) {
            log.notify(`Removing extraction directive in ${this.pos.roomName}: room RCL insufficient.`);
            this.remove();
        }
    }

}

import Directive from "./Directive";
import RoomIntel from "../intel/RoomIntel";
import { log } from "../console/log";
import { Cartographer, ROOMTYPE_CONTROLLER } from "../utilities/Cartographer";
import { StationaryScoutForeman } from "../foremen/Stationary";
import { ReservingForeman } from "../foremen/Reserving";
import { profile } from "../profiler";

@profile
export default class DirectiveOutpost extends Directive {
    static directiveName = "outpost";
    static color = COLOR_PURPLE;
    static secondaryColor = COLOR_PURPLE;

    static settings = {
        canSpawnReserversAtRCL: 3,
    }

    spawnForemen(): void {
        if (this.base.level >= DirectiveOutpost.settings.canSpawnReserversAtRCL) {
            if (Cartographer.roomType(this.pos.roomName) == ROOMTYPE_CONTROLLER) {
                this.foremen.reserve = new ReservingForeman(this);
            }
        } else {
            this.foremen.scout = new StationaryScoutForeman(this);
        }
    }
    init(): void {
    }
    run(): void {
        if (RoomIntel.roomOwnedBy(this.pos.roomName)) {
            log.warning(`Removing ${this.print} since room is owned!`);
            this.remove();
        }
        if (Game.time % 10 == 3 && this.room && this.room.controller
            && !this.pos.isEqualTo(this.room.controller.pos) && !this.memory.setPosition) {
            this.setPosition(this.room.controller.pos);
        }
    }
}

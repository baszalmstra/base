import { profile } from "../profiler";
import Mem, { getAutonomyLevel, Autonomy } from "../Memory";
import _ from "lodash";
import Base, { getAllBases } from "../Base";
import { MAX_OWNED_ROOMS } from "../~settings";
import { Pathing } from "../movement/Pathing";
import { log } from "../console/log";
import { maxBy } from "../utilities/util";
import { Cartographer } from "../utilities/Cartographer";
import { MIN_EXPANSION_DISTANCE } from "../strategy/ExpansionEvaluator";
import { DirectiveColonize } from "../directives/Colonize";

const CHECK_EXPANSION_FREQUENCY = 500;

const UNOWNED_MINERAL_BONUS = 100;
const CATALYST_BONUS = 75;
const MAX_SCORE_BONUS = _.sum([UNOWNED_MINERAL_BONUS, CATALYST_BONUS]);

const TOO_CLOSE_PENALTY = 100;

interface ExpansionPlannerMemory {

}

const defaultExpansionPlannerMemory: ExpansionPlannerMemory = {};

@profile
export class ExpansionPlanner {

    memory: ExpansionPlannerMemory;

    constructor() {
        this.memory = Mem.wrap(Memory, 'expansionPlanner', defaultExpansionPlannerMemory);
    }

    refresh() {
        this.memory = Mem.wrap(Memory, 'expansionPlanner', defaultExpansionPlannerMemory);
    }

    private handleExpansion(): void {
        const allColonies = getAllBases();
        // If you already have max number of colonies, ignore
        if (allColonies.length >= Math.min(Game.gcl.level, MAX_OWNED_ROOMS)) {
            return;
        }

        const roomName = this.chooseNextBaseRoom();
        if (roomName) {
            const pos = Pathing.findPathablePosition(roomName);
            DirectiveColonize.createIfNotPresent(pos, 'room');
            log.notify(`Room ${roomName} selected as next colony! Creating colonization directive.`);
        }
    }

    private chooseNextBaseRoom(): string | undefined {
        // Generate a list of possible colonies to expand from based on level and whether they are already expanding
        // let possibleIncubators: Base[] = []; // TODO: support incubation
        const possibleColonizers: Base[] = [];
        for (const colony of getAllBases()) {
            // if (colony.level >= DirectiveIncubate.requiredRCL
            // 	&& _.filter(colony.flags, flag => DirectiveIncubate.filter(flag)).length == 0) {
            // 	possibleIncubators.push(colony);
            // }
            if (colony.level >= DirectiveColonize.requiredRCL
                && _.filter(colony.flags, flag => DirectiveColonize.filter(flag)).length == 0) {
                possibleColonizers.push(colony);
            }
        }
        const possibleBestExpansions = _.compact(_.map(possibleColonizers, col => this.getBestExpansionRoomFor(col)));
        log.debug(JSON.stringify(possibleBestExpansions));
        const bestExpansion = maxBy(possibleBestExpansions, choice => choice!.score);
        if (bestExpansion) {
            log.alert(`Next expansion chosen: ${bestExpansion.roomName} with score ${bestExpansion.score}`);
            return bestExpansion.roomName;
        } else {
            log.alert(`No viable expansion rooms found!`);
        }

        return undefined;
    }

    private getBestExpansionRoomFor(colony: Base): { roomName: string, score: number } | undefined {
        const allBaseRooms = _.fromPairs(_.map(getAllBases(),
            col => [col.room.name, true])) as { [roomName: string]: boolean };
        const allOwnedMinerals = _.map(getAllBases(), col => col.room.mineral!.mineralType) as MineralConstant[];
        let bestRoom: string = '';
        let bestScore: number = -Infinity;
        for (const roomName in colony.memory.expansionData.possibleExpansions) {
            let score = colony.memory.expansionData.possibleExpansions[roomName] as number | boolean;
            if (typeof score != 'number') continue;
            // Compute modified score
            if (score + MAX_SCORE_BONUS > bestScore) {
                // Is the room too close to an existing colony?
                const range2Rooms = Cartographer.findRoomsInRange(roomName, MIN_EXPANSION_DISTANCE);
                if (_.some(range2Rooms, roomName => allBaseRooms[roomName])) {
                    continue; // too close to another colony
                }
                const range3Rooms = Cartographer.findRoomsInRange(roomName, MIN_EXPANSION_DISTANCE + 1);
                if (_.some(range3Rooms, roomName => allBaseRooms[roomName])) {
                    score -= TOO_CLOSE_PENALTY;
                }
                // Are there powerful hostile rooms nearby?
                const adjacentRooms = Cartographer.findRoomsInRange(roomName, 1);
                if (_.some(adjacentRooms, roomName => Memory.rooms[roomName].avoid)) {
                    continue;
                }
                // Reward new minerals and catalyst rooms
                const mineralType = Memory.rooms[roomName].mineral
                    ? Memory.rooms[roomName].mineral!.type
                    : undefined;
                if (mineralType) {
                    if (!allOwnedMinerals.includes(mineralType)) {
                        score += UNOWNED_MINERAL_BONUS;
                    }
                    if (mineralType == RESOURCE_CATALYST) {
                        score += CATALYST_BONUS;
                    }
                }
                // Update best choices
                if (score > bestScore && Game.map.isRoomAvailable(roomName)) {
                    bestScore = score;
                    bestRoom = roomName;
                }
            }
        }
        if (bestRoom != '') {
            return { roomName: bestRoom, score: bestScore };
        }

        return undefined;
    }

    init(): void {

    }

    run(): void {
        if (Game.time % CHECK_EXPANSION_FREQUENCY == 17 && getAutonomyLevel() == Autonomy.Automatic) {
            this.handleExpansion();
        }
    }

}

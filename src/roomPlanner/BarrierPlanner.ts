import RoomPlanner, { getAllStructureCoordsFromLayout, translatePositions } from "./RoomPlanner"
import Base from "../Base";
import Mem from "../Memory";
import _ from "lodash";
import { getCutTiles } from "../algorithms/minCut";
import { BUNKER_RADIUS, insideBunkerBounds, bunkerLayout } from "./layouts/bunker";
import { profile } from "../profiler";
import { print } from "util";
import { Log, log } from "../console/log";
import { derefCoords } from "../utilities/util";

export interface BarrierPlannerMemory {
    barrierLookup: { [roadCoordName: string]: boolean | undefined }
}

const memoryDefaults = {
    barrierLookup: {},
}

@profile
export default class BarrierPlanner {
    roomPlanner: RoomPlanner;
    base: Base;
    memory: BarrierPlannerMemory;
    barrierPositions: RoomPosition[];

    static settings = {
        buildBarriersAtRCL: 3,
        padding: 3,
        bunkerizeRCL: 7,
    }

    constructor(roomPlanner: RoomPlanner) {
        this.roomPlanner = roomPlanner;
        this.base = roomPlanner.base;
        this.memory = Mem.wrap(this.base.memory, "barrierPlanner", memoryDefaults);
        this.barrierPositions = [];
    }

    refresh(): void {
        this.memory = Mem.wrap(this.base.memory, "barrierPlanner", memoryDefaults);
        this.barrierPositions = []
    }

    private computeBunkerBarrierPositions(bunkerPos: RoomPosition, upgradeSitePos: RoomPosition): RoomPosition[] {
        const rectArray = [];
        const padding = BarrierPlanner.settings.padding;
        if (bunkerPos) {
            const { x, y } = bunkerPos;
            const r = BUNKER_RADIUS - 1;
            let [x1, y1] = [Math.max(x - r - padding, 0), Math.max(y - r - padding, 0)];
            let [x2, y2] = [Math.min(x + r + padding, 49), Math.min(y + r + padding, 49)];
            // Make sure you don't leave open walls
            x1 = _.clamp(x1, 3, 50 - 3);
            x2 = _.clamp(x2, 3, 50 - 3);
            y1 = _.clamp(y1, 3, 50 - 3);
            y2 = _.clamp(y2, 3, 50 - 3);
            rectArray.push({ x1: x1, y1: y1, x2: x2, y2: y2 });
        }
        // Get Min cut
        const barrierCoords = getCutTiles(this.base.name, rectArray, false, 2, false);
        let positions = _.map(barrierCoords, coord => new RoomPosition(coord.x, coord.y, this.base.name));
        positions = positions.concat(upgradeSitePos.availableNeighbors(true));
        return positions;
    }

    init(): void {

    }

    /* Write everything to memory after roomPlanner is closed */
    finalize(): void {
        this.memory.barrierLookup = {};
        if (this.barrierPositions.length == 0) {
            if (this.roomPlanner.bunkerPos) {
                this.barrierPositions = this.computeBunkerBarrierPositions(this.roomPlanner.bunkerPos,
                    this.base.controller.pos);
            } else {
                log.error(`Couldn't generate barrier plan for ${this.base.name}!`);
            }
        }
        for (const pos of this.barrierPositions) {
            this.memory.barrierLookup[pos.coordName] = true;
        }
    }

    /* Quick lookup for if a barrier should be in this position. Barriers returning false won't be maintained. */
    barrierShouldBeHere(pos: RoomPosition): boolean {
        if (this.base.level >= BarrierPlanner.settings.bunkerizeRCL) {
            // Once you are high level, only maintain ramparts at bunker or controller
            return insideBunkerBounds(pos, this.base) || pos.getRangeTo(this.base.controller) == 1;
        } else {
            // Otherwise keep the normal plan up
            return !!this.memory.barrierLookup[pos.coordName] || pos.getRangeTo(this.base.controller) == 1;
        }
    }

    /* Create construction sites for any buildings that need to be built */
    private buildMissingRamparts(): void {
        // Max buildings that can be placed each tick
        let count = RoomPlanner.settings.maxSitesPerbase - this.base.constructionSites.length;

        // Build missing ramparts
        const barrierPositions: RoomPosition[] = [];
        for (const coord of _.keys(this.memory.barrierLookup)) {
            barrierPositions.push(derefCoords(coord, this.base.name));
        }

        // Add critical structures to barrier lookup
        const criticalStructures: Structure[] = _.compact([...this.base.towers,
        ...this.base.spawns,
        this.base.storage!,
        this.base.terminal!]);
        for (const structure of criticalStructures) {
            barrierPositions.push(structure.pos);
        }

        for (const pos of barrierPositions) {
            if (count > 0 && RoomPlanner.canBuild(STRUCTURE_RAMPART, pos) && this.barrierShouldBeHere(pos)) {
                const ret = pos.createConstructionSite(STRUCTURE_RAMPART);
                if (ret != OK) {
                    log.warning(`${this.base.name}: couldn't create rampart site at ${pos.print}. Result: ${ret}`);
                } else {
                    count--;
                }
            }
        }
    }

    private buildMissingBunkerRamparts(): void {
        if (!this.roomPlanner.bunkerPos) return;
        const bunkerCoords = getAllStructureCoordsFromLayout(bunkerLayout, this.base.level);
        bunkerCoords.push(bunkerLayout.data.anchor); // add center bunker tile
        let bunkerPositions = _.map(bunkerCoords, coord => new RoomPosition(coord.x, coord.y, this.base.name));
        bunkerPositions = translatePositions(bunkerPositions, bunkerLayout.data.anchor, this.roomPlanner.bunkerPos);
        let count = RoomPlanner.settings.maxSitesPerbase - this.base.constructionSites.length;
        for (const pos of bunkerPositions) {
            if (count > 0 && !pos.lookForStructure(STRUCTURE_RAMPART)
                && pos.lookFor(LOOK_CONSTRUCTION_SITES).length == 0) {
                const ret = pos.createConstructionSite(STRUCTURE_RAMPART);
                if (ret != OK) {
                    log.warning(`${this.base.name}: couldn't create bunker rampart at ${pos.print}. Result: ${ret}`);
                } else {
                    count--;
                }
            }
        }
    }

    run(): void {
        if (this.roomPlanner.active) {
            if (this.roomPlanner.bunkerPos) {
                this.barrierPositions = this.computeBunkerBarrierPositions(this.roomPlanner.bunkerPos,
                    this.base.controller.pos);
            }
            this.visuals();
        } else {
            if (!this.roomPlanner.memory.relocating && this.base.level >= BarrierPlanner.settings.buildBarriersAtRCL
                && this.roomPlanner.shouldRecheck(2)) {
                this.buildMissingRamparts();
                if (this.base.level >= 7) {
                    this.buildMissingBunkerRamparts();
                }
            }
        }
    }

    visuals(): void {
        for (const pos of this.barrierPositions) {
            this.base.room.visual.structure(pos.x, pos.y, STRUCTURE_RAMPART);
        }
    }
}

import Foreman from "./Foreman";
import DirectiveHarvest from "../directives/Harvest";
import { Roles, Setups } from "../creepSetups/setups";
import { Bot } from "../bots/Bot";
import { CreepSetup, bodyCost } from "../creepSetups/CreepSetup";
import { log } from "../console/log";
import _ from "lodash";
import $ from "../GlobalCache";
import { Pathing } from "../movement/Pathing";
import { BaseStage } from "../Base";
import RoomIntel from "../intel/RoomIntel";
import { ForemanPriority } from "../priorities/foremen";
import { profile } from "../profiler";
import { MY_USERNAME } from "../~settings";

export const StandardMinerSetupCost = bodyCost(Setups.drones.miners.standard.generateBody(Infinity));
export const DoubleMinerSetupCost = bodyCost(Setups.drones.miners.double.generateBody(Infinity));

const BUILD_OUTPUT_FREQUENCY = 15;
const SUICIDE_CHECK_FREQUENCY = 3;
const MINER_SUICIDE_THRESHOLD = 200;

@profile
export default class MiningForeman extends Foreman {

    directive: DirectiveHarvest;
    miners: Bot[];
    source: Source | undefined;
    container: StructureContainer | undefined;
    link: StructureLink | undefined;
    constructionSite: ConstructionSite | undefined;
    harvestPos: RoomPosition | undefined;
    energyPerTick: number;
    miningPowerNeeded: number;
    mode: 'early' | 'SK' | 'link' | 'standard' | 'double';
    setup: CreepSetup;
    minersNeeded: number;
    allowDropMining: boolean;

    static settings = {
        minLinkDistance: 10,
        dropMineUntilRCL: 3,
    };

    constructor(directive: DirectiveHarvest, priority: number) {
        super(directive, "mine", priority);
        this.directive = directive;
        this.miners = this.bots(Roles.drone);

        this.priority += this.outpostIndex * ForemanPriority.remoteRoom.roomIncrement;

        this.populateStructures();

        this.energyPerTick = SOURCE_ENERGY_NEUTRAL_CAPACITY / ENERGY_REGEN_TIME;
        this.miningPowerNeeded = Math.ceil(this.energyPerTick / HARVEST_POWER) + 1;

        if (this.base.room.energyCapacityAvailable < StandardMinerSetupCost) {
            this.mode = "early";
            this.setup = Setups.drones.miners.default;
        } else if (this.link) {
            this.mode = 'link';
            this.setup = Setups.drones.miners.default;
        } else {
            this.mode = "standard";
            this.setup = Setups.drones.miners.standard;

        }

        const miningPowerEach = this.setup.getBodyPotential(WORK, this.base)
        this.minersNeeded = Math.min(Math.ceil(this.miningPowerNeeded / miningPowerEach),
            this.pos.availableNeighbors(true).length);

        this.allowDropMining = this.base.level < MiningForeman.settings.dropMineUntilRCL;
        if (this.mode != 'early' && !this.allowDropMining) {
            if (this.container) {
                this.harvestPos = this.container.pos;
            } else if (this.link) {
                this.harvestPos = _.find(this.link.pos.availableNeighbors(true),
                    pos => pos.getRangeTo(this) == 1)!;
            } else {
                this.harvestPos = this.calculateContainerPos();
            }
        }
    }

    private registerEnergyRequests(): void {
        if (this.container) {
            const transportCapacity = 200 * this.base.level;
            const threshold = this.base.stage > BaseStage.Small ? 0.8 : 0.5;
            if (_.sum(_.values(this.container.store)) > threshold * transportCapacity) {
                this.base.logisticsNetwork.requestOutput(this.container, {
                    resourceType: 'all',
                    dAmountdt: this.energyPerTick
                });
            }
        }
        if (this.link) {
            // If the link will be full with next deposit from the miner
            const minerCapacity = 150;
            if (this.link.energy + minerCapacity > this.link.energyCapacity) {
                this.base.linkNetwork.requestTransmit(this.link);
            }
        }
    }

    init(): void {
        let reservedBy = RoomIntel.roomReservedBy(this.pos.roomName);
        let ownedBy = RoomIntel.roomOwnedBy(this.pos.roomName);
        if (RoomIntel.getSafetyData(this.pos.roomName).safeFor < 10) {
            this.minersNeeded = 0;
        } else if ((ownedBy != MY_USERNAME && ownedBy != undefined)
            || (reservedBy != MY_USERNAME && reservedBy != undefined)) {
            this.minersNeeded = 0;
        }
        this.wishlist(this.minersNeeded, this.setup)
        this.registerEnergyRequests();
    }

    private populateStructures() {
        if (Game.rooms[this.pos.roomName]) {
            this.source = _.first(this.pos.lookFor(LOOK_SOURCES));
            this.constructionSite = _.first(this.pos.findInRange(FIND_MY_CONSTRUCTION_SITES, 2));
            this.container = this.pos.findClosestByLimitedRange(Game.rooms[this.pos.roomName].containers, 1);
            this.link = this.pos.findClosestByLimitedRange(this.base.availableLinks, 2);
        }
    }

    get distance(): number {
        return this.directive.distance;
    }

    /**
	 * Calculate where the container output will be built for this site
	 */
    private calculateContainerPos(): RoomPosition {
        // log.debug(`Computing container position for mining overlord at ${this.pos.print}...`);
        let originPos: RoomPosition | undefined;
        if (this.base.storage) {
            originPos = this.base.storage.pos;
        } else if (this.base.roomPlanner.storagePos) {
            originPos = this.base.roomPlanner.storagePos;
        }
        if (originPos) {
            const path = Pathing.findShortestPath(this.pos, originPos).path;
            const pos = _.find(path, pos => pos.getRangeTo(this) == 1);
            if (pos) return pos;
        }
        // Shouldn't ever get here
        log.warning(`Last resort container position calculation for ${this.print}!`);
        return _.first(this.pos.availableNeighbors(true))!;
    }

    /**
	 * Add or remove containers as needed to keep exactly one of contaner | link
	 */
    private addRemoveContainer(): void {
        if (this.allowDropMining) {
            return; // only build containers in reserved, owned, or SK rooms
        }
        if (!this.room || (!this.room.my && !this.room.reservedByMe)) {
            return; // only build containers in rooms I control
        }
        // Create container if there is not already one being built and no link
        if (!this.container && !this.constructionSite && !this.link) {
            const containerPos = this.calculateContainerPos();
            const container = containerPos.lookForStructure(STRUCTURE_CONTAINER) as StructureContainer | undefined;
            if (container) {
                log.warning(`${this.print}: this.container out of sync at ${containerPos.print}`);
                this.container = container;
                return;
            }
            log.info(`${this.print}: building container at ${containerPos.print}`);
            const result = containerPos.createConstructionSite(STRUCTURE_CONTAINER);
            if (result != OK) {
                log.error(`${this.print}: cannot build container at ${containerPos.print}! Result: ${result}`);
            }
            return;
        }
        // Destroy container if link is nearby
        if (this.container && this.link) {
            // safety checks
            if (this.base.factory && this.container.pos.getRangeTo(this.base.factory) > 2 &&
                this.container.pos.getRangeTo(this.base.upgradeSite) > 3) {
                log.info(`${this.print}: container and link present; destroying container at ${this.container.pos.print}`);
                this.container.destroy();
            }
        }
    }

    private earlyMiningActions(miner: Bot) {
        if (miner.room != this.room) {
            return miner.goToRoom(this.pos.roomName);
        }

        // Container mining
        if (this.container) {
            if (this.container.hits < this.container.hitsMax
                && miner.carry.energy >= Math.min(miner.carryCapacity, REPAIR_POWER * miner.getActiveBodyparts(WORK))) {
                return miner.goRepair(this.container);
            } else {
                if (_.sum(_.values(miner.carry)) < miner.carryCapacity) {
                    return miner.goHarvest(this.source!);
                } else {
                    return miner.goTransfer(this.container);
                }
            }
        }

        // Build output site
        if (this.constructionSite) {
            if (miner.carry.energy >= Math.min(miner.carryCapacity, BUILD_POWER * miner.getActiveBodyparts(WORK))) {
                return miner.goBuild(this.constructionSite);
            } else {
                return miner.goHarvest(this.source!);
            }
        }

        // Drop mining
        if (this.allowDropMining) {
            miner.goHarvest(this.source!);
            if (miner.carry.energy > 0.8 * miner.carryCapacity) {
                const biggestDrop = _.maxBy(miner.pos.findInRange(miner.room.droppedEnergy, 1), (drop: Resource) => drop.amount);
                if (biggestDrop) {
                    miner.goDrop(biggestDrop.pos, RESOURCE_ENERGY);
                }
            }
        }

        return undefined
    }

    private standardMiningActions(miner: Bot) {
        // Approach mining site
        if (this.goToMiningSite(miner)) return;

        // Container mining
        if (this.container) {
            if (this.container.hits < this.container.hitsMax
                && miner.carry.energy >= Math.min(miner.carryCapacity, REPAIR_POWER * miner.getActiveBodyparts(WORK))) {
                return miner.repair(this.container);
            } else {
                return miner.harvest(this.source!);
            }
        }

        // Build output site
        if (this.constructionSite) {
            if (miner.carry.energy >= Math.min(miner.carryCapacity, BUILD_POWER * miner.getActiveBodyparts(WORK))) {
                return miner.build(this.constructionSite);
            } else {
                return miner.harvest(this.source!);
            }
        }

        // Drop mining
        if (this.allowDropMining) {
            let result = miner.harvest(this.source!);
            if (miner.carry.energy > 0.8 * miner.carryCapacity) { // move over the drop when you're close to full
                const biggestDrop = _.maxBy(miner.pos.findInRange(miner.room.droppedEnergy, 1), drop => drop.amount);
                if (biggestDrop) {
                    miner.goTo(biggestDrop);
                }
            }
            if (miner.carry.energy == miner.carryCapacity) { // drop when you are full
                miner.drop(RESOURCE_ENERGY);
            }
            return;
        }

        return
    }

    /**
	 * Move onto harvesting position or near to source (depending on early/standard mode)
	 */
    private goToMiningSite(miner: Bot): boolean {
        if (this.harvestPos) {
            if (!miner.pos.inRangeToPos(this.harvestPos, 0)) {
                miner.goTo(this.harvestPos);
                return true;
            }
        } else {
            if (!miner.pos.inRangeToPos(this.pos, 1)) {
                miner.goTo(this);
                return true;
            }
        }
        return false;
    }

    /**
	 * Actions for handling link mining
	 */
    private linkMiningActions(miner: Bot) {

        // Approach mining site
        if (this.goToMiningSite(miner)) return;

        // Link mining
        if (this.link) {
            miner.harvest(this.source!);
            if (miner.carry.energy > 0.9 * miner.carryCapacity) {
                miner.transfer(this.link, RESOURCE_ENERGY);
            }
            return;
        } else {
            log.warning(`Link miner ${miner.print} has no link!`);
        }
    }

    private handleMiner(miner: Bot) {
        // Flee hostiles
        if (miner.flee(miner.room.fleeDefaults, { dropEnergy: true })) {
            return;
        }

        if (this.mode == "early" || !this.harvestPos) {
            if (!miner.pos.inRangeTo(this.pos, 1)) {
                return miner.goTo(this)
            }
        } else {
            if (!miner.pos.inRangeToPos(this.harvestPos, 0)) {
                return miner.goTo(this.harvestPos, { range: 0 });
            }
        }

        switch (this.mode) {
            case 'early':
                return this.earlyMiningActions(miner);
            case 'link':
                return this.linkMiningActions(miner);
            case 'standard':
                return this.standardMiningActions(miner);
            // case 'SK':
            // 	return this.standardMiningActions(miner);
            // case 'double':
            // 	return this.standardMiningActions(miner);
            default:
                log.error(`UNHANDLED MINER STATE FOR ${miner.print} (MODE: ${this.mode})`);
                return
        }
    }

    /**
	 * Suicide outdated miners when their replacements arrive
	 */
    private suicideOldMiners(): boolean {
        if (this.miners.length > this.minersNeeded && this.source) {
            // if you have multiple miners and the source is visible
            const targetPos = this.harvestPos || this.source.pos;
            const minersNearSource = _.filter(this.miners,
                miner => miner.pos.getRangeTo(targetPos) <= SUICIDE_CHECK_FREQUENCY);
            if (minersNearSource.length > this.minersNeeded) {
                // if you have more miners by the source than you need
                const oldestMiner = _.minBy(minersNearSource, miner => miner.ticksToLive || 9999);
                if (oldestMiner && (oldestMiner.ticksToLive || 9999) < MINER_SUICIDE_THRESHOLD) {
                    // if the oldest miner will die sufficiently soon
                    oldestMiner.suicide();
                    return true;
                }
            }
        }
        return false;
    }

    refresh() {
        if (!this.room && Game.rooms[this.pos.roomName]) {
            this.populateStructures()
        }
        super.refresh();
        $.refresh(this, 'source', 'container', 'link', 'constructionSite');
    }

    run(): void {
        for (const miner of this.miners) {
            this.handleMiner(miner);
        }
        if (this.room && Game.time % BUILD_OUTPUT_FREQUENCY == 1) {
            this.addRemoveContainer();
        }
        if (Game.time % SUICIDE_CHECK_FREQUENCY == 0) {
            this.suicideOldMiners();
        }
    }
}

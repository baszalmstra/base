import { ForemanPriority } from '../priorities/foremen';
import Foreman from './Foreman';
import { getForeman, Bot } from '../bots/Bot';
import Base from '../Base';
import _ from 'lodash';
import { profile } from '../profiler/Profiler';

/**
 * This overlord contains the default actions for any creeps which lack an overlord (for example, miners whose
 * miningSite is no longer visible, or guards with no directive)
 */
@profile
export class DefaultForeman extends Foreman {

    idleBots: Bot[];

    constructor(base: Base) {
        super(base, 'default', ForemanPriority.default);
        this.idleBots = [];
    }

    init() {
        // Bots are collected at end of init phase; by now anything needing to be claimed already has been
        const idleCreeps = _.filter(this.base.creeps, creep => !getForeman(creep));
        this.idleBots = _.map(idleCreeps, creep => Collective.bots[creep.name] || new Bot(creep));
        for (const zerg of this.idleBots) {
            zerg.refresh();
        }
    }

    run() {

    }
}

import Foreman, { DEFAULT_PRESPAWN } from "./Foreman";
import Factory from "../structureGroups/Factory";
import { ForemanPriority } from "../priorities/foremen";
import { CreepSetup } from "../creepSetups/CreepSetup";
import { Setups, Roles } from "../creepSetups/setups";
import { Bot } from "../bots/Bot";
import { TaskWithdraw } from "../tasks/Withdraw";
import { Tasks } from "../tasks/Tasks";
import { profile } from "../profiler";

@profile
export default class FactoryForeman extends Foreman {

    factory: Factory;
    supplierSetup: CreepSetup;
    suppliers: Bot[];

    constructor(factory: Factory, priority: number = ForemanPriority.core.supplier) {
        super(factory, "supply", priority);
        this.factory = factory;
        this.supplierSetup = this.base.storage ? Setups.queens.default : Setups.queens.early;
        this.suppliers = this.bots(Roles.supplier)
    }

    init(): void {
        const amount = this.base.room.energyCapacityAvailable > 2000 ? 2 : 1;
        const prespawn = this.factory.spawns.length <= 1 ? 100 : DEFAULT_PRESPAWN;
        this.wishlist(amount, this.supplierSetup, { prespawn: prespawn })
    }

    private supplyActions(supplier: Bot) {
        const request = this.base.transportRequests.getPrioritizedClosestRequest(supplier.pos, 'supply');
        if (request) {
            supplier.task = Tasks.transfer(request.target);
        } else {
            this.rechargeActions(supplier)
        }
    }

    private rechargeActions(supplier: Bot) {
        if (this.factory.battery && this.factory.battery.energy > 0) {
            supplier.task = Tasks.withdraw(this.factory.battery);
        } else {
            supplier.task = Tasks.recharge();
        }
    }

    private idleActions(supplier: Bot): void {
        if (this.factory.battery && supplier.carry.energy < supplier.carryCapacity) {
            supplier.task = Tasks.withdraw(this.factory.battery);
        }
    }

    private handleSupplier(supplier: Bot) {
        if (supplier.carry.energy > 0) {
            this.supplyActions(supplier);
        } else {
            this.rechargeActions(supplier);
        }

        if (supplier.isIdle) {
            this.idleActions(supplier);
        }

        // If all of the above is done and hatchery is not in emergencyMode, move to the idle point and renew as needed
        //if (!this.emergencyMode && supplier.isIdle && this.base.factory) {
        // if (supplier.isIdle && this.base.factory) {
        //     if (supplier.pos.isEqualTo(this.base.factory.idlePos)) {
        //         // If queen is at idle position, renew her as needed
        //         if (supplier.ticksToLive && supplier.ticksToLive < this.settings.renewSupplierAt && this.base.factory.availableSpawns.length > 0) {
        //             this.base.factory.availableSpawns[0].renewCreep(supplier.creep);
        //         }
        //     } else {
        //         // Otherwise, travel back to idle position
        //         supplier.goTo(this.base.factory.idlePos);
        //     }
        // }
    }

    run(): void {
        for (const supplier of this.suppliers) {
            this.handleSupplier(supplier);
            if (supplier.hasValidTask) {
                supplier.run();
            } else {
                if (this.suppliers.length > 1) {
                    supplier.goTo(this.factory.idlePos, { range: 1 });
                } else {
                    supplier.goTo(this.factory.idlePos);
                }
            }
        }
    }
}

import Foreman from "./Foreman";
import { Bot } from "../bots/Bot";
import { ForemanPriority } from "../priorities/foremen";
import { Roles, Setups } from "../creepSetups/setups";
import _ from "lodash";
import { Tasks } from "../tasks/Tasks";
import Base from "../Base";
import { profile } from "../profiler";

const DEFAULT_NUM_SCOUTS = 3;

@profile
export default class ScoutForeman extends Foreman {
    scouts: Bot[];

    constructor(base: Base, priority = ForemanPriority.scouting.randomWalker) {
        super(base, 'scout', priority);
        this.scouts = this.bots(Roles.scout, { notifyWhenAttacked: false });
    }

    init() {
        this.wishlist(Math.min(DEFAULT_NUM_SCOUTS, this.base.level), Setups.scout);
    }

    private handleScout(scout: Bot) {
        // Stomp on enemy construction sites
        const enemyConstructionSites = scout.room.find(FIND_HOSTILE_CONSTRUCTION_SITES);
        if (enemyConstructionSites.length > 0 && enemyConstructionSites[0].pos.isWalkable(true)) {
            scout.goTo(enemyConstructionSites[0].pos);
            return;
        }
        // Check if room might be connected to newbie/respawn zone
        const indestructibleWalls = _.filter(scout.room.walls, wall => wall.hits == undefined);
        if (indestructibleWalls.length > 0) { // go back to origin base if you find a room near newbie zone
            scout.task = Tasks.goToRoom(this.base.room.name); // todo: make this more precise
        } else {
            // Pick a new room
            const neighboringRooms = _.values(Game.map.describeExits(scout.pos.roomName)) as string[];
            const roomName = _.sample(neighboringRooms);
            if (roomName && Game.map.isRoomAvailable(roomName)) {
                scout.task = Tasks.goToRoom(roomName);
            }
        }
    }

    run() {
        this.autoRun(this.scouts, scout => this.handleScout(scout));
    }
}

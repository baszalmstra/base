import { DirectiveColonize } from "../directives/Colonize";
import Foreman from "./Foreman";
import { ForemanPriority } from "../priorities/foremen";
import { Roles, Setups } from "../creepSetups/setups";
import _ from "lodash";
import { Bot } from "../bots/Bot";
import { profile } from "../profiler";
import { Pathing } from "../movement/Pathing";
import { log } from "../console/log";
import { Tasks } from "../tasks/Tasks";

/**
 * Spawn pioneers - early workers which help to build a spawn in a new colony, then get converted to workers or drones
 */
@profile
export class PioneerForeman extends Foreman {

    directive: DirectiveColonize;
    pioneers: Bot[];
    spawnSite: ConstructionSite | undefined;

    constructor(directive: DirectiveColonize, priority = ForemanPriority.colonization.pioneer) {
        super(directive, 'pioneer', priority);
        this.directive = directive;
        this.pioneers = this.bots(Roles.pioneer);
        this.spawnSite = this.room ? _.filter(this.room.constructionSites,
            s => s.structureType == STRUCTURE_SPAWN)[0] : undefined;
    }

    refresh() {
        super.refresh();
        this.spawnSite = this.room ? _.filter(this.room.constructionSites,
            s => s.structureType == STRUCTURE_SPAWN)[0] : undefined;
    }

    init() {
        this.wishlist(4, Setups.pioneer);
    }

    // private findStructureBlockingController(pioneer: Bot): Structure | undefined {
    //     const blockingPos = Pathing.findBlockingPos(pioneer.pos, pioneer.room.controller!.pos,
    //         _.filter(pioneer.room.structures, s => !s.isWalkable));
    //     if (blockingPos) {
    //         const structure = blockingPos.lookFor(LOOK_STRUCTURES)[0];
    //         if (structure) {
    //             return structure;
    //         } else {
    //             log.error(`${this.print}: no structure at blocking pos ${blockingPos.print}! (Why?)`);
    //         }
    //     }

    //     return undefined
    // }

    private handlePioneer(pioneer: Bot): void {
        // Ensure you are in the assigned room
        if (pioneer.room == this.room && !pioneer.pos.isEdge) {
            // Remove any blocking structures preventing claimer from reaching controller
            // if (!this.room.my && this.room.structures.length > 0) {
            //     const dismantleTarget = this.findStructureBlockingController(pioneer);
            //     if (dismantleTarget) {
            //         pioneer.task = Tasks.dismantle(dismantleTarget);
            //         return;
            //     }
            // }
            // Build and recharge
            if (pioneer.carry.energy == 0) {
                pioneer.task = Tasks.recharge();
            } else if (this.room && this.room.controller &&
                (this.room.controller.ticksToDowngrade < 2500 || !this.spawnSite) &&
                !(this.room.controller.upgradeBlocked > 0)) {
                // Save controller if it's about to downgrade or if you have nothing else to do
                pioneer.task = Tasks.upgrade(this.room.controller);
            } else if (this.spawnSite) {
                pioneer.task = Tasks.build(this.spawnSite);
            }
        } else {
            // pioneer.task = Tasks.goTo(this.pos);
            //pioneer.goTo(this.pos, { ensurePath: true, avoidSK: true, waypoints: this.directive.waypoints });
            pioneer.goTo(this.pos, { ensurePath: true, avoidSK: true });
        }
    }

    run() {
        this.autoRun(this.pioneers, pioneer => this.handlePioneer(pioneer));
    }
}


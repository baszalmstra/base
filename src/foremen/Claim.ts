import Directive from "../directives/Directive";
import { Bot } from "../bots/Bot";
import Foreman from "./Foreman";
import { ForemanPriority } from "../priorities/foremen";
import { Roles, Setups } from "../creepSetups/setups";
import $ from "../GlobalCache";
import { Pathing } from "../movement/Pathing";
import { Tasks } from "../tasks/Tasks";
import { profile } from "../profiler";
import _ from "lodash";

/**
 * Claim an unowned room
 */
@profile
export class ClaimingForeman extends Foreman {

    claimers: Bot[];
    directive: Directive;

    constructor(directive: Directive, priority = ForemanPriority.colonization.claim) {
        super(directive, 'claim', priority);
        this.directive = directive;
        this.claimers = this.bots(Roles.claim);
    }

    init() {
        const amount = $.number(this, 'claimerAmount', () => {
            if (this.room) { // if you have vision
                if (this.room.my) { // already claimed
                    return 0;
                } else { // don't ask for claimers if you can't reach controller
                    const pathablePos = this.room.creeps[0] ? this.room.creeps[0].pos
                        : Pathing.findPathablePosition(this.room.name);
                    if (!Pathing.isReachable(pathablePos, this.room.controller!.pos,
                        _.filter(this.room.structures, s => !s.isWalkable))) {
                        return 0;
                    }
                }
            }
            return 1; // otherwise ask for 1 claimer
        });
        this.wishlist(amount, Setups.infestors.claim);
    }

    private handleClaimer(claimer: Bot): void {
        if (claimer.room == this.room && !claimer.pos.isEdge) {
            if (!this.room.controller!.signedByMe) {
                // Takes care of an edge case where planned newbie zone signs prevents signing until room is reserved
                if (!this.room.my && this.room.controller!.signedByScreeps) {
                    claimer.task = Tasks.claim(this.room.controller!);
                } else {
                    claimer.task = Tasks.signController(this.room.controller!);
                }
            } else {
                claimer.task = Tasks.claim(this.room.controller!);
            }
        } else {
            //claimer.goTo(this.pos, { ensurePath: true, avoidSK: true, waypoints: this.directive.waypoints });
            claimer.goTo(this.pos, { ensurePath: true, avoidSK: true });
        }
    }

    run() {
        this.autoRun(this.claimers, claimer => this.handleClaimer(claimer));
    }
}

import Foreman from "./Foreman";
import DirectiveBootstrap from "../directives/Bootstrap";
import { ForemanPriority } from "../priorities/foremen";
import { Roles, Setups } from "../creepSetups/setups";
import { BaseStage } from "../Base";
import _ from "lodash";
import DirectiveHarvest from "../directives/Harvest";
import { SpawnRequest } from "../structureGroups/Factory";
import { Bot } from "../bots/Bot";
import { Tasks } from "../tasks/Tasks";
import { Log } from "../console/log";
import { profile } from "../profiler";

@profile
export default class BootstrapForeman extends Foreman {
    room!: Room;

    fillers: Bot[];
    withdrawStructures!: (StructureStorage | StructureTerminal | StructureContainer | StructureLink |
        StructureTower | StructureLab | StructurePowerSpawn | StructureNuker)[];
    supplyStructures!: (StructureSpawn | StructureExtension)[];

    static settings = {
        spawnBootstrapMinerThreshold: 2500
    };

    constructor(directive: DirectiveBootstrap, priority = ForemanPriority.emergency.bootstrap) {
        super(directive, "bootstrap", priority);
        this.fillers = this.bots(Roles.filler);
        this.refresh();
    }

    refresh() {
        this.supplyStructures = _.filter([...this.base.spawns, ...this.base.extensions],
            structure => structure.energy < structure.energyCapacity);
        this.withdrawStructures = _.filter(_.compact([
            this.base.storage!,
            this.base.terminal!,
            ...this.room.containers,
            ...this.room.towers,
        ]), structure => structure.energy > 0);
        super.refresh();
    }

    private spawnBootstrapMiners() {
        let miningSites = _.filter(_.values(this.base.miningSites), site => site.room == this.base.room);
        if (this.base.spawns[0]) {
            miningSites = _.sortBy(miningSites, site => site.pos.getRangeTo(this.base.spawns[0]));
        }
        const miningForemen = _.map(miningSites, site => site.foremen.mine);

        // Create a bootstrap miner and donate them to the mining site foreman as needed
        for (const foreman of miningForemen) {
            const filterMiners = this.lifetimeFilter(foreman.miners);
            const miningPowerAssigned = _.sum(_.map(this.lifetimeFilter(foreman.miners), creep => creep.getActiveBodyparts(WORK)));
            if (miningPowerAssigned < foreman.miningPowerNeeded &&
                filterMiners.length < foreman.pos.availableNeighbors().length) {
                if (this.base.factory) {
                    const request: SpawnRequest = {
                        setup: Setups.drones.miners.emergency,
                        foreman: foreman,
                        priority: this.priority + 1
                    }
                    this.base.factory.enqueue(request);
                }
            }
        }
    }

    init(): void {
        // At early levels, spawn one miner, then a filler, then the rest of the miners
        if (this.base.stage == BaseStage.Small) {
            if (this.base.getCreepsByRole(Roles.drone).length == 0) {
                this.spawnBootstrapMiners();
                return;
            }
        }
        // Spawn fillers
        if (this.base.getCreepsByRole(Roles.supplier).length == 0 && this.base.factory) { // no supplier
            const transporter = _.first(this.base.getBotByRole(Roles.transport));
            if (transporter) {
                // reassign transporter to be supplier
                transporter.reassign(this.base.factory.foreman, Roles.supplier);
            } else {
                // wish for a filler
                this.wishlist(1, Setups.filler);
            }
        }
        // Then spawn the rest of the needed miners
        const energyInStructures = _.sum(_.map(this.withdrawStructures, structure => structure.energy));
        const droppedEnergy = _.sumBy(this.room.droppedEnergy, drop => drop.amount);
        if (energyInStructures + droppedEnergy < BootstrapForeman.settings.spawnBootstrapMinerThreshold) {
            this.spawnBootstrapMiners();
        }
    }

    private supplyActions(filler: Bot) {
        const target = filler.pos.findClosestByRange(this.supplyStructures);
        if (target) {
            filler.task = Tasks.transfer(target);
        } else {
            this.rechargeActions(filler);
        }
    }

    private rechargeActions(filler: Bot) {
        const target = filler.pos.findClosestByRange(this.withdrawStructures);
        if (target) {
            filler.task = Tasks.withdraw(target);
        } else {
            filler.task = Tasks.recharge();
        }
    }

    private handleFiller(filler: Bot) {
        if (filler.carry.energy > 0) {
            this.supplyActions(filler);
        } else {
            this.rechargeActions(filler);
        }
    }

    run() {
        for (const filler of this.fillers) {
            if (filler.isIdle) {
                this.handleFiller(filler);
            }
            filler.run();
        }
    }
}

import Base from "../Base";
import _ from "lodash";
import { log } from "../console/log";
import { Bot, getForeman, setForeman } from "../bots/Bot";
import { CreepSetup } from "../creepSetups/CreepSetup";
import { setPriority } from "os";
import { SpawnRequest, SpawnRequestOptions } from "../structureGroups/Factory";
import { Pathing } from "../movement/Pathing";
import BattleBot from "../bots/BattleBot";

export interface OverlordInitializer {
    ref: string;
    room: Room | undefined;
    pos: RoomPosition;
    base: Base;
    memory: any;
    waypoints?: RoomPosition[];
}

export interface CreepRequestOptions {
    reassignIdle?: boolean;
    noLifetimeFilter?: boolean;
    prespawn?: number;
    priority?: number;
    partners?: CreepSetup[];
    options?: SpawnRequestOptions;
}

export const DEFAULT_PRESPAWN = 50;
export const MAX_SPAWN_REQUESTS = 100; // this stops division by zero or related errors from sending infinite requests

export interface BotOptions {
    notifyWhenAttacked?: boolean;
    //	boostWishlist?: _ResourceConstantSansEnergy[] | undefined;
}

export function hasBase(initializer: OverlordInitializer | Base): initializer is OverlordInitializer {
    return (<OverlordInitializer>initializer).base != undefined;
}

export default abstract class Foreman {
    initializer: OverlordInitializer | Base;
    room: Room | undefined;
    priority: number;                   // priorities can be changed in constructor phase but not after
    name: string;
    ref: string;
    pos: RoomPosition;
    base: Base

    isSuspended: boolean = false;

    private _creeps!: { [roleName: string]: Creep[] };
    private _bots: { [roleName: string]: Bot[] };
    private _battleBots: { [roleName: string]: BattleBot[] };
    creepUsageReport: { [roleName: string]: [number, number] | undefined };

    constructor(initializer: OverlordInitializer | Base, name: string, priority: number) {
        this.initializer = initializer;
        this.room = initializer.room;
        this.priority = priority;
        this.name = name;
        this.ref = `${initializer.ref}>${name}`;
        this.pos = initializer.pos;
        this.base = hasBase(initializer) ? initializer.base : initializer
        this._bots = {};
        this._battleBots = {};
        this.recalculateCreeps();

        this.creepUsageReport = _.mapValues(this._creeps, creep => undefined);

        Collective.foremen[this.ref] = this;
        Collective.director.registerForeman(this);
    }

    get print(): string {
        return `<a href="#!/room/${Game.shard.name}/${this.pos.roomName}">[${this.name}]</a>`;
    }

    /* Gets the "ID" of the outpost this overlord is operating in. 0 for owned rooms, >= 1 for outposts, -1 for other */
    get outpostIndex(): number {
        return _.findIndex(this.base.roomNames, roomName => roomName == this.pos.roomName);
    }

    /**
	 * Wraps all creeps of a given role to Bot objects and updates the contents in future ticks to
	 * avoid having to explicitly refresh groups of Bot
	 */
    protected bots(role: string, opts: BotOptions = {}): Bot[] {
        if (!this._bots[role]) {
            this._bots[role] = [];
            this.synchronizeBots(role, opts.notifyWhenAttacked);
        }
        return this._bots[role];
    }

    private synchronizeBots(role: string, notifyWhenAttacked?: boolean): void {
        // Synchronize the corresponding sets of Bot
        const botNames = _.fromPairs(_.map(this._bots[role] || [],
            bot => [bot.name, true])) as { [name: string]: boolean };
        const creepNames = _.fromPairs(_.map(this._creeps[role] || [],
            creep => [creep.name, true])) as { [name: string]: boolean };
        // Add new creeps which aren't in the _bots record
        for (const creep of this._creeps[role] || []) {
            if (!botNames[creep.name]) {
                this._bots[role].push(Collective.bots[creep.name] || new Bot(creep, notifyWhenAttacked));
            }
        }
        // Remove dead/reassigned creeps from the _bots record
        for (const bot of this._bots[role]) {
            if (!creepNames[bot.name]) {
                _.remove(this._bots[role], z => z.name == bot.name);
            }
        }
    }

    /**
	 * Wraps all creeps of a given role to BattleBot objects and updates the contents in future ticks
	 */
    protected battleBots(role: string, opts: BotOptions = {}): BattleBot[] {
        if (!this._battleBots[role]) {
            this._battleBots[role] = [];
            this.synchronizeBattleBots(role, opts.notifyWhenAttacked);
        }
        // if (opts.boostWishlist) {
        // 	this.boosts[role] = opts.boostWishlist;
        // }
        return this._battleBots[role];
    }

    private synchronizeBattleBots(role: string, notifyWhenAttacked?: boolean): void {
        // Synchronize the corresponding sets of BattleBot
        const botNames = _.fromPairs(_.map(this._battleBots[role] || [],
            bot => [bot.name, true])) as { [name: string]: boolean };
        const creepNames = _.fromPairs(_.map(this._creeps[role] || [],
            creep => [creep.name, true])) as { [name: string]: boolean };
        // Add new creeps which aren't in the _battleBots record
        for (const creep of this._creeps[role] || []) {
            if (!botNames[creep.name]) {
                if (Collective.bots[creep.name] && (<BattleBot>Collective.bots[creep.name]).isBattleBot) {
                    this._battleBots[role].push(Collective.bots[creep.name]);
                } else {
                    this._battleBots[role].push(new BattleBot(creep, notifyWhenAttacked));
                }
            }
        }
        // Remove dead/reassigned creeps from the _battleBots record
        for (const zerg of this._battleBots[role]) {
            if (!creepNames[zerg.name]) {
                _.remove(this._battleBots[role], z => z.name == zerg.name);
            }
        }
    }

    recalculateCreeps(): void {
        // Recalculate the sets of creeps for each role in this overlord
        this._creeps = _.mapValues(Collective.cache.foremen[this.ref],
            creepsOfRole => _.map(creepsOfRole, creepName => Game.creeps[creepName]));
        for (const role in this._bots) {
            this.synchronizeBots(role);
        }
        for (const role in this._battleBots) {
            this.synchronizeBattleBots(role);
        }
    }

    refresh(): void {
        this.room = Game.rooms[this.pos.roomName];
        // Refresh bots
        this.recalculateCreeps();
        for (const role in this._creeps) {
            for (const creep of this._creeps[role]) {
                if (Collective.bots[creep.name]) {
                    log.debug(`Refreshing creep ${creep.name}`)
                    Collective.bots[creep.name].refresh();
                } else {
                    log.warning(`${this.print}: could not find and refresh bots with name ${creep.name}!`);
                }
            }
        }
    }

    protected wishlist(quantity: number, setup: CreepSetup, opts: CreepRequestOptions = {}) {
        _.defaults(opts, {
            priority: this.priority,
            prespawn: DEFAULT_PRESPAWN,
            reassignIdle: false
        });
        let creepQuantity: number;
        if (opts.noLifetimeFilter) {
            creepQuantity = (this._creeps[setup.role] || []).length;
        } else if (_.has(this.initializer, "waypoints")) {
            creepQuantity = this.lifetimeFilter(this._creeps[setup.role] || [], opts.prespawn, 500).length;
        } else {
            creepQuantity = this.lifetimeFilter(this._creeps[setup.role] || [], opts.prespawn).length;
        }
        let spawnQuantity = quantity - creepQuantity;
        if (opts.reassignIdle && spawnQuantity > 0) {
            const idleCreeps = _.filter(this.base.getCreepsByRole(setup.role), creep => !getForeman(creep));
            for (let i = 0; i < Math.min(idleCreeps.length, spawnQuantity); i++) {
                setForeman(idleCreeps[i], this);
                spawnQuantity--;
            }
        }
        if (spawnQuantity > MAX_SPAWN_REQUESTS) {
            log.warning(`Too many requests for ${setup.role}s submitted by ${this.print}! (Check for errors.)`);
        } else if (spawnQuantity > 0) {
            //console.log(`wishlisting ${spawnQuantity} ${setup.role} at ${Game.time}`);
            for (let i = 0; i < spawnQuantity; i++) {
                this.requestCreep(setup, opts);
            }
        }
        this.creepReport(setup.role, creepQuantity, quantity);
    }

    protected creepReport(role: string, currentAmt: number, neededAmt: number) {
        this.creepUsageReport[role] = [currentAmt, neededAmt];
    }

    // TODO: include creep move speed
    lifetimeFilter(creeps: (Creep | Bot)[], prespawn = DEFAULT_PRESPAWN, spawnDistance?: number): (Creep | Bot)[] {
        if (!spawnDistance) {
            spawnDistance = 0;
            // if (this.spawnGroup) {
            // 	const distances = _.take(_.sortBy(this.spawnGroup.memory.distances), 2);
            // 	spawnDistance = (_.sum(distances) / distances.length) || 0;
            // } else
            if (this.base.factory) {
                // Use distance or 0 (in case distance returns something undefined due to incomplete pathfinding)
                spawnDistance = Pathing.distance(this.pos, this.base.factory.pos) || 0;
            }
            // if (this.base.isIncubating && this.base.spawnGroup) {
            // 	spawnDistance += this.base.spawnGroup.stats.avgDistance;
            // }
        }

		/* The last condition fixes a bug only present on private servers that took me a fucking week to isolate.
		 * At the tick of birth, creep.spawning = false and creep.ticksTolive = undefined
		 * See: https://screeps.com/forum/topic/443/creep-spawning-is-not-updated-correctly-after-spawn-process */
        return _.filter(creeps, creep =>
            creep.ticksToLive! > CREEP_SPAWN_TIME * creep.body.length + spawnDistance! + prespawn ||
            creep.spawning || (!creep.spawning && !creep.ticksToLive));
    }

    /**
	 * Create a creep setup and enqueue it to the Hatchery; does not include automatic reporting
	 */
    protected requestCreep(setup: CreepSetup, opts = {} as CreepRequestOptions) {
        _.defaults(opts, { priority: this.priority, prespawn: DEFAULT_PRESPAWN });
        const spawner = /* this.spawnGroup || this.base.spawnGroup || */ this.base.factory;
        if (spawner) {
            const request: SpawnRequest = {
                setup: setup,
                foreman: this,
                priority: opts.priority!,
            };
            if (opts.partners) {
                request.partners = opts.partners;
            }
            if (opts.options) {
                request.options = opts.options;
            }
            spawner.enqueue(request);
        } else {
            if (Game.time % 100 == 0) {
                log.warning(`Overlord ${this.ref} @ ${this.pos.print}: no spawner object!`);
            }
        }
    }

    abstract init(): void;

    abstract run(): void;

    visuals(): void { };

    /**
	 * Standard sequence of actions for running task-based creeps
	 */
    autoRun(roleCreeps: Bot[], taskHandler: (creep: Bot) => void, fleeCallback?: (creep: Bot) => boolean) {
        for (const creep of roleCreeps) {
            if (!!fleeCallback) {
                if (fleeCallback(creep)) continue;
            }
            if (creep.isIdle) {
                // if (this.shouldBoost(creep)) {
                // 	this.handleBoosting(creep);
                // } else {
                taskHandler(creep);
                //}
            }
            creep.run();
        }
    }

    canBoostSetup(setup: CreepSetup): boolean {
        // if (this.base.labratory && this.boosts[setup.role] && this.boosts[setup.role]!.length > 0) {
        // 	let energyCapacityAvailable: number;
        // 	if (this.spawnGroup) {
        // 		energyCapacityAvailable = this.spawnGroup.energyCapacityAvailable;
        // 	} else if (this.base.spawnGroup) {
        // 		energyCapacityAvailable = this.base.spawnGroup.energyCapacityAvailable;
        // 	} else if (this.base.hatchery) {
        // 		energyCapacityAvailable = this.base.hatchery.room.energyCapacityAvailable;
        // 	} else {
        // 		return false;
        // 	}
        // 	const body = _.map(setup.generateBody(energyCapacityAvailable), part => ({type: part, hits: 100}));
        // 	if (body.length == 0) return false;
        // 	return _.all(this.boosts[setup.role]!,
        // 				 boost => this.base.labratory!.canBoost(body, boost));
        // }
        return false;
    }

	/**
	 * Return whether you are capable of boosting a creep to the desired specifications
	 */
    shouldBoost(creep: Bot, onlyBoostInSpawn = false): boolean {
        // // Can't boost if there's no evolution chamber or TTL is less than threshold
        // const base = Overmind.colonies[creep.room.name] as Colony | undefined;
        // const labratory = base ? base.labratory : undefined;
        // if (!labratory ||
        // 	(creep.ticksToLive && creep.ticksToLive < MIN_LIFETIME_FOR_BOOST * creep.lifetime)) {
        // 	return false;
        // }

        // // EDIT: they removed in-spawn boosting... RIP :(
        // // // If you're in a bunker layout at level 8 with max labs, only boost while spawning
        // // if (onlyBoostInSpawn && this.base.bunker && this.base.level == 8 && this.base.labs.length == 10) {
        // // 	if (!creep.spawning) {
        // // 		return false;
        // // 	}
        // // }

        // // Otherwise just boost if you need it and can get the resources
        // if (this.boosts[creep.roleName]) {
        // 	const boosts = _.filter(this.boosts[creep.roleName]!, boost =>
        // 		(creep.boostCounts[boost] || 0) < creep.getActiveBodyparts(boostParts[boost]));
        // 	if (boosts.length > 0) {
        // 		return _.all(boosts, boost => labratory!.canBoost(creep.body, boost));
        // 	}
        // }
        return false;
    }

    /**
	 * Handle boosting of a creep; should be called during run()
	 */
    protected handleBoosting(creep: Bot): void {
        // const colony = Collective.bases[creep.room.name] as Base | undefined;
        // const evolutionChamber = colony ? colony.labratory : undefined;

        // if (this.boosts[creep.roleName] && evolutionChamber) {
        // 	const boosts = _.filter(this.boosts[creep.roleName]!, boost =>
        // 		(creep.boostCounts[boost] || 0) < creep.getActiveBodyparts(boostParts[boost]));
        // 	for (const boost of boosts) {
        // 		const boostLab = _.find(evolutionChamber.boostingLabs, lab => lab.mineralType == boost);
        // 		if (boostLab) {
        // 			creep.task = Tasks.getBoosted(boostLab, boost);
        // 		}
        // 	}
        // }
    }
}

import Foreman from "./Foreman";
import DirectiveOutpost from "../directives/Outpost";
import { ForemanPriority } from "../priorities/foremen";
import { Roles, Setups } from "../creepSetups/setups";
import { Bot } from "../bots/Bot";
import RoomIntel from "../intel/RoomIntel";
import { MY_USERNAME } from "../~settings";
import { Tasks } from "../tasks/Tasks";
import { log } from "../console/log";

/**
 * Spawns reservers to reserve an outpost room
 */
export class ReservingForeman extends Foreman {

    reservers: Bot[];
    reserveBuffer: number;

    constructor(directive: DirectiveOutpost, priority = ForemanPriority.remoteRoom.reserve) {
        super(directive, 'reserve', priority);
        // Change priority to operate per-outpost
        this.priority += this.outpostIndex * ForemanPriority.remoteRoom.roomIncrement;
        this.reserveBuffer = 2000;
        this.reservers = this.bots(Roles.claim);
    }

    init() {
        let amount = 0;
        if (this.room) {
            if (this.room.controller!.needsReserving(this.reserveBuffer)) {
                amount = 1;
            }
        } else if (RoomIntel.roomReservedBy(this.pos.roomName) == MY_USERNAME &&
            RoomIntel.roomReservationRemaining(this.pos.roomName) < 1000) {
            amount = 1;
        } else if (RoomIntel.roomOwnedBy(this.pos.roomName) == undefined) {
            amount = 1;
        }
        //log.info(`reservers need for ${this.ref} ${amount}, hasRoom: ${this.room != undefined}`)
        this.wishlist(amount, Setups.infestors.reserve);
    }

    private handleReserver(reserver: Bot): void {
        if (reserver.room == this.room && !reserver.pos.isEdge) {
            // If reserver is in the room and not on exit tile
            if (!this.room.controller!.signedByMe) {
                // Takes care of an edge case where planned newbie zone signs prevents signing until room is reserved
                if (!this.room.my && this.room.controller!.signedByScreeps) {
                    reserver.task = Tasks.reserve(this.room.controller!);
                } else {
                    reserver.task = Tasks.signController(this.room.controller!);
                }
            } else {
                reserver.task = Tasks.reserve(this.room.controller!);
            }
        } else {
            // reserver.task = Tasks.goTo(this.pos);
            reserver.goTo(this.pos);
        }
    }

    run() {
        this.autoRun(this.reservers, reserver => this.handleReserver(reserver));
    }
}

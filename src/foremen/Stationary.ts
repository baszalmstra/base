import { ForemanPriority } from "../priorities/foremen";
import Directive from "../directives/Directive";
import { Roles, Setups } from "../creepSetups/setups";
import Foreman from "./Foreman";
import _ from "lodash";
import { Bot } from "../bots/Bot";

/**
 * Sends out a stationary scout, which travels to a waypoint and remains there indefinitely
 */
export class StationaryScoutForeman extends Foreman {

    scouts: Bot[];

    constructor(directive: Directive, priority = ForemanPriority.scouting.stationary) {
        super(directive, 'scout', priority);
        this.scouts = this.bots(Roles.scout, { notifyWhenAttacked: false });
    }

    init() {
        this.wishlist(1, Setups.scout);
    }

    run() {
        for (const scout of this.scouts) {
            if (this.pos.roomName == scout.room.name) {
                const enemyConstructionSites = scout.room.find(FIND_HOSTILE_CONSTRUCTION_SITES);
                const squashTarget = _.first(enemyConstructionSites);
                if (squashTarget) {
                    scout.goTo(squashTarget);
                    return;
                }
            }

            if (!(scout.pos.inRangeTo(this.pos, 3) && !scout.pos.isEdge)) {
                scout.goTo(this.pos, { range: 3 });
            }
        }
    }
}

import Foreman from "./Foreman";
import UpgradeSite from "../structureGroups/UpgradeSite";
import { ForemanPriority } from "../priorities/foremen";
import { Roles, Setups } from "../creepSetups/setups";
import { Bot } from "../bots/Bot";
import { Tasks } from "../tasks/Tasks";
import { profile } from "../profiler";

/**
 * Spawns an upgrade to upgrade the room controller
 */
@profile
export default class UpgradeForeman extends Foreman {
    upgradersNeeded: number = 0;
    upgraders: Bot[];
    upgradeSite: UpgradeSite;
    settings!: { [property: string]: number };
    room!: Room

    constructor(upgradeSite: UpgradeSite, priority = ForemanPriority.upgrading.upgrade) {
        super(upgradeSite, "upgrade", priority);
        this.upgradeSite = upgradeSite;
        this.upgraders = this.bots(Roles.upgrader)
    }

    init(): void {
        if (this.base.level < 3) {
            return;
        }
        if (this.base.assets[RESOURCE_ENERGY] > UpgradeSite.settings.energyBuffer
            || this.upgradeSite.controller.ticksToDowngrade < 500) {
            const setup = this.base.level == 8 ? Setups.upgraders.rcl8 : Setups.upgraders.default;
            if (this.base.level == 8) {
                this.wishlist(1, setup);
            } else {
                const upgradePowerEach = setup.getBodyPotential(WORK, this.base);
                const upgradersNeeded = Math.ceil(this.upgradeSite.upgradePowerNeeded / upgradePowerEach);
                this.wishlist(upgradersNeeded, setup);
            }
        }
    }

    private handleUpgrader(upgrader: Bot): void {
        if (upgrader.carry.energy > 0) {
            // Repair link
            if (this.upgradeSite.link && this.upgradeSite.link.hits < this.upgradeSite.link.hitsMax) {
                upgrader.task = Tasks.repair(this.upgradeSite.link);
                return;
            }
            // Repair container
            if (this.upgradeSite.battery && this.upgradeSite.battery.hits < this.upgradeSite.battery.hitsMax) {
                upgrader.task = Tasks.repair(this.upgradeSite.battery);
                return;
            }
            // Build construction site
            const inputSite = this.upgradeSite.findInputConstructionSite();
            if (inputSite) {
                upgrader.task = Tasks.build(inputSite);
                return;
            }
            // Sign controller if needed
            if (!this.upgradeSite.controller.signedByMe &&
                !this.upgradeSite.controller.signedByScreeps) {
                upgrader.task = Tasks.signController(this.upgradeSite.controller);
                return;
            }
            upgrader.task = Tasks.upgrade(this.upgradeSite.controller);
        } else {
            // Recharge from link or battery
            if (this.upgradeSite.link && this.upgradeSite.link.energy > 0) {
                upgrader.task = Tasks.withdraw(this.upgradeSite.link);
            } else if (this.upgradeSite.battery && this.upgradeSite.battery.energy > 0) {
                upgrader.task = Tasks.withdraw(this.upgradeSite.battery);
            }
            // Find somewhere else to recharge from
            else { // TODO: BUG HERE IF NO UPGRADE CONTAINER
                if (this.upgradeSite.battery && this.upgradeSite.battery.targetedBy.length == 0) {
                    upgrader.task = Tasks.recharge();
                }
            }
        }
    }

    run() {
        this.autoRun(this.upgraders, upgrader => this.handleUpgrader(upgrader));
    }
}

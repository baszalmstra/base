import Directive from "./directives/Directive";
import Foreman from "./foremen/Foreman";
import _ from "lodash";
import Base from "./Base";
import { Roles } from "./creepSetups/setups";
import { bodyCost } from "./creepSetups/CreepSetup";
import DirectiveBootstrap from "./directives/Bootstrap";
import { hasJustSpawned, onPublicServer, derefCoords, minBy } from "./utilities/util";
import { Log, log } from "./console/log";
import LogisticsNetwork from "./logistics/LogisticsNetwork";
import { getAutonomyLevel, Autonomy } from "./Memory";
import { Cartographer, ROOMTYPE_CONTROLLER, ROOMTYPE_SOURCEKEEPER } from "./utilities/Cartographer";
import RoomIntel from "./intel/RoomIntel";
import { Pathing } from "./movement/Pathing";
import DirectiveOutpost from "./directives/Outpost";
import DirectiveGuard from "./directives/Guard";
import { profile } from "./profiler";
import { DirectiveOutpostDefense } from "./directives/OutpostDefense";
import { DirectivePairDestroy } from "./directives/PairDestroy";

@profile
export default class Director implements IDirector {
    private foremen: Foreman[];
    private foremenByBase: { [col: string]: Foreman[] };	// Overlords grouped by colony
    private directives: Directive[];

    static settings = {
        outpostCheckFrequency: onPublicServer() ? 250 : 100
    };

    constructor() {
        this.directives = []
        this.foremen = []
        this.foremenByBase = {}
    }

    private get bases(): Base[] {
        return _.values(Collective.bases);
    }

    refresh() {

    }

    registerDirective(directive: Directive): void {
        this.directives.push(directive);
        for (const name in directive.foremen) {
            this.removeForeman(directive.foremen[name])
        }
    }
    removeDirective(directive: Directive): void {
        _.remove(this.directives, dir => dir.ref == directive.ref);
        for (const name in directive.foremen) {
            this.removeForeman(directive.foremen[name]);
        }
    }

    registerForeman(foreman: Foreman): void {
        this.foremen.push(foreman);
        if (!this.foremenByBase[foreman.base.name]) {
            this.foremenByBase[foreman.base.name] = [];
        }
        this.foremenByBase[foreman.base.name].push(foreman);
    }

    getForemenForBase(base: Base): Foreman[] {
        return this.foremenByBase[base.name];
    }

    removeForeman(foreman: Foreman): void {
        _.remove(this.foremen, f => f.ref == foreman.ref);
        if (this.foremenByBase[foreman.base.name]) {
            _.remove(this.foremenByBase[foreman.base.name], o => o.ref == foreman.ref);
        }
    }

    private registerLogisticsRequests(base: Base): void {
        // Register logistics requests for all dropped resources and tombstones
        for (const room of base.rooms) {
            // Pick up all nontrivial dropped resources
            for (const resourceType in room.drops) {
                for (const drop of room.drops[resourceType]) {
                    if (drop.amount > LogisticsNetwork.settings.droppedEnergyThreshold
                        || drop.resourceType != RESOURCE_ENERGY) {
                        base.logisticsNetwork.requestOutput(drop);
                    }
                }
            }
        }
        // Place a logistics request directive for every tombstone with non-empty store that isn't on a container
        for (const tombstone of base.tombstones) {
            if (_.sum(_.values(tombstone.store)) > LogisticsNetwork.settings.droppedEnergyThreshold
                || _.sum(_.values(tombstone.store)) > tombstone.store.energy) {
                //if (base.bunker && tombstone.pos.isEqualTo(base.bunker.anchor)) continue;
                base.logisticsNetwork.requestOutput(tombstone, { resourceType: 'all' });
            }
        }
    }

    private handleBootstrapping(base: Base) {
        // Bootstrap directive: in the event of catastrophic room crash, enter emergency spawn mode.
        // Doesn't apply to incubating colonies.
        //if (!base.isIncubating) {
        const noSupplier = base.getCreepsByRole(Roles.supplier).length == 0;
        if (noSupplier && base.factory) { //&& !base.spawnGroup) {
            const setup = base.factory.foreman.supplierSetup;
            const energyToMakeQueen = bodyCost(setup.generateBody(base.room.energyCapacityAvailable));
            if (base.room.energyAvailable < energyToMakeQueen || hasJustSpawned()) {
                const result = DirectiveBootstrap.createIfNotPresent(base.factory.pos, 'pos');
                if (typeof result == 'string' || result == OK) { // successfully made flag
                    base.factory.settings.suppressSpawning = true;
                }
            }
        }
        //}
    }

    private computePossibleOutposts(base: Base, depth = 3): string[] {
        return _.filter(Cartographer.findRoomsInRange(base.room.name, depth), roomName => {
            if (Cartographer.roomType(roomName) != ROOMTYPE_CONTROLLER) {
                return false;
            }
            const alreadyAnOutpost = _.some(Collective.cache.outpostFlags, flag => (flag.memory.setPosition || flag.pos).roomName == roomName);
            const alreadyABase = Collective.bases[roomName];
            if (alreadyABase || alreadyAnOutpost) {
                return false;
            }
            const alreadyOwned = RoomIntel.roomOwnedBy(roomName);
            const alreadyReserved = RoomIntel.roomReservedBy(roomName);
            if (alreadyOwned || alreadyReserved) {
                return false;
            }
            const neighboringRooms = _.values(Game.map.describeExits(roomName)) as string[];
            const isReachableFrombase = _.some(neighboringRooms, r => base.roomNames.includes(r));
            return isReachableFrombase && Game.map.isRoomAvailable(roomName);
        })
    }

    private handleNewOutposts(base: Base) {

        // Do no create new outposts when bootstrapping
        if (base.bootstrapping) {
            return;
        }

        const numSources = _.sumBy(base.roomNames,
            roomName => Memory.rooms[roomName] && Memory.rooms[roomName].sources
                ? Memory.rooms[roomName].sources!.length
                : 0);
        const numRemotes = numSources - base.room.sources.length;
        if (numRemotes < Base.settings.remoteSourcesByLevel[base.level]) {
            const possibleOutposts = this.computePossibleOutposts(base);
            const origin = base.pos;
            const bestOutpost = minBy(possibleOutposts, roomName => {
                if (!Memory.rooms[roomName]) return false;
                const sourceCoords = Memory.rooms[roomName].sources;
                if (!sourceCoords) return false;
                const sourcePositions = _.map(sourceCoords, src => derefCoords(src.c, roomName));
                const sourceDistances = _.map(sourcePositions, pos => Pathing.distance(origin, pos));
                if (_.some(sourceDistances, dist => dist == undefined || dist > Base.settings.maxSourceDistance)) {
                    return false;
                }
                return _.sum(sourceDistances) / sourceDistances.length;
            })

            if (bestOutpost) {
                const pos = Pathing.findPathablePosition(bestOutpost);
                log.info(`Colony ${base.room.print} now remote mining from ${pos.print}`);
                DirectiveOutpost.createIfNotPresent(pos, 'room', { memory: { base: base.name } });
            }
        }
    }

    private handleOutpostDefense(base: Base) {
        for (const room of base.outposts) {
            // Handle player defense
            if (room.dangerousPlayerHostiles.length > 0) {
                DirectiveOutpostDefense.createIfNotPresent(Pathing.findPathablePosition(room.name), 'room');
                return;
            }
            // Handle NPC invasion directives
            if (Cartographer.roomType(room.name) != ROOMTYPE_SOURCEKEEPER) {
                const defenseFlag = _.filter(room.flags, flag => DirectiveGuard.filter(flag));
                if (room.dangerousHostiles.length > 0 && defenseFlag.length == 0) {
                    DirectiveGuard.create(room.dangerousHostiles[0].pos);
                }
                const pairDestroy = _.filter(room.flags, flag => DirectivePairDestroy.filter(flag));
                if (room.invaderCores.length > 0 && pairDestroy.length == 0) {
                    DirectivePairDestroy.create(room.invaderCores[0].pos);
                }
            }
        }
    }

    private placeDirectives(base: Base): void {
        this.handleBootstrapping(base);
        this.handleOutpostDefense(base);
        if (getAutonomyLevel() > Autonomy.Manual) {
            if (Game.time % Director.settings.outpostCheckFrequency == 2 * base.id) {
                this.handleNewOutposts(base);
            }
        }
    }

    init(): void {
        // Initialize directives
        for (const directive of this.directives) {
            directive.init();
        }

        // Sort foreman by priority if needed (assumes priority does not change after constructor phase)
        this.foremen.sort((f1, f2) => f1.priority - f2.priority);

        // Initialize foreman
        for (const foreman of this.foremen) {
            foreman.init()
        }
        // Register cleanup requests to logistics network
        for (const colony of this.bases) {
            this.registerLogisticsRequests(colony);
        }
    }
    run(): void {
        for (const directive of this.directives) {
            directive.run();
        }

        for (const foreman of this.foremen) {
            foreman.run()
        }

        for (const base of this.bases) {
            this.placeDirectives(base)
        }
    }

    getCreepReport(base: Base): string[][] {
        const spoopyBugFix = false;
        const roleOccupancy: { [role: string]: [number, number] } = {};

        for (const foreman of this.foremenByBase[base.name]) {
            for (const role in foreman.creepUsageReport) {
                const report = foreman.creepUsageReport[role];
                if (report == undefined) {
                    if (Game.time % 100 == 0) {
                        log.info(`Role ${role} is not reported by ${foreman.ref}!`);
                    }
                } else {
                    if (roleOccupancy[role] == undefined) {
                        roleOccupancy[role] = [0, 0];
                    }
                    roleOccupancy[role][0] += report[0];
                    roleOccupancy[role][1] += report[1];
                    if (spoopyBugFix) { // bizzarely, if you comment these lines out, the creep report is incorrect
                        log.debug(`report: ${JSON.stringify(report)}`);
                        log.debug(`occupancy: ${JSON.stringify(roleOccupancy)}`);
                    }
                }
            }
        }


        // let padLength = _.max(_.map(_.keys(roleOccupancy), str => str.length)) + 2;
        const roledata: string[][] = [];
        for (const role in roleOccupancy) {
            const [current, needed] = roleOccupancy[role];
            // if (needed > 0) {
            // 	stringReport.push('| ' + `${role}:`.padRight(padLength) +
            // 					  `${Math.floor(100 * current / needed)}%`.padLeft(4));
            // }
            roledata.push([role, `${current}/${needed}`]);
        }
        return roledata;
    }

    visuals(): void {
        for (const directive of this.directives) {
            directive.visuals();
        }
        for (const overlord of this.foremen) {
            overlord.visuals();
        }
    }
}

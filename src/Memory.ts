import _ from "lodash";
import { DEFAULT_OPERATION_MODE, USE_PROFILER } from "./~settings";
import { log } from "./console/log";
import { profile } from "./profiler";

export const enum _MEM {
    TICK = 'T',
    EXPIRATION = 'X',
    COLONY = 'C',
    OVERLORD = 'O',
    DISTANCE = 'D',
}

export const enum _RM {
    AVOID = 'a',
    SOURCES = 's',
    CONTROLLER = 'c',
    MINERAL = 'm',
    SKLAIRS = 'k',
    EXPANSION_DATA = 'e',
    INVASION_DATA = 'v',
    HARVEST = 'h',
    CASUALTIES = 'd',
    SAFETY = 'f',
    PREV_POSITIONS = 'p',
    CREEPS_IN_ROOM = 'cr',
    IMPORTANT_STRUCTURES = 'i',
    PORTALS = 'pr',
}

export const enum _RM_IS {
    TOWERS = 't',
    SPAWNS = 'sp',
    STORAGE = 's',
    TERMINAL = 'e',
    WALLS = 'w',
    RAMPARTS = 'r',
}

export const enum _RM_CTRL {
    LEVEL = 'l',
    OWNER = 'o',
    RESERVATION = 'r',
    RES_USERNAME = 'u',
    RES_TICKSTOEND = 't',
    SAFEMODE = 's',
    SAFEMODE_AVAILABLE = 'sa',
    SAFEMODE_COOLDOWN = 'sc',
    PROGRESS = 'p',
    PROGRESS_TOTAL = 'pt',
}

export enum Autonomy {
    Manual = 0,
    SemiAutomatic = 1,
    Automatic = 2,
}

export function getAutonomyLevel(): number {
    switch (Memory.settings.operationMode) {
        case ('manual'):
            return Autonomy.Manual;
        case ('semiautomatic'):
            return Autonomy.SemiAutomatic;
        case ('automatic'):
            return Autonomy.Automatic;
        default:
            log.warning(`ERROR: ${Memory.settings.operationMode} is not a valid operation mode! ` +
                `Defaulting to ${DEFAULT_OPERATION_MODE}; use setMode() to change.`);
            Memory.settings.operationMode = DEFAULT_OPERATION_MODE;
            return getAutonomyLevel();
    }
}

const MAX_BUCKET = 10000;
const HEAP_CLEAN_FREQUENCY = 200;
const BUCKET_CLEAR_CACHE = 7000;
const BUCKET_CPU_HALT = 4000;

@profile
export default class Mem {
    static shouldRun(): boolean {
        let shouldRun: boolean = true;
        // if (USE_PROFILER && Game.time % 10 == 0) {
        // 	log.warning(`Profiling is currently enabled; only ${PROFILER_COLONY_LIMIT} colonies will be run!`);
        // }
        if (Game.cpu.bucket < 500) {
            if (_.keys(Game.spawns).length > 1 && !Memory.resetBucket && !Memory.haltTick) {
                // don't run CPU reset routine at very beginning or if it's already triggered
                log.warning(`CPU bucket is critically low (${Game.cpu.bucket})! Starting CPU reset routine.`);
                Memory.resetBucket = true;
                Memory.haltTick = Game.time + 1; // reset global next tick
            } else {
                log.info(`CPU bucket is too low (${Game.cpu.bucket}). Postponing operation until bucket reaches 500.`);
            }
            shouldRun = false;
        }
        if (Memory.resetBucket) {
            if (Game.cpu.bucket < MAX_BUCKET - Game.cpu.limit) {
                log.info(`Operation suspended until bucket recovery. Bucket: ${Game.cpu.bucket}/${MAX_BUCKET}`);
                shouldRun = false;
            } else {
                delete Memory.resetBucket;
            }
        }
        if (Memory.haltTick) {
            if (Memory.haltTick == Game.time) {
                (<any>Game.cpu).halt(); // TODO: remove any typing when typed-screeps updates to include this method
                shouldRun = false;
            } else if (Memory.haltTick < Game.time) {
                delete Memory.haltTick;
            }
        }
        return shouldRun;
    }

    private static initGlobalMemory() {
        global._cache = <IGlobalCache>{
            accessed: {},
            expiration: {},
            structures: {},
            numbers: {},
            lists: {},
            costMatrices: {},
            roomPositions: {},
            things: {},
        };
    }

    private static formatDefaultMemory() {
        if (!Memory.rooms) {
            Memory.rooms = {};
        }
        if (!Memory.creeps) {
            Memory.creeps = {};
        }
        if (!Memory.flags) {
            Memory.flags = {};
        }
    }

    private static formatOvermindMemory() {
        if (!Memory.Collective) {
            Memory.Collective = {};
        }
        if (!Memory.colonies) {
            Memory.colonies = {};
        }
    }

    private static formatPathingMemory() {
        if (!Memory.pathing) {
            Memory.pathing = {} as PathingMemory; // Hacky workaround
        }
        _.defaults(Memory.pathing, {
            paths: {},
            distances: {},
            weightedDistances: {},
        });
    }

    static wrap(memory: any, memName: string, defaults = {}, deep = false) {
        if (!memory[memName]) {
            memory[memName] = _.clone(defaults);
        }
        if (deep) {
            _.defaultsDeep(memory[memName], defaults);
        } else {
            _.defaults(memory[memName], defaults);
        }
        return memory[memName];
    }

    static format() {
        this.formatDefaultMemory()
        this.formatOvermindMemory();
        this.formatPathingMemory();

        // Rest of memory formatting
        if (!Memory.settings) {
            Memory.settings = {} as any;
        }

        _.defaults(Memory.settings, {
            signature: "Dont hurt me!?",
            operationMode: DEFAULT_OPERATION_MODE,
            log: {},
            enableVisuals: true,
        });

        if (!Memory.constructionSites) {
            Memory.constructionSites = {};
        }

        this.initGlobalMemory();
    }

    static clean() {
        // Clean the memory of non-existent objects every tick
        this.cleanHeap();
        this.cleanCreeps();
        this.cleanFlags();
        this.cleanColonies();
        this.cleanPathingMemory();
        this.cleanConstructionSites();
        //Stats.clean();
    }

	/**
	 * Attempt to clear some things out of the global heap to prevent increasing CPU usage
	 */
    private static cleanHeap(): void {
        if (Game.time % HEAP_CLEAN_FREQUENCY == HEAP_CLEAN_FREQUENCY - 3) {
            if (Game.cpu.bucket < BUCKET_CPU_HALT) {
                (<any>Game.cpu).halt();
            } else if (Game.cpu.bucket < BUCKET_CLEAR_CACHE) {
                delete global._cache;
                this.initGlobalMemory();
            }
        }
    }

    private static cleanCreeps() {
        // Clear memory for non-existent creeps
        for (const name in Memory.creeps) {
            if (!Game.creeps[name]) {
                delete Memory.creeps[name];
                delete global[name];
            }
        }
    }

    private static cleanFlags() {
        // Clear memory for non-existent flags
        for (const name in Memory.flags) {
            if (!Game.flags[name]) {
                delete Memory.flags[name];
                delete global[name];
            }
        }
    }

    private static cleanColonies() {
        // Clear memory of dead colonies
        for (const name in Memory.colonies) {
            const room = Game.rooms[name];
            if (!(room && room.my)) {
                // Delete only if "persistent" is not set - use case: praise rooms
                if (!Memory.colonies[name].persistent) {
                    delete Memory.colonies[name];
                    delete global[name];
                }
            }
        }
    }

    private static cleanConstructionSites() {
        // Remove ancient construction sites
        if (Game.time % 10 == 0) {
            const CONSTRUCTION_SITE_TIMEOUT = 50000;
            // Add constructionSites to memory and remove really old ones
            for (const id in Game.constructionSites) {
                const site = Game.constructionSites[id];
                if (!Memory.constructionSites[id]) {
                    Memory.constructionSites[id] = Game.time;
                } else if (Game.time - Memory.constructionSites[id] > CONSTRUCTION_SITE_TIMEOUT) {
                    site.remove();
                }
                // Remove duplicate construction sites that get placed on top of existing structures due to caching
                if (site && site.pos.isVisible && site.pos.lookForStructure(site.structureType)) {
                    site.remove();
                }
            }
            // Remove dead constructionSites from memory
            for (const id in Memory.constructionSites) {
                if (!Game.constructionSites[id]) {
                    delete Memory.constructionSites[id];
                }
            }
        }
    }

    private static cleanPathingMemory() {
        const CLEAN_FREQUENCY = 5;
        if (Game.time % CLEAN_FREQUENCY == 0) {
            const distanceCleanProbability = 0.001 * CLEAN_FREQUENCY;
            const weightedDistanceCleanProbability = 0.01 * CLEAN_FREQUENCY;

            // Randomly clear some cached path lengths
            for (const pos1Name in Memory.pathing.distances) {
                if (_.isEmpty(Memory.pathing.distances[pos1Name])) {
                    delete Memory.pathing.distances[pos1Name];
                } else {
                    for (const pos2Name in Memory.pathing.distances[pos1Name]) {
                        if (Math.random() < distanceCleanProbability) {
                            delete Memory.pathing.distances[pos1Name][pos2Name];
                        }
                    }
                }
            }

            // Randomly clear weighted distances
            for (const pos1Name in Memory.pathing.weightedDistances) {
                if (_.isEmpty(Memory.pathing.weightedDistances[pos1Name])) {
                    delete Memory.pathing.weightedDistances[pos1Name];
                } else {
                    for (const pos2Name in Memory.pathing.weightedDistances[pos1Name]) {
                        if (Math.random() < weightedDistanceCleanProbability) {
                            delete Memory.pathing.weightedDistances[pos1Name][pos2Name];
                        }
                    }
                }
            }
        }
    }
}

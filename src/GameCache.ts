import _ from "lodash";
import DirectiveOutpost from "./directives/Outpost";
import { profile } from "./profiler";

@profile
export default class GameCache implements ICache {
    foremen: { [foremen: string]: { [roleName: string]: string[] } };
    creepsByBase: { [baseName: string]: Creep[] };
    targets: { [ref: string]: string[] };
    outpostFlags: Flag[];

    constructor() {
        this.foremen = {};
        this.creepsByBase = {};
        this.targets = {};
        this.outpostFlags = _.filter(Game.flags, flag => DirectiveOutpost.filter(flag));
    }

    private cacheCreepsByColony() {
        this.creepsByBase = _.groupBy(Game.creeps, creep => creep.memory.base) as { [colName: string]: Creep[] };
    }

    private cacheForemen() {
        this.foremen = {};
        const creepNamesByForemen = _.groupBy(_.keys(Game.creeps), name => Game.creeps[name].memory.foreman);
        for (const name in creepNamesByForemen) {
            this.foremen[name] = _.groupBy(creepNamesByForemen[name], name => Game.creeps[name].memory.role);
        }
    }

    private cacheTargets() {
        this.targets = {};
        for (const i in Game.creeps) {
            const creep = Game.creeps[i];
            let task = creep.memory.task;
            while (task) {
                if (!this.targets[task._target.ref]) this.targets[task._target.ref] = [];
                this.targets[task._target.ref].push(creep.name);
                task = task._parent;
            }
        }
    }

    build() {
        this.cacheCreepsByColony();
        this.cacheForemen();
        this.cacheTargets();
    }

    refresh() {
        this.cacheCreepsByColony();
        this.cacheForemen();
        this.cacheTargets();
    }
}

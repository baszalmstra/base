import DirectiveHarvest from "./directives/Harvest";
import _ from "lodash";
import StructureGroup from "./structureGroups/StructureGroup";
import { Bot } from "./bots/Bot";
import Factory from "./structureGroups/Factory";
import TransportRequestGroup from "./logistics/TransportRequestGroup";
import Mem from "./Memory";
import UpgradeSite from "./structureGroups/UpgradeSite";
import WorkerForeman from "./foremen/Worker";
import RoomPlanner from "./roomPlanner/RoomPlanner";
import Visualizer from "./visuals/Visualizer";
import RoadLogistics from "./logistics/RoadLogistics";
import LogisticsNetwork from "./logistics/LogisticsNetwork";
import { log } from "./console/log";
import { StoreStructure } from "./declarations/typeGuards";
import { mergeSum, derefProtoPos } from "./utilities/util";
import TransportForeman from "./foremen/Transport";
import { bunkerLayout, getPosFromBunkerCoord } from "./roomPlanner/layouts/bunker";
import ScoutForeman from "./foremen/Scout";
import Shield from "./structureGroups/Shield";
import { LinkNetwork } from "./logistics/LinkNetwork";
import Operations from "./structureGroups/Operations";
import { profile } from "./profiler";
import { EXPANSION_EVALUATION_FREQ, ExpansionEvaluator } from "./strategy/ExpansionEvaluator";
import { DefaultForeman } from "./foremen/Default";
import { Cartographer, ROOMTYPE_CONTROLLER } from "./utilities/Cartographer";
import { DirectiveExtract } from "./directives/Extract";

export enum DEFCON {
    safe = 0,
    invasionNPC = 1,
    boostedInvasionNPC = 2,
    playerInvasion = 2,
    bigPlayerInvasion = 3,
}

export enum BaseStage {
    Small = 0,		// No storage and no incubator
    Medium = 1,	// Has storage but RCL < 8
    Large = 2,		// RCL 8 room
}

export interface BaseMemory {
    defcon: {
        level: number,
        tick: number,
    };
    expansionData: {
        possibleExpansions: { [roomName: string]: number | boolean },
        expiration: number,
    };
}

const defaultBaseMemory: BaseMemory = {
    defcon: {
        level: DEFCON.safe,
        tick: -Infinity
    },
    expansionData: {
        possibleExpansions: {},
        expiration: 0,
    },
};

export interface BunkerData {
    anchor: RoomPosition;
    bottomSpawn: StructureSpawn | undefined;
    coreSpawn: StructureSpawn | undefined;
    rightSpawn: StructureSpawn | undefined;
}

export function getAllBases(): Base[] {
    return _.values(Collective.bases);
}

@profile
export default class Base {

    memory: BaseMemory;                         // Memory.colonies[name]

    id: number;
    name: string;                               // The name of the base
    ref: string;

    flags: Flag[] = [];                         // Flags belonging to the colony
    constructionSites!: ConstructionSite[];

    roomName!: string;
    room!: Room;
    roomNames!: string[];                       // The names of all the rooms of the base including the primary one
    rooms!: Room[];
    outposts!: Room[];

    controller!: StructureController;
    spawns!: StructureSpawn[];
    sources!: Source[];
    extractors!: StructureExtractor[];
    extensions!: StructureExtension[];
    storage: StructureStorage | undefined;
    rechargeables!: rechargeObjectType[];
    repairables!: Structure[];
    terminal!: StructureTerminal | undefined;
    towers!: StructureTower[];
    labs!: StructureLab[];
    tombstones!: Tombstone[];
    links!: StructureLink[];
    availableLinks!: StructureLink[];
    observer: StructureObserver | undefined;
    powerSpawn: StructurePowerSpawn | undefined;

    structureGroups!: StructureGroup[];

    miningSites!: { [flagName: string]: DirectiveHarvest };	// Component with logic for mining and hauling

    logisticsNetwork!: LogisticsNetwork;
    transportRequests!: TransportRequestGroup;
    linkNetwork!: LinkNetwork;

    pos!: RoomPosition

    assets!: { [resourceType: string]: number };

    factory: Factory | undefined;            // Component to encapsulate spawner logic
    operations: Operations | undefined;
    upgradeSite!: UpgradeSite;
    shield!: Shield | undefined;

    level!: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8; 				// Level of the colony's main room
    stage!: number;										// The stage of the colony "lifecycle"
    defcon!: number;
    bootstrapping: boolean = false;
    lowPowerMode: boolean = false;
    bunker: BunkerData | undefined;						// The center tile of the bunker, else undefined

    // Creeps and subset
    creeps!: Creep[];
    creepsByRole!: { [roleName: string]: Creep[] };

    foremen!: {
        default: DefaultForeman;
        work: WorkerForeman;
        logistics: TransportForeman;
        scout?: ScoutForeman;
    };

    roadLogistics!: RoadLogistics;
    // Room planner
    roomPlanner!: RoomPlanner;
    destinations!: { pos: RoomPosition, order: number }[];

    static settings = {
        remoteSourcesByLevel: {
            1: 1,
            2: 2,
            3: 3,
            4: 4,
            5: 5,
            6: 6,
            7: 7,
            8: 9,
        },
        maxSourceDistance: 100
    };

    constructor(id: number, roomName: string, outposts: string[]) {
        this.id = id;
        this.name = roomName
        this.ref = roomName;
        this.memory = Mem.wrap(Memory.colonies, roomName, defaultBaseMemory, true);

        // Register the base globally so that you can refer to it by its name
        global[this.name] = this
        global[this.name.toLowerCase()] = this;

        this.build(roomName, outposts);
    }

    /**
	 * Pretty-print the colony name in the console
	 */
    get print(): string {
        return `<a href="#!/room/${Game.shard.name}/${this.room.name}">[${this.name}]</a>`;
    }

    build(roomName: string, outposts: string[]) {
        this.roomName = roomName
        this.roomNames = _.concat([roomName], outposts);
        this.room = Game.rooms[roomName];
        this.outposts = _.compact(_.map(outposts, outpost => Game.rooms[outpost]));
        this.rooms = [this.room].concat(this.outposts);
        this.miningSites = {}
        this.destinations = [];

        // Register creeps
        this.creeps = Collective.cache.creepsByBase[this.name] || [];
        this.creepsByRole = _.groupBy(this.creeps, creep => creep.memory.role);

        this.registerRoomObjects()
        this.registerOperationalState();
        this.registerUtilities();
        this.registerStructureGroups();

        for (let spawn of this.sources) {
            DirectiveHarvest.createIfNotPresent(spawn.pos, "pos");
        }

        if (this.controller.level >= 6) {
            _.forEach(this.extractors, extractor => DirectiveExtract.createIfNotPresent(extractor.pos, 'pos'));
        }
    }

    private registerRoomObjects() {
        this.controller = this.room.controller!
        this.spawns = _.sortBy(_.filter(this.room.spawns, spawn => spawn.my && spawn.isActive()), spawn => spawn.name);

        this.pos = (this.spawns[0] || this.controller).pos

        this.sources = _.sortBy(_.flatten(_.map(this.rooms, room => room.sources)),
            source => source.pos.getMultiRoomRangeTo(this.pos));
        this.extractors = _(this.rooms)
            .map(room => room.extractor)
            .compact()
            .filter(extractor => (extractor!.my && extractor!.room.my)
                || Cartographer.roomType(extractor!.room.name) != ROOMTYPE_CONTROLLER)
            .sortBy(extractor => extractor!.pos.getMultiRoomRangeTo(this.pos)).value() as StructureExtractor[];
        this.extensions = this.room.extensions;
        this.storage = this.room.storage;
        this.terminal = this.room.terminal && this.room.terminal.isActive() ? this.room.terminal : undefined;
        this.towers = this.room.towers;
        this.labs = _.sortBy(_.filter(this.room.labs, lab => lab.my && lab.isActive()), lab => 50 * lab.pos.y + lab.pos.x);
        this.rechargeables = _.flatten(_.map(this.rooms, room => room.rechargeables));
        this.constructionSites = _.flatten(_.map(this.rooms, room => room.constructionSites));
        this.tombstones = _.flatten(_.map(this.rooms, room => room.tombstones));
        this.observer = this.room.observer;
        this.repairables = _.flatten(_.map(this.rooms, room => room.repairables));
        this.links = this.room.links;
        this.availableLinks = _.clone(this.links);
        this.powerSpawn = this.room.powerSpawn;

        this.assets = this.getAllAssets();
    }

    private registerOperationalState() {
        this.level = this.controller.level as 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8;
        this.bootstrapping = false;

        if (this.storage && this.spawns[0]) {
            // If the colony has storage and a hatchery
            if (this.controller.level == 8) {
                this.stage = BaseStage.Large;
            } else {
                this.stage = BaseStage.Medium;
            }
        } else {
            this.stage = BaseStage.Small;
        }

        // Set DEFCON level
        // TODO: finish this
        let defcon = DEFCON.safe;
        const defconDecayTime = 200;
        // if (this.room.dangerousHostiles.length > 0 && !this.controller.safeMode) {
        // 	const effectiveHostileCount = _.sum(_.map(this.room.dangerousHostiles,
        // 											  hostile => hostile.boosts.length > 0 ? 2 : 1));
        // 	if (effectiveHostileCount >= 3) {
        // 		defcon = DEFCON.boostedInvasionNPC;
        // 	} else {
        // 		defcon = DEFCON.invasionNPC;
        // 	}
        // }
        if (this.memory.defcon) {
            if (defcon < this.memory.defcon.level) { // decay defcon level over time if defcon less than memory value
                if (this.memory.defcon.tick + defconDecayTime < Game.time) {
                    this.memory.defcon.level = defcon;
                    this.memory.defcon.tick = Game.time;
                }
            } else if (defcon > this.memory.defcon.level) { // refresh defcon time if it increases by a level
                this.memory.defcon.level = defcon;
                this.memory.defcon.tick = Game.time;
            }
        } else {
            this.memory.defcon = {
                level: defcon,
                tick: Game.time
            };
        }
        this.defcon = this.memory.defcon.level;
    }

    private registerStructureGroups() {
        this.structureGroups = []

        if (this.spawns[0]) {
            this.factory = new Factory(this, this.spawns[0]);
        }

        this.upgradeSite = new UpgradeSite(this, this.controller);

        if (this.towers[0]) {
            this.shield = new Shield(this, this.towers[0]);
        }
        if (this.storage) {
            this.operations = new Operations(this, this.storage);
        }
    }

    private registerUtilities() {
        this.logisticsNetwork = new LogisticsNetwork(this);
        this.transportRequests = new TransportRequestGroup();
        this.linkNetwork = new LinkNetwork(this);
        this.roomPlanner = new RoomPlanner(this);
        if (this.roomPlanner.memory.bunkerData && this.roomPlanner.memory.bunkerData.anchor) {
            const anchor = derefProtoPos(this.roomPlanner.memory.bunkerData.anchor);
            const spawnPositions = _.map(bunkerLayout[8]!.buildings.spawn.pos, c => getPosFromBunkerCoord(c, this));
            const bottomSpawnPos = _.maxBy(spawnPositions, pos => pos.y)!;
            const rightSpawnPos = _.maxBy(spawnPositions, pos => pos.x)!;
            const coreSpawnPos = anchor.findClosestByRange(spawnPositions)!;
            this.bunker = {
                anchor: anchor,
                bottomSpawn: bottomSpawnPos.lookForStructure(STRUCTURE_SPAWN) as StructureSpawn | undefined,
                coreSpawn: coreSpawnPos.lookForStructure(STRUCTURE_SPAWN) as StructureSpawn | undefined,
                rightSpawn: rightSpawnPos.lookForStructure(STRUCTURE_SPAWN) as StructureSpawn | undefined,
            };
        }

        this.roadLogistics = new RoadLogistics(this);
    }

    private refreshStructureGroups() {
        for (const structure of this.structureGroups) {
            structure.refresh();
        }
    }

    private refreshUtilities() {
        this.logisticsNetwork.refresh();
        this.transportRequests.refresh();
        this.linkNetwork.refresh();
        this.roomPlanner.refresh();
        this.roadLogistics.refresh();
    }

    /**
     * Instantiate all foremen for the colony
     */
    spawnForemen(): void {
        this.foremen = {
            default: new DefaultForeman(this),
            work: new WorkerForeman(this),
            logistics: new TransportForeman(this),
        }

        if (!this.observer) {
            this.foremen.scout = new ScoutForeman(this);
        }

        for (const group of this.structureGroups) {
            group.spawnForemen()
        }
    }

    /**
	 * Get a list of creeps in the colony which have a specified role name
	 */
    getCreepsByRole(roleName: string): Creep[] {
        return this.creepsByRole[roleName] || [];
    }

	/**
	 * Get a list of zerg in the colony which have a specified role name
	 */
    getBotByRole(roleName: string): (Bot | undefined)[] {
        return _.map(this.getCreepsByRole(roleName), creep => Collective.bots[creep.name]);
    }

    /**
	 * Summarizes the total of all resources in colony store structures, labs, and some creeps
	 */
    private getAllAssets(verbose = false): { [resourceType: string]: number } {
        // if (this.name == 'E8S45') verbose = true; // 18863
        // Include storage structures, lab contents, and manager carry
        const stores = _.map(<StoreStructure[]>_.compact([this.storage, this.terminal]), s => s.store);
        const creepCarriesToInclude = _.map(this.creeps, creep => creep.carry) as { [resourceType: string]: number }[];
        const labContentsToInclude = _.map(_.filter(this.labs, lab => !!lab.mineralType), lab =>
            ({ [<string>lab.mineralType]: lab.mineralAmount })) as { [resourceType: string]: number }[];
        const allAssets: { [resourceType: string]: number } = mergeSum([
            ...stores,
            ...creepCarriesToInclude,
            ...labContentsToInclude
        ]);
        if (verbose) log.debug(`${this.room.print} assets: ` + JSON.stringify(allAssets));
        return allAssets;
    }

    refresh() {
        this.memory = Mem.wrap(Memory.colonies, this.roomName, defaultBaseMemory, true);
        this.room = Game.rooms[this.roomName];
        this.outposts = _.compact(_.map(this.outposts, outpost => Game.rooms[outpost.name]));
        this.rooms = [this.room].concat(this.outposts);

        // refresh creeps
        this.creeps = Collective.cache.creepsByBase[this.name] || [];
        this.creepsByRole = _.groupBy(this.creeps, creep => creep.memory.role);

        this.registerRoomObjects();
        this.registerOperationalState();
        this.refreshUtilities();
        this.refreshStructureGroups();
    }

    init() {
        for (const group of this.structureGroups) {
            group.init()
        }
        this.roadLogistics.init();
        this.roomPlanner.init();
        this.linkNetwork.init();
        if (Game.time % EXPANSION_EVALUATION_FREQ == 5 * this.id) {			// Re-evaluate expansion data if needed
            ExpansionEvaluator.refreshExpansionData(this);
        }
    }

    run() {
        for (const group of this.structureGroups) {
            group.run()
        }
        this.roadLogistics.run();
        this.roomPlanner.run();
        this.linkNetwork.run();
    }

    private drawCreepReport(coord: Coord): Coord {
        let { x, y } = coord;
        const roledata = Collective.director.getCreepReport(this);
        const tablePos = new RoomPosition(x, y, this.room.name);
        y = Visualizer.infoBox(`${this.name} Creeps`, roledata, tablePos, 7);
        return { x, y };
    }

    visuals() {
        let x = 1;
        let y = 1.5;
        let coord: Coord;
        coord = this.drawCreepReport({ x, y });
        x = coord.x;
        y = coord.y;

        for (const structureGroups of _.compact([this.factory, this.operations])) {
            coord = structureGroups!.visuals({ x, y });
            x = coord.x;
            y = coord.y;
        }
    }
}

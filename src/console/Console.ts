import { bullet } from "../utilities/stringConstants";
import Directive from "../directives/Directive";
import _ from "lodash";
import { toColumns } from "../utilities/util";

export class BaseConsole {
    static init() {
        global.help = this.help();
        global.print = this.print
        global.listDirectives = this.listDirectives;
        global.setMode = this.setMode;
    }

    static help() {
        let msg = "\n";
        // let msg = '\n<font color="#ff00ff">';
        // for (const line of asciiLogoSmall) {
        // 	msg += line + '\n';
        // }
        // msg += '</font>';

        const descr: { [functionName: string]: string } = {};
        descr.help = 'show this message';
        // descr['info()'] = 'display version and operation information';
        // descr['notifications()'] = 'print a list of notifications with hyperlinks to the console';
        descr['setMode(mode)'] = 'set the operational mode to "manual", "semiautomatic", or "automatic"';
        // descr['setSignature(newSignature)'] = 'set your controller signature; no argument sets to default';
        descr['print(...args[])'] = 'log stringified objects to the console';
        // descr['debug(thing)'] = 'enable debug logging for a game object or process';
        // descr['stopDebug(thing)'] = 'disable debug logging for a game object or process';
        // descr['timeit(function, repeat=1)'] = 'time the execution of a snippet of code';
        // descr['setLogLevel(int)'] = 'set the logging level from 0 - 4';
        // descr['suspendColony(roomName)'] = 'suspend operations within a colony';
        // descr['unsuspendColony(roomName)'] = 'resume operations within a suspended colony';
        // descr['openRoomPlanner(roomName)'] = 'open the room planner for a room';
        // descr['closeRoomPlanner(roomName)'] = 'close the room planner and save changes';
        // descr['cancelRoomPlanner(roomName)'] = 'close the room planner and discard changes';
        // descr['listActiveRoomPlanners()'] = 'display a list of colonies with open room planners';
        // descr['destroyErrantStructures(roomName)'] = 'destroys all misplaced structures within an owned room';
        // descr['destroyAllHostileStructures(roomName)'] = 'destroys all hostile structures in an owned room';
        // descr['destroyAllBarriers(roomName)'] = 'destroys all ramparts and barriers in a room';
        // descr['listConstructionSites(filter?)'] = 'list all construction sites matching an optional filter';
        descr['listDirectives(filter?)'] = 'list directives, matching a filter if specified';
        // descr['listPersistentDirectives()'] = 'print type, name, pos of every persistent directive';
        // descr['removeFlagsByColor(color, secondaryColor)'] = 'remove flags that match the specified colors';
        // descr['removeErrantFlags()'] = 'remove all flags which don\'t match a directive';
        // descr['deepCleanMemory()'] = 'deletes all non-critical portions of memory (be careful!)';
        // descr['profileMemory(depth=1)'] = 'scan through memory to get the size of various objects';
        // descr['startRemoteDebugSession()'] = 'enables the remote debugger so Muon can debug your code';
        // descr['cancelMarketOrders(filter?)'] = 'cancels all market orders matching filter (if provided)';
        // Console list
        const descrMsg = toColumns(descr, { justify: true, padChar: '.' });
        const maxLineLength = _.max(_.map(descrMsg, line => line.length))! + 10;
        msg += 'Console Commands: '.padRight(maxLineLength, '=') + '\n' + descrMsg.join('\n');

        msg += '\n\nRefer to the repository for more information\n';

        return msg;
    }

    static listDirectives(filter?: (dir: Directive) => any): string {
        let msg = '';
        for (const i in Collective.directives) {
            const dir = Collective.directives[i];
            if (!filter || filter(dir)) {
                msg += `${bullet}Name: ${dir.print}`.padRight(70) +
                    `Colony: ${dir.base.print}`.padRight(55) +
                    `Pos: ${dir.pos.print}\n`;
            }
        }
        return msg;
    }

    static print(...args: any[]): string {
        for (const arg of args) {
            let cache: any = [];
            const msg = JSON.stringify(arg, function (key, value) {
                if (typeof value === 'object' && value !== null) {
                    if (cache.indexOf(value) !== -1) {
                        // Duplicate reference found
                        try {
                            // If this value does not reference a parent it can be deduped
                            return JSON.parse(JSON.stringify(value));
                        } catch (error) {
                            // discard key if value cannot be deduped
                            return;
                        }
                    }
                    // Store value in our collection
                    cache.push(value);
                }
                return value;
            }, '  ');
            cache = null;
            console.log(msg);
        }
        return 'Done.';
    }

    static setMode(mode: operationMode): string {
        switch (mode) {
            case 'manual':
                Memory.settings.operationMode = 'manual';
                return `Operational mode set to manual. Only defensive directives will be placed automatically; ` +
                    `remove harvesting, claiming, room planning, and raiding must be done manually.`;
            case 'semiautomatic':
                Memory.settings.operationMode = 'semiautomatic';
                return `Operational mode set to semiautomatic. Claiming, room planning, and raiding must be done ` +
                    `manually; everything else is automatic.`;
            case 'automatic':
                Memory.settings.operationMode = 'automatic';
                return `Operational mode set to automatic. All actions are done automatically, but manually placed ` +
                    `directives will still be responded to.`;
            default:
                return `Invalid mode: please specify 'manual', 'semiautomatic', or 'automatic'.`;
        }
    }
}

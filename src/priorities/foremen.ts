export const ForemanPriority = {
    emergency: {				// Colony-wide emergencies such as a catastrohic crash
        bootstrap: 0
    },

    core: {                 // Functionality related to spawning more creeps
        supplier: 100,
        manager: 101,
    },

    offense: {					// Offensive operations like raids or sieges
        destroy: 300,
        healPoint: 301,
        siege: 302,
        controllerAttack: 399
    },

    colonization: { 			// Colonizing new rooms
        claim: 400,
        pioneer: 401,
    },

    ownedRoom: {
        firstTransport: 500,		// High priority to spawn the first transporter
        mine: 501,
        work: 502,
        mineralRCL8: 503,
        transport: 510,		// Spawn the rest of the transporters
        mineral: 520
    },

    outpostDefense: {
        outpostDefense: 550,
        guard: 551,
    },

    upgrading: {
        upgrade: 600,
    },

    scouting: {
        stationary: 800,
        randomWalker: 801
    },

    remoteRoom: { 				// Operation of a remote room. Allows colonies to restart one room at a time.
        reserve: 900,
        mine: 901,
        roomIncrement: 5, 			// remote room priorities are incremented by this for each outpost
    },

    remoteSKRoom: {
        sourceReaper: 1000,
        mineral: 1001,
        mine: 1002,
        roomIncrement: 5,
    },

    default: 99999				// Default overlord priority to ensure it gets run last
}

import _ from "lodash";
import Base from "../Base";

export interface BodySetup {
    pattern: BodyPartConstant[];            // Body pattern to be repeated
    sizeLimit: number;                      // Maximum number of unit repetitions to make the body
    prefix: BodyPartConstant[];             // Units at the start of the body
    suffix: BodyPartConstant[];             // Units at the end of the body
    proportionalPrefixSuffix: boolean;
    ordered: boolean;                       // Order the units by type
}

export function bodyCost(bodyParts: BodyPartConstant[]): number {
    return _.sumBy(bodyParts, part => BODYPART_COST[part]);
}

export function patternCost(setup: CreepSetup): number {
    return bodyCost(setup.bodySetup.pattern);
}

export class CreepSetup {
    role: string;
    bodySetup: BodySetup;

    constructor(roleName: string, bodySetup: Partial<BodySetup> = {}) {
        this.role = roleName;
        this.bodySetup = _.defaults(bodySetup, {
            pattern: [],
            sizeLimit: Infinity,
            prefix: [],
            suffix: [],
            proportionalPrefixSuffix: false,
            ordered: true
        });
    }

    /* Generate the largest body of the given pattern that is producable with the given energy */
    generateBody(availableEnergy: number): BodyPartConstant[] {
        let patternCost, patternLength, numRepeats: number;
        const prefix = this.bodySetup.prefix;
        const suffix = this.bodySetup.suffix;
        let body: BodyPartConstant[] = [];
        // Calculate repetitions
        if (this.bodySetup.proportionalPrefixSuffix) {
            patternCost = bodyCost(prefix) + bodyCost(this.bodySetup.pattern) + bodyCost(suffix);
            patternLength = prefix.length + this.bodySetup.pattern.length + suffix.length;
            const energyLimit = Math.floor(availableEnergy / patternCost);
            const maxPartLimit = Math.floor(MAX_CREEP_SIZE / patternLength);
            numRepeats = Math.min(energyLimit, maxPartLimit, this.bodySetup.sizeLimit);
        } else {
            const extraCost = bodyCost(prefix) + bodyCost(suffix);
            patternCost = bodyCost(this.bodySetup.pattern);
            patternLength = this.bodySetup.pattern.length;
            const energyLimit = Math.floor((availableEnergy - extraCost) / patternCost);
            const maxPartLimit = Math.floor((MAX_CREEP_SIZE - prefix.length - suffix.length) / patternLength);
            numRepeats = Math.min(energyLimit, maxPartLimit, this.bodySetup.sizeLimit);
        }
        // Build the body
        if (this.bodySetup.proportionalPrefixSuffix) {
            for (let i = 0; i < numRepeats; i++) {
                body = body.concat(prefix);
            }
        } else {
            body = body.concat(prefix);
        }
        if (this.bodySetup.ordered) {
            for (const part of this.bodySetup.pattern) {
                for (let i = 0; i < numRepeats; ++i) {
                    body.push(part);
                }
            }
        } else {
            for (let i = 0; i < numRepeats; ++i) {
                body = body.concat(this.bodySetup.pattern);
            }
        }
        if (this.bodySetup.proportionalPrefixSuffix) {
            for (let i = 0; i < numRepeats; i++) {
                body = body.concat(suffix);
            }
        } else {
            body = body.concat(suffix);
        }
        return body;
    }

    getBodyPotential(partType: BodyPartConstant, colony: Base): number {
        // let energyCapacity = Math.max(colony.room.energyCapacityAvailable,
        // 							  colony.incubator ? colony.incubator.room.energyCapacityAvailable : 0);
        let energyCapacity = colony.room.energyCapacityAvailable;
        // if (colony.spawnGroup) {
        //     const colonies = _.compact(_.map(colony.spawnGroup.memory.colonies,
        //         name => Collective.bases[name])) as Base[];
        //     energyCapacity = _.max(_.map(colonies, colony => colony.room.energyCapacityAvailable));
        // }
        const body = this.generateBody(energyCapacity);
        return _.filter(body, (part: BodyPartConstant) => part == partType).length;
    }
}

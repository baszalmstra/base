import Task from "./Task";
import { log } from "../console/log";
import _ from "lodash";
import { Roles } from "../creepSetups/setups";
import { Bot } from "../bots/Bot";
import { isResource } from "../declarations/typeGuards";
import { TaskWithdraw } from "./Withdraw";
import TaskPickup from "./Pickup";
import { Tasks } from "./Tasks";
import { TaskHarvest } from "./TaskHarvest";
import { maxBy } from "../utilities/util";
import { profile } from "../profiler";

export type rechargeTargetType = null;

@profile
export default class TaskRecharge extends Task {
    static taskName = "recharge";

    data!: {
        minEnergy: number;
    }

    constructor(target: rechargeTargetType, minEnergy = 0, options: TaskOptions = {}) {
        super(TaskRecharge.taskName, { ref: "", pos: { x: -1, y: -1, roomName: "" } }, options);
        this.data.minEnergy = minEnergy;
    }

    private rechargeRateForCreep(creep: Bot, obj: rechargeObjectType): number | false {
        if (creep.base.factory && creep.base.factory.battery
            && obj.id == creep.base.factory.battery.id && creep.roleName != Roles.supplier) {
            return false; // only queens can use the hatchery battery
        }
        let amount = isResource(obj) ? obj.amount : obj.energy;
        if (amount < this.data.minEnergy) {
            return false;
        }
        const otherTargeters = _.filter(_.map(obj.targetedBy, name => Collective.bots[name]),
            zerg => !!zerg && zerg.memory._task
                && (zerg.memory._task.name == TaskWithdraw.taskName
                    || zerg.memory._task.name == TaskPickup.taskName));
        const resourceOutflux = _.sum(_.map(otherTargeters,
            other => other.carryCapacity - _.sum(other.carry)));
        amount = _.clamp(amount - resourceOutflux, 0, creep.carryCapacity);
        const effectiveAmount = amount / (creep.pos.getMultiRoomRangeTo(obj.pos) + 1);
        if (effectiveAmount <= 0) {
            return false;
        } else {
            return effectiveAmount;
        }
    }

    set creep(creep: Bot) {
        this._creep.name = creep.name;
        if (this._parent) {
            this.parent!.creep = creep;
        }
        const target = creep.inColonyRoom
            ? maxBy(creep.base.rechargeables, o => this.rechargeRateForCreep(creep, o))
            : maxBy(creep.room.rechargeables, o => this.rechargeRateForCreep(creep, o));

        if (!target || creep.pos.getMultiRoomRangeTo(target.pos) > 40) {
            // Worker shouldn't harvest; let drones do it(disabling this check can destabilize early economy)
            const canHarvest = creep.getActiveBodyparts(WORK) > 0 && creep.roleName != "worker";
            if (canHarvest) {
                // Harvest from a soruce if there is no recharge target available
                const availableSources = _.filter(creep.room.sources, source => {
                    const isSurrounded = source.pos.availableNeighbors(false).length == 0;
                    return !isSurrounded || creep.pos.isNearTo(source);
                })
                const availableSource = creep.pos.findClosestByMultiRoomRange(availableSources);
                if (availableSource) {
                    creep.task = new TaskHarvest(availableSource);
                    return;
                }
            }
        }
        if (target) {
            if (isResource(target)) {
                creep.task = new TaskPickup(target);
                return
            } else {
                creep.task = new TaskWithdraw(target);
                return;
            }
        } else {
            log.debug(`No valid withdraw target for ${creep.print}!`);
            creep.task = null;
        }
    }

    isValidTask(): boolean {
        return false;
    }

    isValidTarget(): boolean {
        return false;
    }

    work(): number {
        log.warning(`BAD RESULT: Should not get here...`);
        return ERR_INVALID_TARGET;
    }


}

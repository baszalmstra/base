import Task from "./Task";
import { profile } from "../profiler/Profiler";
import { isSource } from "../declarations/typeGuards";

export type harvestTargetType = Source | Mineral;

@profile
export class TaskHarvest extends Task {
    static taskName = 'harvest';

    target!: harvestTargetType;

    constructor(target: harvestTargetType, options = {} as TaskOptions) {
        super(TaskHarvest.taskName, target, options);
    }

    isValidTask() {
        return this.creep.carry.getUsedCapacity() < this.creep.carryCapacity;
    }

    isValidTarget() {
        if (isSource(this.target)) {
            return this.target.energy > 0;
        } else {
            return this.target.mineralAmount > 0;
        }
    }

    work() {
        return this.creep.harvest(this.target);
    }
}


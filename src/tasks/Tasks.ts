import { withdrawTargetType, TaskWithdraw } from "./Withdraw";
import TaskRecharge from "./Recharge";
import { transferTargetType, TaskTransfer } from "./Transfer";
import TaskSignController, { signControllerTargetType } from "./SignController";
import { TaskUpgrade, upgradeTargetType } from "./Upgrade";
import TaskBuild, { buildTargetType } from "./Build";
import TaskRepair, { repairTargetType } from "./Repair";
import Task from "./Task";
import _ from "lodash";
import { transferAllTargetType, TaskTransferAll } from "./TransferAll";
import TaskPickup, { pickupTargetType } from "./Pickup";
import { withdrawAllTargetType, TaskWithdrawAll } from "./WithdrawAll";
import { TaskGoToRoom, goToRoomTargetType } from "./GoToRoom";
import { reserveTargetType, TaskReserve } from "./Reserve";
import { fortifyTargetType, TaskFortify } from "./Fortify";
import { claimTargetType, TaskClaim } from "./Claim";
import { dismantleTargetType, TaskDismantle } from "./Dismantle";
import { TaskHarvest, harvestTargetType } from "./TaskHarvest";

export class Tasks {
    static chain(tasks: Task[], setNextPos = true): Task | null {
        if (tasks.length == 0) {
            // log.error(`Tasks.chain was passed an empty array of tasks!`);
            return null;
        }
        if (setNextPos) {
            for (let i = 0; i < tasks.length - 1; i++) {
                tasks[i].options.nextPos = tasks[i + 1].targetPos;
            }
        }
        // Make the accumulator task from the end and iteratively fork it
        let task = _.last(tasks)!; // start with last task
        tasks = _.dropRight(tasks); // remove it from the list
        for (let i = (tasks.length - 1); i >= 0; i--) { // iterate over the remaining tasks
            task = task.fork(tasks[i]);
        }
        return task;
    }

    static pickup(target: pickupTargetType, options = {} as TaskOptions): TaskPickup {
        return new TaskPickup(target, options);
    }

    static withdraw(target: withdrawTargetType,
        resourceType: ResourceConstant = RESOURCE_ENERGY,
        amount?: number,
        options = {} as TaskOptions): TaskWithdraw {
        return new TaskWithdraw(target, resourceType, amount, options);
    }

    static recharge(minEnergy = 0, options = {} as TaskOptions): TaskRecharge {
        return new TaskRecharge(null, minEnergy, options);
    }

    static transfer(target: transferTargetType,
        resourceType: ResourceConstant = RESOURCE_ENERGY,
        amount?: number,
        options: TaskOptions = {}) {
        return new TaskTransfer(target, resourceType, amount, options);
    }

    static transferAll(target: transferAllTargetType,
        skipEnergy = false,
        options = {} as TaskOptions): TaskTransferAll {
        return new TaskTransferAll(target, skipEnergy, options);
    }

    static signController(target: signControllerTargetType,
        options = {} as TaskOptions): TaskSignController {
        return new TaskSignController(target, options);
    }

    static reserve(target: reserveTargetType, options = {} as TaskOptions): TaskReserve {
        return new TaskReserve(target, options);
    }

    static upgrade(target: upgradeTargetType, options: TaskOptions = {}): TaskUpgrade {
        return new TaskUpgrade(target, options);
    }

    static build(target: buildTargetType, options: TaskOptions = {}) {
        return new TaskBuild(target, options);
    }

    static repair(target: repairTargetType, options: TaskOptions = {}) {
        return new TaskRepair(target as repairTargetType);
    }

    static withdrawAll(target: withdrawAllTargetType, options: TaskOptions = {}): TaskWithdrawAll {
        return new TaskWithdrawAll(target, options);
    }

    static goToRoom(target: goToRoomTargetType, options = {} as TaskOptions): TaskGoToRoom {
        return new TaskGoToRoom(target, options);
    }

    static fortify(target: fortifyTargetType, options: TaskOptions = {}): TaskFortify {
        return new TaskFortify(target, options);
    }

    static claim(target: claimTargetType, options: TaskOptions = {}): TaskClaim {
        return new TaskClaim(target, options);
    }

    static dismantle(target: dismantleTargetType, options = {} as TaskOptions): TaskDismantle {
        return new TaskDismantle(target, options);
    }

    static harvest(target: harvestTargetType, options = {} as TaskOptions): TaskHarvest {
        return new TaskHarvest(target, options);
    }
}

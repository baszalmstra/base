import { profile } from "../profiler/Profiler";
import Task from "./Task";


export type dismantleTargetType = Structure;

@profile
export class TaskDismantle extends Task {
    static taskName: string = 'dismantle';

    target!: dismantleTargetType;

    constructor(target: dismantleTargetType, options = {} as TaskOptions) {
        super(TaskDismantle.taskName, target, options);
        this.settings.timeout = 100;
    }

    isValidTask() {
        return (this.creep.getActiveBodyparts(WORK) > 0);
    }

    isValidTarget() {
        return this.target && this.target.hits > 0;
    }

    work() {
        return this.creep.dismantle(this.target);
    }
}

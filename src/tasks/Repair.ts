import Task from "./Task";
import { profile } from "../profiler";

export type repairTargetType = Structure;

@profile
export default class TaskRepair extends Task {
    static taskName = "repair"

    target!: repairTargetType;

    constructor(target: repairTargetType, options = {} as TaskOptions) {
        super(TaskRepair.taskName, target, options);
        // Settings
        this.settings.timeout = 100;
        this.settings.targetRange = 3;
    }

    isValidTask() {
        return this.creep.carry.energy > 0;
    }

    isValidTarget() {
        return this.target && this.target.hits < this.target.hitsMax;
    }

    work() {
        const result = this.creep.repair(this.target);
        if (this.target.structureType == STRUCTURE_ROAD) {
            // prevents workers from idling for a tick before moving to next target
            const newHits = this.target.hits + this.creep.getActiveBodyparts(WORK) * REPAIR_POWER;
            if (newHits > this.target.hitsMax) {
                this.finish();
            }
        }
        return result;
    }
}

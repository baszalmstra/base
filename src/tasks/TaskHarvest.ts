import Task from "./Task";
import _ from "lodash";
import { isSource } from "../declarations/typeGuards";

export type harvestTargetType = Source | Mineral;

export class TaskHarvest extends Task {
    static taskName = "harvest"

    target!: harvestTargetType;

    constructor(target: harvestTargetType, options = {} as TaskOptions) {
        super(TaskHarvest.taskName, target, options);
    }

    isValidTask() {
        return _.sum(_.values(this.creep.carry)) < this.creep.carryCapacity;
    }

    isValidTarget() {
        if (isSource(this.target)) {
            return this.target.energy > 0;
        } else {
            return this.target.mineralAmount > 0;
        }
    }

    work() {
        return this.creep.harvest(this.target);
    }
}


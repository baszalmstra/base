import { Bot } from "../bots/Bot";
import { derefProtoPos, deref } from "../utilities/util";
import { log } from "../console/log";
import { initializeTask } from "./initializer";
import _ from "lodash";

type targetType = { ref: string, pos: ProtoPos };

export default abstract class Task {
    static taskName: string;

    name: string;               // Name of the task type
    _creep: {
        name: string;
    }
    _target: {
        ref: string;
        _pos: ProtoPos;
    }
    _parent: ProtoTask | null;
    tick: number
    settings: TaskSettings;
    options: TaskOptions;
    data: TaskData;

    private _targetPos: RoomPosition | undefined;

    constructor(taskName: string, target: targetType, options: TaskOptions = {}) {
        this.name = taskName;
        this._creep = {
            name: ""
        }
        if (target) {
            this._target = {
                ref: target.ref,
                _pos: target.pos
            }
        } else {
            this._target = {
                ref: "",
                _pos: {
                    x: -1,
                    y: -1,
                    roomName: ""
                }
            }
        }
        this._parent = null;
        this.settings = {
            targetRange: 1,         // range at which you can perform action
            workOffRoad: false,     // whether work() should be perform off road
            oneShot: false,         // remove this task once work() returns OK, regardless of validity
            timeout: Infinity,      // task becomes invalid after this long
            blind: true             // don't require vision of target unless in room
        }
        this.tick = Game.time;
        this.options = options;
        this.data = {}
    }

    get proto(): ProtoTask {
        return {
            name: this.name,
            _creep: this._creep,
            _target: this._target,
            _parent: this._parent,
            tick: this.tick,
            options: this.options,
            data: this.data
        }
    }

    set proto(protoTask: ProtoTask) {
        // Don't write to this.name; used in task switcher
        this._creep = protoTask._creep;
        this._target = protoTask._target;
        this._parent = protoTask._parent;
        this.tick = protoTask.tick;
        this.options = protoTask.options;
        this.data = protoTask.data;
    }

    get creep(): Bot {
        return Collective.bots[this._creep.name]
    }

    set creep(creep: Bot) {
        this._creep.name = creep.name;
        if (this._parent) {
            this.parent!.creep = creep;
        }
    }

    get target(): RoomObject | null {
        return deref(this._target.ref);
    }

    get targetPos(): RoomPosition {
        if (!this._targetPos) {
            if (this.target) {
                this._target._pos = this.target.pos;
            }
            this._targetPos = derefProtoPos(this._target._pos);
        }
        return this._targetPos;
    }

    get parent(): Task | null {
        return (this._parent ? initializeTask(this._parent) : null);
    }

    set parent(parentTask: Task | null) {
        this._parent = parentTask ? parentTask.proto : null;
        if (this.creep) {
            this.creep.task = this;
        }
    }

    /**
	 * Return a list of [this, this.parent, this.parent.parent, ...] as tasks
	 */
    get manifest(): Task[] {
        const manifest: Task[] = [this];
        let parent = this.parent;
        while (parent) {
            manifest.push(parent);
            parent = parent.parent;
        }
        return manifest;
    }

	/**
	 * Return a list of [this.target, this.parent.target, ...] without fully instantiating the list of tasks
	 */
    get targetManifest(): (RoomObject | null)[] {
        const targetRefs: string[] = [this._target.ref];
        let parent = this._parent;
        while (parent) {
            targetRefs.push(parent._target.ref);
            parent = parent._parent;
        }
        return _.map(targetRefs, ref => deref(ref));
    }

	/**
	 * Return a list of [this.targetPos, this.parent.targetPos, ...] without fully instantiating the list of tasks
	 */
    get targetPosManifest(): RoomPosition[] {
        const targetPositions: ProtoPos[] = [this._target._pos];
        let parent = this._parent;
        while (parent) {
            targetPositions.push(parent._target._pos);
            parent = parent._parent;
        }
        return _.map(targetPositions, protoPos => derefProtoPos(protoPos));
    }

    /**
	 * Return expected number of ticks until creep arrives at its first destination
	 */
    get eta(): number | undefined {
        if (this.creep && this.creep.memory._go && this.creep.memory._go.path) {
            return this.creep.memory._go.path.length;
        }
        return undefined;
    }

    fork(newTask: Task): Task {
        newTask.parent = this;
        if (this.creep) {
            this.creep.task = newTask;
        }
        return newTask;
    }

    abstract isValidTask(): boolean;

    abstract isValidTarget(): boolean;


    isValid(): boolean {
        let validTask = false;
        if (this.creep) {
            validTask = this.isValidTask() && Game.time - this.tick < this.settings.timeout;
        }
        let validTarget = false;
        if (this.target) {
            validTarget = this.isValidTarget();
        } else if ((this.settings.blind || this.options.blind) && !Game.rooms[this.targetPos.roomName]) {
            validTarget = true;
        }

        if (validTask && validTarget) {
            return true;
        } else {
            this.finish();
            const isValid = this.parent ? this.parent.isValid() : false;
            return isValid;
        }
    }

    /**
	 * Move to within range of the target
	 */
    moveToTarget(range = this.settings.targetRange): number {
        return this.creep.goTo(this.targetPos, { range: range });
    }

    moveToNextPos(): number | undefined {
        if (this.options.nextPos) {
            const nextPos = derefProtoPos(this.options.nextPos);
            return this.creep.goTo(nextPos);
        }
        return undefined;
    }

    run(): number | undefined {
        if (this.isWorking) {
            delete this.creep.memory._go;
            const result = this.work();
            if (this.settings.oneShot && result === OK) {
                this.finish();
            }
            return result;
        } else {
            this.moveToTarget();
        }
        return undefined
    }

    /**
	 * Return whether the creep is currently performing its task action near the target
	 */
    get isWorking(): boolean {
        return this.creep.pos.inRangeToPos(this.targetPos, this.settings.targetRange) && !this.creep.pos.isEdge;
    }

	/**
	 * Task to perform when at the target
	 */
    abstract work(): number;

    /**
	 * Finalize the task and switch to parent task (or null if there is none)
	 */
    finish(): void {
        this.moveToNextPos();
        if (this.creep) {
            this.creep.task = this.parent;
        } else {
            log.debug(`No creep executing ${this.name}! Proto: ${JSON.stringify(this.proto)}`);
        }
    }
}

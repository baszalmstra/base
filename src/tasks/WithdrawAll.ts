import { StoreStructure } from "../declarations/typeGuards";
import Task from "./Task";
import _ from "lodash";

export type withdrawAllTargetType = StoreStructure | Tombstone;

export const withdrawAllTaskName = 'withdrawAll';

export class TaskWithdrawAll extends Task {
    static taskName = "withdrawAll";

    target!: withdrawAllTargetType;

    constructor(target: withdrawAllTargetType, options = {} as TaskOptions) {
        super(withdrawAllTaskName, target, options);
    }

    isValidTask() {
        return (_.sum(_.values(this.creep.carry)) < this.creep.carryCapacity);
    }

    isValidTarget() {
        return _.sum(_.values(this.target.store)) > 0;
    }

    work() {
        for (const resourceType in this.target.store) {
            const amountInStore = this.target.store[<ResourceConstant>resourceType] || 0;
            if (amountInStore > 0) {
                return this.creep.withdraw(this.target, <ResourceConstant>resourceType);
            }
        }
        return -1;
    }

}


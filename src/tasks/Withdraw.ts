import Task from "./Task";
import _ from "lodash";
import { isStoreStructure, isEnergyStructure, EnergyStructure, StoreStructure } from "../declarations/typeGuards";

export type withdrawTargetType =
    EnergyStructure
    | StoreStructure
    | StructureLab
    | StructurePowerSpawn
    | Tombstone;

export class TaskWithdraw extends Task {
    static taskName = "withdraw";

    target!: withdrawTargetType;
    data!: {
        resourceType: ResourceConstant,
        amount: number | undefined
    }

    constructor(target: withdrawTargetType,
        resourceType: ResourceConstant = RESOURCE_ENERGY, amount?: number, options: TaskOptions = {}) {
        super(TaskWithdraw.taskName, target, options);
        this.settings.oneShot = true;
        this.data.resourceType = resourceType;
        this.data.amount = amount;
    }

    isValidTask(): boolean {
        const amount = this.data.amount || 1;
        return (_.sum(_.values(this.creep.carry)) <= this.creep.carryCapacity - amount);
    }
    isValidTarget(): boolean {
        const amount = this.data.amount || 1;
        const target = this.target;
        if (target instanceof Tombstone || isStoreStructure(target)) {
            return (target.store[this.data.resourceType] || 0) >= 0;
        } else if (isEnergyStructure(target) && this.data.resourceType == RESOURCE_ENERGY) {
            return target.energy >= amount;
        } else {
            if (target instanceof StructureLab) {
                return this.data.resourceType == target.mineralType && target.mineralAmount >= amount;
            } else if (target instanceof StructurePowerSpawn) {
                return this.data.resourceType == RESOURCE_POWER && target.power >= amount;
            }
        }
        return false;
    }
    work(): number {
        return this.creep.withdraw(this.target, this.data.resourceType, this.data.amount);
    }
}

import { deref } from "../utilities/util";
import { TaskWithdraw, withdrawTargetType } from "./Withdraw";
import TaskPickup, { pickupTargetType } from "./Pickup";
import TaskRecharge from "./Recharge";
import { TaskTransfer, transferTargetType } from "./Transfer";
import Task from "./Task";
import TaskSignController, { signControllerTargetType } from "./SignController";
import { TaskUpgrade, upgradeTargetType } from "./Upgrade";
import TaskBuild, { buildTargetType } from "./Build";
import TaskRepair, { repairTargetType } from "./Repair";
import { TaskTransferAll, transferAllTargetType } from "./TransferAll";
import { TaskWithdrawAll, withdrawAllTargetType } from "./WithdrawAll";
import { TaskHarvest, harvestTargetType } from "./TaskHarvest";
import { TaskGoToRoom, goToRoomTargetType } from "./GoToRoom";
import { TaskReserve, reserveTargetType } from "./Reserve";
import { TaskFortify, fortifyTargetType } from "./Fortify";
import { TaskClaim, claimTargetType } from "./Claim";
import { TaskDismantle, dismantleTargetType } from "./Dismantle";

export function initializeTask(protoTask: ProtoTask): Task {
    const taskName = protoTask.name;
    const target = deref(protoTask._target.ref);
    let task: any;
    if (taskName == TaskWithdraw.taskName) {
        task = new TaskWithdraw(target as withdrawTargetType);
    } else if (taskName == TaskPickup.taskName) {
        task = new TaskPickup(target as pickupTargetType);
    } else if (taskName == TaskRecharge.taskName) {
        task = new TaskRecharge(null);
    } else if (taskName == TaskTransfer.taskName) {
        task = new TaskTransfer(target as transferTargetType);
    } else if (taskName == TaskSignController.taskName) {
        task = new TaskSignController(target as signControllerTargetType)
    } else if (taskName == TaskUpgrade.taskName) {
        task = new TaskUpgrade(target as upgradeTargetType);
    } else if (taskName == TaskBuild.taskName) {
        task = new TaskBuild(target as buildTargetType);
    } else if (taskName == TaskRepair.taskName) {
        task = new TaskRepair(target as repairTargetType);
    } else if (taskName == TaskTransferAll.taskName) {
        task = new TaskTransferAll(target as transferAllTargetType);
    } else if (taskName == TaskWithdrawAll.taskName) {
        task = new TaskWithdrawAll(target as withdrawAllTargetType);
    } else if (taskName == TaskHarvest.taskName) {
        task = new TaskHarvest(target as harvestTargetType);
    } else if (taskName == TaskGoToRoom.taskName) {
        task = new TaskGoToRoom(protoTask._target._pos.roomName as goToRoomTargetType);
    } else if (taskName == TaskReserve.taskName) {
        task = new TaskReserve(target as reserveTargetType);
    } else if (taskName == TaskFortify.taskName) {
        task = new TaskFortify(target as fortifyTargetType)
    } else if (taskName == TaskClaim.taskName) {
        task = new TaskClaim(target as claimTargetType);
    } else if (taskName == TaskDismantle.taskName) {
        task = new TaskDismantle(target as dismantleTargetType)
    } else if (taskName == TaskHarvest.taskName) {
        task = new TaskHarvest(target as harvestTargetType)
    } else {
        throw new Error("invalid task!")
    }
    task.proto = protoTask;
    return task;
}

import Task from "./Task";
import { profile } from "../profiler";

export type buildTargetType = ConstructionSite;

@profile
export default class TaskBuild extends Task {
    static taskName: "build";

    target!: buildTargetType;

    constructor(target: buildTargetType, options = {} as TaskOptions) {
        super(TaskBuild.taskName, target, options);
        // Settings
        this.settings.targetRange = 3;
        this.settings.workOffRoad = true;
    }

    isValidTask() {
        return this.creep.carry.energy > 0;
    }

    isValidTarget() {
        return this.target && this.target.my && this.target.progress < this.target.progressTotal;
    }

    work() {
        // Fixes issue #9 - workers freeze if creep sitting on square
        if (!this.target.isWalkable) {
            const creepOnTarget = this.target.pos.lookFor(LOOK_CREEPS)[0];
            if (creepOnTarget) {
                const zerg = Collective.bots[creepOnTarget.name];
                if (zerg) {
                    this.creep.say('move pls');
                    zerg.moveOffCurrentPos();
                }
            }
        }
        return this.creep.build(this.target);
    }
}

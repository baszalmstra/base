import Task from "./Task";
import { profile } from "../profiler";

export type signControllerTargetType = StructureController;

@profile
export default class TaskSignController extends Task {
    static taskName = "signController";
    target!: signControllerTargetType;

    constructor(target: signControllerTargetType, options = {} as TaskOptions) {
        super(TaskSignController.taskName, target, options);
    }

    isValidTask() {
        return true;
    }

    isValidTarget() {
        const controller = this.target;
        return (!controller.sign || controller.sign.text != Memory.settings.signature) && !controller.signedByScreeps;
    }

    work() {
        return this.creep.signController(this.target, Memory.settings.signature);
    }
}

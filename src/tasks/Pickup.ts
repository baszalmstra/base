import Task from "./Task";
import _ from "lodash";
import { profile } from "../profiler";

export type pickupTargetType = Resource;

@profile
export default class TaskPickup extends Task {
    static taskName = "pickup";

    target!: pickupTargetType;

    constructor(target: pickupTargetType, options: TaskOptions = {}) {
        super(TaskPickup.taskName, target, options);
        this.settings.oneShot = true;
    }

    isValidTask(): boolean {
        return _.sum(_.values(this.creep.carry)) < this.creep.carryCapacity;
    }
    isValidTarget(): boolean {
        return this.target && this.target.amount > 0;
    }
    work(): number {
        return this.creep.pickup(this.target);
    }
}

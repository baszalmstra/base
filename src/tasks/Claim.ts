import Task from "./Task";
import { profile } from "../profiler";

export type claimTargetType = StructureController;

@profile
export class TaskClaim extends Task {
    static taskName = 'claim';

    target!: claimTargetType;

    constructor(target: claimTargetType, options = {} as TaskOptions) {
        super(TaskClaim.taskName, target, options);
        // Settings
    }

    isValidTask() {
        return (this.creep.getActiveBodyparts(CLAIM) > 0);
    }

    isValidTarget() {
        return (this.target != null && (!this.target.room || !this.target.owner));
    }

    work() {
        const result = this.creep.claimController(this.target);
        if (result == OK) {
            Collective.shouldRebuild = true; // rebuild the overmind object on the next tick to account for new room
        }
        return result;
    }
}
